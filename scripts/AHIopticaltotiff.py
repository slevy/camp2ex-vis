#! /usr/bin/env python3

import sys, os
import h5py
import numpy
import PIL.Image

outdir = "../geo/AHI/optical/20190915"

def Usage():
    print("Usage: %s [-o outdir] ../himawari/downloads/Sept-15-2019/hima8*.png" % sys.argv[0])
    sys.exit(1)

verbose = False
trimfrac = 0 # 180/11000.0

ii = 1
while ii < len(sys.argv) and sys.argv[ii][0] == '-':
    opt = sys.argv[ii]; ii += 1
    if opt == '-o':
        outdir = sys.argv[ii]; ii += 1
    elif opt == '-trim':
        trimfrac = eval( sys.argv[ii] ); ii += 1
    elif opt == '-v':
        verbose = True
    else:
        Usage()

if len(sys.argv) <= ii:
    Usage()

if not os.path.isdir(outdir):
    try:
        os.makedirs(outdir)
    except:
        pass

tossmargin = 180  # at least 168
imagediam = 11000

geocorns = numpy.array( [ [-5500639.4747,5500658.2272 ], [ 5497639.0713,-5497620.3188 ] ] )

imagecorns = numpy.array( [ [ 0,0 ], [ 11000,11000 ] ] )

def trimmed( corns ):
    return numpy.array( [ corns[0]*(1-trimfrac) + corns[1]*trimfrac, corns[0]*trimfrac + corns[1]*(1-trimfrac) ] ).ravel()

for himaimage in sys.argv[ii:]:

    outstem = os.path.join( outdir, os.path.basename( himaimage.replace('.png','') ))
    outimage = outstem + '.tif'

    print("Writing trimmed georeferenced image to %s" % outimage)

    cmd = "gdal_translate -a_srs '+proj=geos +a=6378169 +b=6356583.8 +lon_0=140.7 +h=35785831' -co COMPRESS=LZW -a_ullr %.11g %.11g %.11g %.11g  -srcwin %.11g %.11g %.11g %.11g  %s %s; " % (*trimmed(geocorns), *trimmed(imagecorns), himaimage, outimage)
    if verbose:
        print("# cmd: ", cmd)

    rslt = os.system( cmd )

    if rslt != 0:
        if os.path.exists( outimage ):
            os.unlink( outimage )
        break
