#! /usr/bin/env python3

import sys, os
import datetime
import re
import time

import numpy
import h5py

import matplotlib.transforms   # needed for RSP.make_plots() 
import matplotlib.cm  as cm

from camp2exdb import DatedFiles

from camp2expages import ribbon

import camp2exvis

class APR3( DatedFiles ):

    prefix = 'CAMP2Ex-APR3-L2ZV_P3B'
    # CAMP2Ex-APR3-L2ZV_P3B_20190915_R0_S20190916a022842_E20190916a022935_KUsKAs.h5
    suffix = '.h5'
    datepat = re.compile('%s_(\d\d\d\d\d\d\d\d)_R0_S(\d\d\d\d\d\d\d\d)a(\d\d\d\d\d\d)_E(\d\d\d\d\d\d\d\d)a(\d\d\d\d\d\d)_([KUKAsn]+)(W[sn])\.h5' % prefix)

    maxdwell = 8 / 3600.0  # when assembling interval tree, assume that last file might have data for up to 8 seconds beyond its end time.  Yes only that.

    whatname = "APR3"    # name of data source

    def __init__(self, dirname, Day0yyyymmdd):
        super().__init__( dirname, Day0yyyymmdd )

        # Store the offset (seconds) between our adopted visualization 0-time for this day (self.Day0) after 0h UT on this day.
        self.Day0, self.Day0time0, self.Day0time1 = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

        os.environ['TZ'] = 'UTC0'; time.tzset()
        self.Day0unix = time.mktime(time.strptime(Day0yyyymmdd, '%Y%m%d')) # Unix time of 00:00UTC on date of flight

        self.time0_hours_within_day = (self.Day0time0 - self.Day0).total_seconds() / 3600.0
        self.time1_hours_within_day = (self.Day0time1 - self.Day0).total_seconds() / 3600.0

        self.timerange = None
        self.nadirtimeset = set()
        self.scantimeset = set()

        """
/hires/alt3D             Dataset {456, 1, 8676}
/hires/lat               Dataset {1, 8676}
/hires/lat3D             Dataset {456, 1, 8676}
/hires/lon               Dataset {1, 8676}
/hires/lon3D             Dataset {456, 1, 8676}
/hires/s095n             Dataset {1, 8676}
/hires/scantime          Dataset {1, 8676}
/hires/sig95n            Dataset {456, 1, 8676}
/hires/vel95n            Dataset {456, 1, 8676}
/hires/z95n              Dataset {456, 1, 8676}
/hi2lo/scantime          Dataset {25, 235}
/hi2lo/sig95n            Dataset {456, 25, 235}
/hi2lo/vel95n            Dataset {456, 25, 235}
/hi2lo/z95n              Dataset {456, 25, 235}

/lores/alt3D             Dataset {456, 25, 216}
/lores/lon3D             Dataset {456, 25, 216}
/lores/look_vector       Dataset {3, 25, 216}
/lores/look_vector_nadir Dataset {3, 25, 216}
/lores/look_vector_radar Dataset {3, 25, 216}
/lores/scantime          Dataset {25, 216}
/lores/zhh35             Dataset {456, 25, 216}
/lores/vel35             Dataset {456, 25, 216}

        """
        # data array fragments, indexed by whattime() 
        # For 2-D or 3-D arrays, time must be the _first_ (slow) axis
        self.nadirtimes_h = {}
        self.nadirWdbz = {}
        self.nadirKadbz = {}
        self.nadirKaalt3D = {}
        self.nadirWalt3D = {}
        self.nadiralt3D = {}

        self.scantimes_h = {}
        self.scanKadbz = {}
        self.scanKavel = {}
        self.scanKaalt3D = {}   # /lores/alt3D aircraft of individual radar sample points.   {25, Ntimes}
        self.scan_alt_nav = {} # /lores/alt_nav altitude of aircraft (from GPS?)
        self.scan_roll = {}    # /lores/roll   -- could use this to adjust direction of "down", but is positive roll toward Left or Right?

        self.dbzcmap = camp2exvis.cmap_for( 'apr3' )

        self.scancache_time = None
        self.scancache = None

    ###

    def ymdhms2time_h(self, ymd, hms):
        # APR3 filenames just use standard 24-hour range + yyyymmdd dates, not e.g. "271500" for 3:15h on next day.  So strptime can handle this.
        return ( datetime.datetime.strptime( ymd+hms, "%Y%m%d%H%M%S" ) - self.Day0 ).total_seconds() / 3600.0

    def whattime(self, fname):
        """parses filename, returns floating-point (starttime, endtime) in hours since 0hUTC on Day0.
        Note: these time numbers must be in the same time system as the self.times_h[] values read from inside the datafile in load_data()."""
        # CAMP2EX-RSP1-CLD_P3B_20190915223026_R2.h5
        m = self.datepat.search(fname)
        if m is None:
            return None

    ##datepat = re.compile('%s_(\d\d\d\d\d\d\d\d)_R0_S(\d\d\d\d\d\d\d\d)a(\d\d\d\d\d\d)_E(\d\d\d\d\d\d\d\d)a(\d\d\d\d\d\d)_([KUKAsn]+)(W[sn])\.h5' % prefix)

        tstart_h = self.ymdhms2time_h( m.group(2), m.group(3) )
        tend_h = self.ymdhms2time_h( m.group(4), m.group(5) )

        return (tstart_h, tend_h)


    def gather(self, ddict, trange, timeaxis=0):
        # Lots of short time spans in APR3 data -- only gather those which overlap the needed time range.
        
        def overlap(trA, trB):
            return trA[1] > trB[0] and trA[0] < trB[1]

        ftimes = sorted( [ t0t1 for t0t1 in ddict.keys() if overlap(t0t1, trange) ] )

        if len(ftimes) == 0:
            return numpy.array([]) # or None, but this [] makes calling ribbon() easier.

        
        # are the components masked arrays?
        if hasattr( ddict[ftimes[0]], 'mask' ):
            result = numpy.ma.concatenate( [ ddict[ftime].data for ftime in ftimes ], axis=timeaxis )
        else:
            result = numpy.concatenate( [ ddict[ftime] for ftime in ftimes ], axis=timeaxis )

        return result

    def load_timerange(self, timerange, for_scan=True):
        # find all the data files within the given timerange, in increasing time order, selecting for side-scan, else for nadir only
        for fname in self.files_in_timerange( timerange ):
            if (not for_scan) or ('Ws.h5' in fname):
                self.load_data( fname, for_scan )
            elif for_scan and ('KAs' in fname):
                self.load_data( fname, for_scan )



    def masked_where( self, invalidval, values ):
        if invalidval is None:
            return numpy.ma.masked_array( values, numpy.logical_not( numpy.isfinite(values) ) )
        else:
            return numpy.ma.masked_array( values, (values==invalidval) | numpy.logical_not( numpy.isfinite(values) ) )

    

    def load_data( self, fname, for_scan ):
        (tstart_h, tend_h) = self.whattime(fname)
        ftime = (tstart_h, tend_h)
        filetimeset = [self.nadirtimeset, self.scantimeset][ for_scan ]
        if ftime in filetimeset:
            return None

        filetimeset.add( ftime )
        hf = h5py.File( fname, 'r' )

        try:
            nbeams = hf['params_KUKA/Nbeams'][0,0]
            midbeam = int(nbeams // 2)
        except:
            nbeams = 25
            midbeam = 12
        self.nbeams, self.midbeam = nbeams, midbeam

        if 'params_KUKA/Range_Size_m' in hf:
            self.rangestep_Ka = float(hf['params_KUKA/Range_Size_m'][:])

        hfl = hf['lores']
        if for_scan:
            # Ka band (35 GHz) reflectivity and Doppler velocity
            if 'zhh35' in hfl and 'params_KUKA' in hf:
                self.scanKadbz[ftime] = hfl['zhh35'][:].transpose(2, 0, 1)  # NRange, NBeam, NTime ==> NTime, NRange, NBeam
                self.scanKavel[ftime] = hfl['vel35'][:].transpose(2, 0, 1)  # ditto
                hfpk = hf['params_KUKA']
                if 'AntScanLeft_Deg' in hfpk:
                    beamL, beamR = hfpk['AntScanLeft_Deg'][:], hfpk['AntScanRight_Deg'][:] 
                else:   # Grr, some files have the field with slightly different name
                    beamL, beamR = hfpk['AntScanLeft_deg'][:], hfpk['AntScanRight_deg'][:] 
                self.scanbeamangles = numpy.linspace( float(beamL), float(beamR), num=nbeams )
                self.scantimes_h[ftime] = ( hfl['scantime'][midbeam, :] - self.Day0unix ) / 3600.0
                self.scanKaalt3D[ftime] = hfl['alt3D'][:].transpose(2, 0, 1)  # NRange, NBeam, NTime ==> NTime, NRange, NBeam
                self.scan_alt_nav[ftime] = hfl['alt_nav'][:].transpose()     # NBeam,NTime ==> NTime, NBeam
                self.scan_roll[ftime] = hfl['roll'][:].transpose()           # NBeam,NTime ==> NTime, NBeam
            
        else:
            # nadir data
            hfhl = hf['hi2lo'] if 'hi2lo' in hf else hfl
            if 'zhh35' in hfl:  # some files, named *_Wn.h5, have no Ka-band at all, only nadir W-band.
                self.nadirKadbz[ftime] = hfl['zhh35'][:].transpose(2, 0, 1)[:,:,midbeam]  # Ka-band dBz is /lores/zhh35 ==> NTime, NRange
                self.nadirKaalt3D[ftime] = hfl['alt3D'][:].transpose(2, 0, 1)[:, :, midbeam]

            hfhl_z95 = hfhl['z95n'] if 'z95n' in hfhl else hfhl['z95s']
            self.nadirWdbz[ftime] = hfhl_z95[:].transpose(2, 0, 1)[:,:,midbeam]       # W-band dBz is /hi2lo/z95n if available, else /lores/zhh35  ==> NTime, NRange
            self.nadirtimes_h[ftime] = ( hfhl['scantime'][:][midbeam,:] - self.Day0unix ) / 3600.0
            self.nadirWalt3D[ftime] = hfhl['alt3D'][:].transpose(2, 0, 1)[:, :, midbeam]


    plotnadirlabel = [ 'APR3 W-band nadir',  'APR3 Ka-band nadir' ]
    plotnadiryrange = [ (0,7)                       ] * 2
    plotnadirvrange = [ (-20, 50)                   ] * 2
    plotnadirylabel = [ '$km$'                      ] * 2
    plotnadirvlabel = [ 'Reflectivity (dBz)'        ] * 2

    def make_plot_nadir( self, plotno, onaxis, timerange, tnow, legends=None, do_cbar=False, vthresh=None, do_substitute=False, timemarkopts={}, meshopts={}, cbaropts={} ):
        """APR.make_plot_nadir().  0:nadirWdbz   1:nadirKadbz.   Returns (imageobject, cbarobject) tuple"""

        self.load_timerange( timerange, for_scan=False )

        cbar = None

        yrange = self.plotnadiryrange[plotno]

        extent = (timerange[0], yrange[0], timerange[1], yrange[1])

        onaxis.set_label( self.plotnadirlabel[plotno] )
        onaxis.set_xlim( *timerange )
        onaxis.set_ylim( *yrange )

        if self.plotnadirylabel[plotno] is not None:
            onaxis.set_ylabel( self.plotnadirylabel[plotno] )

        onaxis.set_adjustable('box')

        times = self.gather( self.nadirtimes_h, timerange ) # may be [] if no data in this timerange

        ddata = [ self.nadirWdbz, self.nadirKadbz ][ plotno ]
        altdata = [ self.nadirWalt3D, self.nadirKaalt3D ][ plotno ]
        values = self.gather( ddata, timerange )
        alts = self.gather( altdata, timerange )

        vrange = self.plotnadirvrange[plotno]

        cut_thresh = 15.0 / 3600.0 # cut ribbon at time-series gaps exceeding 15 seconds

        if vthresh is not None:
            # there *MUST* be a cleaner way to do this.  Mask off existing elements of values[] which are < vthresh.
            # that way, pcolormesh() (which ribbon() uses) will make those masked-off cells transparent.
            values = numpy.ma.masked_array( values, numpy.logical_or( numpy.logical_not(numpy.isfinite(values)),  numpy.nan_to_num(values) < vthresh ) )
            #if isinstance(values, numpy.ma.MaskedArray):
            #    values.mask[ values.data < vthresh ] = True
            #else:
            #    values = numpy.ma.masked_array( values, numpy.logical_or( values < vthresh, numpy.logical_not( numpy.isfinite( values ) ) ) )

        implain = ribbon(onaxis, timerange, tnow, vrange, values, alts*0.001, times,
                    cmap=self.dbzcmap,
                    altrange=yrange, do_log10=False,
                    title=self.plotnadirvlabel[plotno],
                    maintitle=self.plotnadirlabel[plotno],
                    do_cbar=do_cbar,
                    cut_thresh=cut_thresh,
                    timemarkopts=timemarkopts, meshopts=meshopts, cbaropts=cbaropts)

        if False and do_cbar:
            cbar = onaxis.figure.colorbar( implain, ax=onaxis, orientation='horizontal' ) # , pad=0.03, fraction=0.13, aspect=5 )
            title = "Reflectivity $(dBz)$"
            if title is not None:
                cbar.ax.set_xlabel( title )

        if implain is None and do_substitute:
            implain = cm.ScalarMappable( cmap=self.dbzcmap )
            implain.set_clim( vrange )

        return (implain, cbar)
        # onaxis.legend(loc='upper right')

    ###

    def build_scan_mesh(self, tsliver, timeix, values):
        # sign of roll: positive roll increases altitude of beam 0, decreases altitude of beam 24

        # sample 80 out of 0..455 (give or take 1) seems to be the location of the radar source.
        rangebase = 80

        valuesnow = values[timeix, rangebase:, :]

        if self.scancache_time == tsliver[0]:
            return (*self.scancache, valuesnow)

        self.scancache_time = tsliver[0]

        self.scancache = None, None

        nrange, nbeam = values.shape[1], values.shape[2]

        xmesh = numpy.empty( (nrange-rangebase, nbeam) )
        ymesh = numpy.empty( (nrange-rangebase, nbeam) )

        # p(r,b,t) = [ 0, alt_nav[t,b] ] + rangestep * (r-rangebase) * [ sin(theta), -cos(theta) ]
        #    where theta = (beam-midbeam)*2 + roll(b,t)   so beam 0 has theta = (roll-2*12) degrees.
        scan_alt_nav = self.gather( self.scan_alt_nav, tsliver )
        scan_roll = self.gather( self.scan_roll, tsliver )
        if scan_alt_nav is None or scan_roll is None:
            return None, None, valuesnow

        alt_b = scan_alt_nav[timeix,:]
        roll_b = scan_roll[timeix,:]
        valsnow = values[timeix,:,:]

        # theta_b = radians( roll_b + (b - 12)*2 )
        # x_b_r = rangestep * (r - 80) * sin(theta_b)
        # y_b_r = alt_b - rangestep * (r - 80) * cos(theta_b)

        rvec = self.rangestep_Ka * numpy.arange( nrange - rangebase )
        theta_b = numpy.radians( roll_b + self.scanbeamangles )
        for b in range(nbeam):
            xmesh[:,b] = rvec * numpy.sin( theta_b[b] )
            ymesh[:,b] = alt_b[b] - rvec * numpy.cos( theta_b[b] )

        self.scancache = xmesh, ymesh

        return xmesh, ymesh, valuesnow


    ### Scan-mode plots
    plotscanlabel = [ 'APR Ka reflectivity $(dBz)$',   'APR Ka Doppler $(m/s)$' ]
    plotscanxlabel= [   "cross-track $(km)$" ] * 2
    plotscanvlabel= [      "$dBz$",                         "$m/s$"             ]

    plotscanyrange =  [  (0,7)               ] * 2
    plotscanxrange =  [  (-4,4)              ] * 2
    plotscanvrange =  [  (-20,50),                            (-7,7)            ]

    plotscancmap   =  [  camp2exvis.cmap_for('apr3'),  camp2exvis.cmap_for('apr3-doppler' ) ]


    def make_plot_scan( self, plotno, onaxis, timerange, tnow, legends=None, cmap=None):
        """APR.make_plot_scan().   0:scanKa_dbz   1:scanKa_vel.   Returns tuple: (imageobject, cbarobject)"""

        self.load_timerange( timerange, for_scan=True )

        pxrange = self.plotscanxrange[plotno]
        pyrange = self.plotscanyrange[plotno]
        vrange = self.plotscanvrange[plotno]

        if cmap is None:
            cmap = self.plotscancmap[plotno]

        extent = (timerange[0], pyrange[0], timerange[1], pyrange[1])

        camp2exvis.axis_set_title_styled( onaxis, self.plotscanlabel[plotno] )
        onaxis.set_xlabel( self.plotscanxlabel[plotno] )

        onaxis.set_xlim( *pxrange )
        onaxis.set_ylim( *pyrange )

        # if self.plotylabel[plotno] is not None:
        #     onaxis.set_ylabel( self.plotylabel[plotno] )


        onaxis.set_adjustable('box')

        tsliver = (tnow, tnow+0.001)

        times = self.gather( self.scantimes_h, tsliver )

        ddata = [ self.scanKadbz, self.scanKavel ][ plotno ]
        values = self.gather( ddata, tsliver )

        j = min( numpy.searchsorted( times, tnow ), len(times)-1 )

        imp = None
        # DEBUG
        if j>=0 and values is not None and tsliver[0] <= times[j] and times[j] <= tsliver[1]:
            try:
                ignore = ("apr scan %d:  gathered %s ix %d maxval %g mean %g" % (plotno, values.shape, j, numpy.nanmax(values[j,:,:]), numpy.nanmean(values[j,:,:])))
            except:
                print("apr scan %d ACK: gathered %s ix %d" % (plotno, values.shape, j))

            meshx, meshy, valsnow = self.build_scan_mesh( tsliver, j, values )

            if meshx is not None:

                imp = onaxis.pcolormesh( 0.001*meshx, 0.001*meshy, valsnow, cmap=cmap, vmin=vrange[0], vmax=vrange[1] )
                

        
        if imp is None:
            imp = matplotlib.cm.ScalarMappable( cmap = cmap )

        onaxis.set_ylabel( "$km$" )
        onaxis.set_xlabel( "cross-track $(km)$" )

        imp.set_clim( vrange )
        cbar = onaxis.figure.colorbar( imp, ax=onaxis, orientation='horizontal', cmap=cmap, pad=0.15, fraction=0.18, aspect=40 )
        cbar.ax.set_xlabel( self.plotscanvlabel[plotno] )

        return (imp, cbar)
