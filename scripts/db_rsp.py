#! /usr/bin/env python3

import sys, os
import datetime
import re

import numpy
import h5py

import matplotlib.transforms   # needed for RSP.make_plots() 

from camp2exdb import DatedFiles

from camp2expages import ribbon

import camp2exvis


class RSP( DatedFiles ):

    ## old style from LARC archive: CAMP2EX-RSP1-CLD_P3B_20190915223026_R2.h5
    ## CAMP2EX-RSP1-CLD_P3B_20190915223026_R2.h5
    # prefix = 'CAMP2EX-RSP1-CLD_P3B'

    ## new style from https://data.giss.nasa.gov/pub/rsp/data/CAMP2Ex/L2CLD/
    ## RSP1-P3_L2-RSPCLOUD-Clouds_20190915T251317Z_V003-20210223T101837Z.h5
    prefix = 'RSP1-P3_L2-RSPCLOUD-Clouds'
    suffix = '.h5'

    datepat = re.compile('%s_(\d\d\d\d)(\d\d)(\d\d)T?(\d\d)(\d\d)(\d\d)' % prefix)

    maxdwell = 20*60 / 3600.0  # when assembling interval tree, assume that last file might have data for up to 20 minutes.

    whatname = "RSP"    # name of data source

    def __init__(self, dirname, Day0yyyymmdd):
        super().__init__( dirname, Day0yyyymmdd )

        # RSP's "/Data/Product_Time_Seconds" is in seconds since 0h UT on start of this flight's day.
        # Store the offset (seconds) between our adopted visualization 0-time for this day (self.Day0) after 0h UT on this day.
        self.time0_secs_within_day = (self.Day0 - datetime.datetime(self.Day0.year, self.Day0.month, self.Day0.day, 0, 0, 0)).total_seconds()
        self.timerange = None
        self.filetimeset = set()

        # data array fragments, indexed by whattime() 
        self.times_h = {}
        self.CloudBiSpec_Reff = {}
        self.CloudBow_Reff = {}
        # self.CloudBow_Reff_byband = {} # unused.
        self.CloudBiSpec_OT = {}
        self.CloudTop = {}
        self.CloudLiquidIndex = {}

        self.wavelength = None


    def whattime(self, fname):
        """parses filename, returns floating-point hours since visualization-time-0 on Day0.
        Note: these time numbers must be in the same time system as the self.times_h[] values read from inside the datafile in load_data()."""
        # CAMP2EX-RSP1-CLD_P3B_20190915223026_R2.h5
        m = self.datepat.search(fname)
        if m is None:
            return None

        y,m,d, H,M,S = [int(v) for v in m.groups()]

        # return floating-point hours since adopted visualization 0-time for start of this file's data
        # For these files the "hour" field might exceed 23, so add the H field separately -- datetime() doesn't want e.g. H=28.
        return (datetime.datetime( y,m,d, 0,M,S ) - self.Day0).total_seconds() / 3600.0 + H


    def gather(self, ddict):
        ftimes = sorted( ddict.keys() )
        if len(ftimes) == 0:
            return None
        # are the components masked arrays?
        if hasattr( ddict[ftimes[0]], 'mask' ):
            data = numpy.concatenate( [ ddict[ftime].data for ftime in ftimes ] )
            mask = numpy.concatenate( [ ddict[ftime].mask for ftime in ftimes ] )
            result = numpy.ma.masked_array( data=data, mask=mask )
        else:
            result = numpy.concatenate( [ ddict[ftime] for ftime in ftimes ] )

        return result

    def load_timerange(self, timerange):
        # find all the data files within the given timerange, in increasing time order
        for fname in self.files_in_timerange( timerange ):
            self.load_data( fname )


    def any_data_during(self, timerange):
        self.load_timerange( timerange )
        values = self.gather(self.CloudBiSpec_OT)

        if values is None:
            return False

        times = self.gather(self.times_h)
        j0, j1 = sorted( [min(j, len(times)-1) for j in numpy.searchsorted( times, timerange, side='left' )] )

        if j0 >= j1:
            return False

        windowvalues = values[j0:j1]
        gotany = not numpy.all( windowvalues.mask )

        if False:  # XXX DEBUG
            windowtimes = times[j0:j1]
            gotrpt = "True range %.3f %.3f" % (windowvalues.min(), windowvalues.max()) if gotany else "False"
            print("rsp any(%.2f, %.2f) gathered (%.2f, %.2f) any: %s" % (*timerange, windowtimes.min(), windowtimes.max(), gotrpt))

        return gotany

    def masked_where( self, invalidval, values ):
        if invalidval is None:
            return numpy.ma.masked_array( values, numpy.logical_not( numpy.isfinite(values) ) )
        else:
            return numpy.ma.masked_array( values, (values==invalidval) | numpy.logical_not( numpy.isfinite(values) ) )

    def load_data( self, fname ):
        ftime = self.whattime(fname)
        if ftime in self.filetimeset:
            return None

        self.filetimeset.add( ftime )
        hf = h5py.File( fname, 'r' )
        hfd = hf['Data']

        self.times_h[ftime] = tchunk = (hfd['Product_Time_Seconds'][:] - self.time0_secs_within_day) / 3600.0  # times in hours since today's visualization-time-0
        # update self.timerange to include this interval.  NOTE we need the times_h[] values to be in the same time system as the whattime() values parsed from filenames.  Beware!
        if self.timerange is None:
            self.timerange = (tchunk[0], tchunk[-1])
        else:
            self.timerange = ( min(self.timerange[0], tchunk[0]), max(self.timerange[1], tchunk[-1]) )

        self.CloudBiSpec_Reff[ftime] = self.masked_where( -999, hfd['Cloud_Bi_Spec_Particle_Effective_Radius'][:,1] )  ## of the Bi_Spec bands, use [1] == 2260nm (second index)
        # self.CloudBow_Reff_byband[ftime] = self.masked_where( -999, hfd['Cloud_Bow_Droplet_Effective_Radius_Bands'][:] ) # unused.
        self.CloudBow_Reff[ftime] = self.masked_where( -999, hfd['Cloud_Bow_Droplet_Effective_Radius'][:] )
        self.CloudBiSpec_OT[ftime] = self.masked_where( -999, hfd['Cloud_Bi_Spec_Optical_Thickness'][:,0] )  # 1590nm
        self.CloudTop[ftime] = self.masked_where( -999, hfd['Cloud_Top_Altitude'][0,:] )     ## of the two Bi_Spec bands, use [0] == 865 nm (first index).  [1] won't do, it's always -999.
        self.CloudLiquidIndex[ftime] = self.masked_where( -999, hfd['Cloud_Liquid_Index'][0,:] )

        if self.wavelength is None:
            self.wavelength = hfd['Wavelength'][:]

    ########### [ self.CloudBiSpec_Reff, self.CloudBow_Reff, self.CloudBiSpec_OT, self.CloudTop ]

    plotlabel = [ '$R_{eff~2260}$',   'CBow $R_{eff}$',     'COT',        'RSP CloudTop$_{865}$' ]
    plotvrange = [ (0,35),             (0,35),              (0,150),           (0,8),            ]
    plottitle = [ 'Reff',              'Reff',              'RSP COT',       'CloudTop alt'      ]
    plotvscale = [ 1,                  1,                   1,                  0.001,           ]
    plotylabel = [ '$\mu{m}$',         '$\mu{m}$',          'COT',              '$km$'           ]
    plotcolor = [ 'g',                 'r',                 ('c','m','b'),      'y'              ]
    plotliqthresh= [ None,             None,                0.3,                None             ]

    def make_plot( self, plotno, onaxis, timerange, tnow, legends=None):
        """RSP.make_plot().  0:R_eff2260  1:CBow R_eff  2:CBow OT_865  3:CloudTop_865
        timerange, tnow in hours since 0h UTC on flight day (not since visualization time0)"""

        self.load_timerange( timerange )

        vrange = self.plotvrange[plotno]
        extent = (timerange[0], vrange[0], timerange[1], vrange[1])

        onaxis.set_label( self.plotlabel[plotno] )
        onaxis.set_xlim( *timerange )
        onaxis.set_ylim( *vrange )

        if self.plotylabel[plotno] is not None:
            onaxis.set_ylabel( self.plotylabel[plotno] )


        onaxis.set_adjustable('box')

        if self.times_h == {}:
            return


        ddata = [ self.CloudBiSpec_Reff, self.CloudBow_Reff, self.CloudBiSpec_OT, self.CloudTop ][ plotno ]
        values = self.gather(ddata) * self.plotvscale[plotno]
        times_h = self.gather(self.times_h) - self.time0_secs_within_day/3600.0

        # these plots are all scalars-as-functions-of-time.   
        bbox = matplotlib.transforms.Bbox.from_extents( *extent )

        label = self.plotlabel[plotno]
        
        liqthresh = self.plotliqthresh[plotno]
        if liqthresh is None:
            artist = onaxis.scatter( times_h, values, s=5, color=self.plotcolor[plotno], label=label, clip_box=bbox, clip_on=True )
        else:
            liquid = self.gather(self.CloudLiquidIndex)
            selpos = (liquid > liqthresh)
            selabel = label + '(liquid)'
            artist = onaxis.scatter( times_h[selpos], values[selpos], s=5, color=self.plotcolor[plotno][1], label=selabel, clip_box=bbox, clip_on=True )
            unselpos = numpy.logical_not( selpos )
            if numpy.any(unselpos):
                unselabel = label + '(ice)'
                onaxis.scatter( times_h[unselpos], values[unselpos], s=5, color=self.plotcolor[plotno][0], label=unselabel, clip_box=bbox, clip_on=True )
            neitherpos = selpos.mask
            if numpy.any(neitherpos):
                neitherlabel = label + '(unknown)'
                onaxis.scatter( times_h[unselpos], values[unselpos], s=5, color=self.plotcolor[plotno][2], label=neitherlabel, clip_box=bbox, clip_on=True, zorder=-0.2 )

        if legends is not None:
            legends.append( (artist,label) )
        else:
            onaxis.legend(loc='upper right')
            

        #if plotno == 2:
        #    print("RSP.make_plot(%d...) time[%d] dt %g  range %.3f .. %.3f using %.3f .. %.3f, values.shape %s (%d non-masked)" % (plotno, len(times_h), timerange[1]-timerange[0], *timerange, times_h[0], times_h[-1], values.shape, numpy.logical_not(values.mask).sum() if hasattr(values, 'mask') else 0))


    def make_plot_Re_ribbon( self, plotno, onaxis, timerange, tnow, cmap='plasma', legends=None ):
        """RSP.make_plot_Re_ribbon()  /Data/Cloud_Bow_Droplet_Effective_Radius_Bands Dataset {444, 9}  /Data/Wavelength {9}"""

        self.load_timerange( timerange )

        plottitle = 'RSP CloudBow $R_{eff} (\\mu{m})$ per-$\\lambda$'
        vrange = (0, 35)   # per-band R_eff ribbon.   Just the one plot for now, so ignore plotno.
        lambdarange = (400-10, 2260+10)

        ##onaxis.set_ylim( self.wavelength[0]-10, self.wavelength[-1]+10 )
        onaxis.set_xlim( *timerange )
        onaxis.set_ylim( *lambdarange )
        if self.wavelength is not None:
            onaxis.set_yticks( self.wavelength )
        onaxis.set_ylabel( "$\lambda (nm)$" )

        onaxis.set_adjustable('box')

        camp2exvis.axis_set_title_styled( onaxis, plottitle )

        if self.times_h == {}:
            return

        values = self.gather( self.CloudBow_Reff_byband )
        times_h = self.gather(self.times_h) - self.time0_secs_within_day/3600.0

        label = plottitle
        artist = ribbon( onaxis, timerange, tnow, vrange, values, self.wavelength, times_h, cmap, title=label )

        if legends is not None:
            legends.append( (artist,label) )
        else:
            onaxis.legend(loc='upper right')
