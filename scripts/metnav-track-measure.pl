#! /usr/bin/perl

if(@ARGV == 0) {
    print STDERR "Usage: $0 ../CAMP2Ex/metnav/CAMP2EX-MetNav*.ict\n";
    exit(1);
}

$maxwlon = $maxwlat = 0;

foreach $f (@ARGV) {

    $lonmin = $latmin = 9999;
    $lonmax = $latmax = -9999;


    if($f =~ /CAMP2EX-MetNav_P3B_(\d\d\d\d\d\d\d\d)_R0.ict/) {
        $datetag = $1;
    } else {
        $datetag = $f;
    }

    open(F, $f);
    while(<F>) {

        @F = split(/,/);

        if($F[0] >= 50000 && $F[0] <= 120000) {
            ($lat,$lon) = @F[2,3];
            $latmax = $lat if $latmax < $lat;
            $lonmax = $lon if $lonmax < $lon;

            $lonmin = $lon if $lonmin > $lon;
            $latmin = $lat if $latmin > $lat;
        }
    }
    $wlon = $lonmax-$lonmin;
    $wlat = $latmax-$latmin;
    if($wlon > 0 && $wlon < 30) {
        printf  "wbox %6.2f %6.2f  cenlon %6.2f cenlat %6.2f  tag %s\n", $wlon,$wlat, 0.5*($lonmin+$lonmax), 0.5*($latmin+$latmax), $datetag;
        $maxwlon = $wlon if $maxwlon < $wlon;
        $maxwlat = $wlat if $maxwlat < $wlat;
    }
}
printf "Overall max wbox %.2f %.2f\n", $maxwlon, $maxwlat;
