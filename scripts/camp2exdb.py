#! /usr/bin/env python3

import sys, os
import datetime

import re
import glob
import PIL.Image

import numpy

from intervaltree.intervaltree import IntervalTree
#from intervaltree.interval     import Interval

import matplotlib.transforms   # needed for RSP.make_plots() 


def jday2date(jday):    # make a datetime object of 0h UTC on the given Jday day.   E.g. 737683 => datetime(2019, 9, 15).
    # Jday zero point is something like Jan 0 of 0 AD.  366+1 days before Jan 1 of 1 AD.
    return datetime.date( 1, 1, 1 ) + datetime.timedelta(days=(jday - 366 - 1))

#####

class DataPack(object):

    loaded = {}     # per-class index of loaded instances

    def __init__(self, fname, datetag):
        if self.__class__ not in self.loaded:
            self.loaded[ self.__class__ ] = {}
        self.loaded[ self.__class__ ][(fname, datetag)] = self

    # partway implementing a per-subclass shared instance cache, indexed by datetag.
    @classmethod
    def withdata( cls, fname, datetag ):
        if cls in cls.loaded and datetag in cls.loaded[cls]:
            return cls.loaded[cls][(fname, datetag)]
        else:
            # create new instance.  Something about __new__(cls, fname, datetag) and using super( ... cls ... ), which will call __init__
            pass
        

class DatedFiles( DataPack ):

    prefix = 'FILENAME_PREFIX'  # within each dir, for glob(prefix+'*'+suffix) file search
    suffix = '.png'             # file suffix within each dir

    datepat = re.compile('%s_(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)' % prefix) # y,m,d,H,M,S

    maxdwell = 15.0 / 3600.0    # time-limit at end of sequence.    Overridden in data-specific subclasses.

    whatname = "WHAT"  # For messages.  subclass should override, e.g. "CPImage"

    

    def ymd2date( self, ymd ):
        """Given a UTC date in yyyymmdd form (as 8-char string, or 8-digit integer, or datetime object), return a datetime object for 0h UTC on that date"""

        if isinstance(ymd, (int, float)) and ymd >= 20000000:
            ymd = "%08d" % ymd

        if isinstance(ymd, str) and len(ymd) >= 8:
            ymd = ymd.replace('-','')  # 2019-09-15 => 20190915
            return datetime.datetime( int(ymd[0:4]), int(ymd[4:6]), int(ymd[6:8]), 0, 0, 0 )

        if instance(ymd, datetime.datetime):
            return ymd
        raise ValueError("ymd2date(): expected yyyymmdd as string or integer or datetime; what's %s ?" % repr(ymd))

    def __init__(self, dirname, Day0yyyymmdd):

        super().__init__(dirname, Day0yyyymmdd)

        self.dirname = dirname

        self.Day0 = self.ymd2date( Day0yyyymmdd )

        self.inti = IntervalTree()

        self.buildtree( dirname )

        # one-entry static image cache  [or should this be specific to subclass?]
        self.last_imname = None
        self.last_impixels = None

    def whattime(self, fname):
        """Return time of this file -- or (starttime, endtime) tuple/list -- generally in 'time_h' form, float hours since 0h UTC"""
        raise ValueError("DatedFiles subclass %s must provide whattime() to parse time from filename %s" % ( self.__class__.__name__, fname ) )

    def buildtree(self, dirname):
        globpattern = os.path.join( dirname, self.prefix + '*' + self.suffix )
        nt = [ (name, self.whattime(name)) for name in glob.glob(globpattern) ]
        nt = [ (name,time) for (name,time) in nt if time is not None ]
        nt.sort( key=lambda nametime: nametime[1] )
        if len(nt) == 0:
            print("%s.buildtree(): can't find any files named %s" % ( self.__class__.__name__, globpattern ))
            return

        if isinstance(nt[0][1], (list,tuple)):
            # If the subclass wants dwell time, it should add it into the span returned by whattime
            for name, timespan in nt:
                self.inti.addi( timespan[0], timespan[1], name )
        else:
            prevname, prevtime = nt[0]
            for name, time in nt[1:]:
                endtime = min( time, prevtime + self.maxdwell )
                if endtime > prevtime:
                    self.inti.addi( prevtime, endtime, prevname )
                prevname, prevtime = name, time
            # add final image for one dwell time
            self.inti.addi( prevtime, prevtime + self.maxdwell, prevname )

    def file_at_time( self, time ):
        s = self.inti.at( time )
        if len(s) > 0:
            iminterval = max( s )   # choose the highest (latest-starting) interval that includes this time point
            return iminterval.data
        else:
            return None

    def files_in_timerange( self, timerange ):
        ss = self.inti.overlap(timerange[0], timerange[1])
        return [s.data for s in sorted(list(ss))]  # ordered by time

    def image_at_time( self, time ):
        """Returns <height> x <width> x 3 numpy array of bytes, or None."""
        imname = self.file_at_time(time)
        if imname is None:
            print("# No %s at time %g = %02d%02d%02d" % (self.whatname, time, int(time), int((time-int(time))*60), int(time*3600)%60))
            return None

        if self.last_imname == imname:
            return self.last_impixels

        self.last_imname = imname
        self.last_impixels = None
        im = PIL.Image.open(imname)

        if im.mode != 'RGB':
            im = im.convert(mode='RGB')
        w, h = im.width, im.height
        rgb = numpy.frombuffer( im.tobytes(), dtype=numpy.uint8 ).reshape( h, w, 3 )

        self.last_impixels = rgb
        print("# Found %dx%d %s %s at time %g = %02d%02d%02d" % (w,h, self.whatname, imname, time, int(time), int((time-int(time))*60), int(time*3600)%60))
        return rgb
    
#####
