#! /usr/bin/env python3

import sys, os
import re
import numpy
import datetime
import pykml.parser

import shapely.geometry
import shapely.affinity

import camp2exvis

from camp2expages import hmstick

import matplotlib

class SatTrack(object):

    namepat = re.compile('\(([^()]+)\) for Overpass (\S+) of (.*)')

    satlabel_bbox_opts = dict(boxstyle='round', facecolor='DarkSlateGray', alpha=0.65)


    fadetime_h  = 0.06
    extendby = 30       # (in degrees) amount to widen 90-degree-rotated path by to construct swath-slicing box

    @staticmethod
    def name_from( ksat ):
        return str(ksat.name).replace('Track ','').replace('Swath ','')

    @staticmethod
    def namekey_from( ksat ):
        name = SatTrack.name_from( ksat )
        m = SatTrack.namepat.search(name)
        if m:
            passno, satname = m.group(2), m.group(3)
            return "%s # %s" % (satname, passno)
        else:
            return name

    def sattimeto0h(self, s): # "20190916 00:04:20 UTC"
        
        dt = datetime.datetime.strptime( s, "%Y%m%d %H:%M:%S UTC" )
        return (dt - self.sattracks.Day0).total_seconds() / 3600.0


    def __init__(self, sattracks, ksat, trackno ):

        self.sattracks = sattracks
        self.trackno = trackno

        name = self.name_from(ksat)
        self.namekey = self.namekey_from(ksat)
        m = self.namepat.search(name)
        if m:
            self.color, self.passno, self.satname = m.groups()
        else:
            self.color = 'pink'
            self.passno = '00'
            self.satname = name

        # shortened satellite name, omitting instruments.
        #  E.g. TERRA-ASTER-MISR-MODIS => TERRA, but SENTINEL-3A-OLCI-... => SENTINEL-3A
        sas = self.satname.split('-')
        self.sat_shortname = "-".join( sas[0:2] ) if sas[0] in ('SENTINEL','CYGNSS') else sas[0]

        descrip = str( ksat.Placemark[0].description )
        marks = [ pm for pm in ksat.Placemark ][1:]
        self.lonlats = numpy.fromstring( " ".join( [ str(pm.Point.coordinates) for pm in marks ] ).replace(',',' '), sep=' ' ).reshape(-1,2)
            # each point has a timestamp in "yyyymmdd hh:mm:ss UTC" in "name"
        self.times_h = numpy.array( [ self.sattimeto0h( str(pm.name) ) for pm in marks ] )
        self.timerange = numpy.array( [ self.times_h[0], self.times_h[-1] ] )

        self.southbound = ( self.lonlats[0,1] > self.lonlats[-1,1] )

        trackext = numpy.empty( (len(self.lonlats)+2, 2) )
        trackext[1:-1,:] = self.lonlats
        tvec = (self.lonlats[-1] - self.lonlats[0])
        tunit = tvec / numpy.sqrt( numpy.square(tvec).sum() )
        trackext[0] = trackext[1] - self.extendby*tunit
        trackext[-1] = trackext[-2] + self.extendby*tunit

        self.sgtrack = shapely.geometry.LineString( self.lonlats )
        self.sgexttrack = shapely.geometry.LineString( trackext )
        self.sgextperptrack = shapely.affinity.rotate( self.sgexttrack, 90, origin=tuple(self.lonlats[0]) )

        self.swaths = []


    def add_swath( self, ksatsw ):
        swathdesc = str(ksatsw.description)
        outlinestr = str(ksatsw.Polygon.outerBoundaryIs.LinearRing.coordinates)
        swathoutline = numpy.fromstring( outlinestr.replace(',',' '), sep=' ' ).reshape(-1,3) # lon lat 0 ("3D")
        swathpoly = shapely.geometry.polygon.Polygon( swathoutline )
        self.swaths.append( dict(description=swathdesc, sgpoly=swathpoly) )

    def during_time( self, start_h, end_h ):
        return start_h < self.timerange[1] and self.timerange[0] < end_h

    def frameno_of(self, time_h):
        return self.sattracks.frameno_of(time_h)

    def plotpoly(self, ax, sgobj, tcenter=None, zorder_nudge=0, **fillargs):
        try:
            if 'Multi' in sgobj.geom_type:
                for geom in sgobj:
                    self.plotpoly( ax, geom, tcenter=tcenter, zorder_nudge=zorder_nudge, **fillargs )
            elif hasattr(sgobj, 'exterior'):
                xs, ys = sgobj.exterior.xy
                ax.fill( xs, ys, zorder=1+zorder_nudge, **fillargs )
            elif not sgobj.is_empty:
                xs, ys = sgobj.coords.xy
                ax.plot( xs, ys, zorder=1+zorder_nudge, **fillargs )
        except NotImplementedError as e:
            print("***ERROR [tcenter %s frame %s]: SatTrack.plotpoly failed: %s -- sgobj %s is_valid %s dir %s" % (tcenter, self.frameno_of(tcenter), e, sgobj, sgobj.is_valid, dir(sgobj)))

    def color_rgba(self):
        try:
            c_main   =  matplotlib.colors.to_rgba( self.color.replace(' ','') )
        except:
            c_main   =  (0.8, 0.4, 0.2, 1)
        return numpy.array( c_main )

    def make_plot_track( self, plotno, onaxis, trange, tcenter, clipbox, zorder_nudge, legends=None, **kwargs ):
        """plot track on (cartopy) onaxis"""

        # cases:
        # tcenter < satobj.timerange[0] : sat hasn't reached area yet.
        # satobj.timerange[0] <= tcenter <= satobj.timerange[1] : part of track has been seen, part hasn't yet
        # satobj.timerange[1] < tcenter : satellite has passed on

        c_notyet =   numpy.array( matplotlib.colors.to_rgba( '#66666699' ) )
        c_main = self.color_rgba()
        c_seen      = c_main
        c_soon      = 0.75*c_main
        c_gone      = numpy.array( matplotlib.colors.to_rgba( '#88888899' ) )

        clight_limit = 0.5

        lw_seen = 4
        lw_soon = 2 

        did_plot = False
        plotcolor = None

        # if drawing swaths, default opacity depends on how many overlain swaths this satellite has
        alpha_per_swath = 0.35 if len(self.swaths) <= 1 else 0.80 / len(self.swaths)

        # if it's a really bright color (lightness > limit), make it more transparent
        # Full yellow = 0.89, TERRA red = 0.59
        clight = c_main[0]*0.3 + c_main[1]*0.59 + c_main[2]*0.11  # lightness from RGB
        if clight > clight_limit:
            alpha_per_swath *= clight_limit / clight
        

        if tcenter <= self.timerange[0]:
            # print("NOTYET %s timetill %g" % (self.satname, self.timerange[0]-tcenter))
            # Swaths: Don't draw swaths for satellites that haven't yet passed over
            if False:
                # Should we even draw tracks for satellites that haven't yet started passing over?   Maybe not.
                onaxis.plot( self.lonlats[:,0], self.lonlats[:,1], linewidth=2, label=self.satname, color=c_notyet )

        elif tcenter < self.timerange[1]:
            # split into two pieces according to where tcenter lies along the curve
            # determine marker for sub-satellite point (slon, slat)

            npoints = len(self.times_h)

            slon = numpy.interp(tcenter, self.times_h, self.lonlats[:,0])
            slat = numpy.interp(tcenter, self.times_h, self.lonlats[:,1])
            pno = numpy.interp(tcenter, self.times_h, numpy.arange(npoints))

            ipno = int(pno)
            mappoints = numpy.insert(self.lonlats, ipno+1, [slon, slat], axis=0)

            onaxis.plot( mappoints[0:ipno+2,0], mappoints[0:ipno+2,1], color=c_seen, linewidth=lw_seen, label=self.satname, zorder=0.8+zorder_nudge )
            onaxis.plot( mappoints[ipno:,0],    mappoints[ipno:,1],    color=c_soon, linewidth=lw_soon, zorder=0.7+zorder_nudge )
            onaxis.plot( slon, slat, marker='*', markersize=15, color=c_main, zorder=0.9+zorder_nudge )

            plotcolor = c_seen


            # What has satellite seen so far?  Build slice box, with edge perpendicular to track
            sgslice = shapely.affinity.translate( self.sgextperptrack, slon-self.lonlats[0,0], slat-self.lonlats[0,1] )

            looppts = sgslice.coords[:] + self.sgextperptrack.coords[::-1] + [ sgslice.coords[0] ]
            sgslicebox = shapely.geometry.polygon.Polygon( looppts )

            if not sgslicebox.is_valid:
                print("Trouble: invalid slice box")
            
            for swath in self.swaths:

                sgseen = swath['sgpoly'].intersection( sgslicebox )
                sgunseen = swath['sgpoly'].difference( sgslicebox )
                self.plotpoly( onaxis, sgseen, tcenter=tcenter, alpha=alpha_per_swath, zorder_nudge=zorder_nudge, facecolor=c_seen )
                self.plotpoly( onaxis, sgunseen, tcenter=tcenter, alpha=0.25*alpha_per_swath, zorder_nudge=zorder_nudge, facecolor=c_soon )
                did_plot = True

        else:

            # tell me how long the train's been gone
            ago = (tcenter - self.timerange[1])
            frac = numpy.exp( -ago / self.fadetime_h )
            #c_faded = frac*c_seen + (1-frac)*c_gone
            c_faded = c_seen  # try this way
            c_faded[3] = frac*c_seen[3]  # make more-transparent, too
            onaxis.plot( self.lonlats[:,0], self.lonlats[:,1], color=c_faded, linewidth=lw_seen, zorder=0.6 )

            if c_faded[3] > 0.06:
                print("FADING %s frame %d ago %g frac %g c_faded %.3f %.3f %.3f %g" % (self.satname, self.frameno_of(tcenter), ago, frac, *c_faded))
                for swath in self.swaths:
                    ##self.plotpoly( onaxis, swath['sgpoly'], tcenter=tcenter, alpha=c_faded[3]*0.80*alpha_per_swath, facecolor=c_faded )
                    self.plotpoly( onaxis, swath['sgpoly'], tcenter=tcenter, alpha=c_faded[3]*alpha_per_swath, zorder_nudge=zorder_nudge, facecolor=c_faded )
                    did_plot = True
                    plotcolor = c_faded
        
        if legends is not None and plotcolor is not None:
            artist = matplotlib.patches.Patch(color=plotcolor, label=self.satname)
            legends.append( (artist, self.sat_shortname) )

        return did_plot


class SatTracks(object):

    def __init__(self, fname, Day0yyyymmdd):

        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd )
        zero, self.timebase_h_0h, self.timeend_h_0h = camp2exvis.timerange_from( Day0yyyymmdd, 'h_0h' )

        with open(fname, 'r') as pkf:
            par = pykml.parser.parse(pkf)
            pdoc = par.getroot().Document

            daytracks = [fo for fo in pdoc.Folder if str(fo.name).lower().startswith('DAY Tracks'.lower())]
            if len(daytracks)!=1:
                raise ValueError("Can't find DAY Tracks in " + fname)
            self.sat = {}
            wanted_set = set()
            for trackno, sat in enumerate( daytracks[0].Folder ):
                # Pulls name, "description", points and times from KML satellite "DAY Track" node
                satobj = SatTrack( self, sat, trackno )

                self.sat[ satobj.namekey ] = satobj
                if satobj.during_time( self.timebase_h_0h, self.timeend_h_0h ):
                    wanted_set.add( satobj )

            self.wanted_sat = sorted( list(wanted_set), key = lambda satobj: satobj.timerange[0] )

            dayswaths = [fo for fo in pdoc.Folder if str(fo.name).lower().startswith('DAY Swaths'.lower())]
            if len(dayswaths)==1:
                for satsw in dayswaths[0].Placemark:
                    namekey = SatTrack.namekey_from( satsw )
                    if namekey in self.sat:
                        self.sat[namekey].add_swath( satsw ) # there might be multiple swaths per track
                    else:
                        print("SatTracks(): Warning: no corresponding Track for DAY Swath %s" % str(satsw.name))

    def frameno_of(self, time_h):
        if time_h is None:
            return None
        return (time_h - self.timebase_h_0h) / camp2exvis.time_incr
                
    def ticklist(self):
        """Return dict of lists: 'time_h' times (in hours since 0h UTC), 'tag' brief labels (S01, S02...), 'color' (r,g,b,a) tuple, 'fullname' string"""
        tags = []; times = []; colors = []; fullnames = []

        senpat = re.compile('SENTINEL-(\d\w?)')
        cygpat = re.compile('CYGNSS-(\d+)')
        for satobj in self.wanted_sat:
            m = senpat.match( satobj.satname )
            cm = cygpat.match( satobj.satname )
            if m:
                tag = "S%s" % m.group(1)    # SENTINEL-2A -> S2A
            elif cm:
                tag = "CYG%d" % int(cm.group(1))
            elif 'LANDSAT8' in satobj.satname:
                tag = 'LS8'
            else:
                tag = satobj.satname[0:4].replace('-','')   # else first 4 letters, omitting dashes
            tags.append(tag)
            times.append(0.5*(satobj.timerange[0] + satobj.timerange[1]))
            colors.append(satobj.color_rgba())
            fullnames.append(satobj.satname)

        return { 'time_h':times, 'tag':tags, 'color':colors, 'fullname':fullnames }

    def make_plot(self, plotno, onaxis, trange, tcenter, legends=None, **kwargs ):

        x0, x1 = onaxis.get_xlim()
        y0, y1 = onaxis.get_ylim()
        clipbox = matplotlib.transforms.Bbox.from_extents( x0,y0, x1,y1 )

        zorder_nudge = 0
        for satobj in self.wanted_sat:
            did_plot = satobj.make_plot_track(plotno, onaxis, trange, tcenter, clipbox, legends=legends, zorder_nudge=zorder_nudge)
            if did_plot:
                zorder_nudge += 0.01


def ll2sphere(lons, lats, alts=0, R_sphere=camp2exvis.R_earth, basepoint=camp2exvis.basepoint):
    """given two N-element lists in degrees, and optional altitude for each, return Nx3 array of points on sphere of radius R_sphere"""
    flat = numpy.radians( numpy.ravel(lats) )
    flon = numpy.radians( numpy.ravel(lons) )
    coslat = numpy.cos( flat )
    sinlon = numpy.sin( flon )
    coslon = numpy.cos( flon )
    mesh = numpy.empty( shape=(flat.size, 3) )
    R_with = R_sphere + alts
    mesh[:,0] = R_with * coslon * coslat - basepoint[0]
    mesh[:,1] = R_with * sinlon * coslat - basepoint[1]
    mesh[:,2] = R_with * numpy.sin(flat) - basepoint[2]
    return mesh


if __name__ == "__main__":

    sys.path.append('/fe3/demmapping/scripts')
    import bgeo


    ymd = None # '20190915'
    trackwidth = 5
    outname = None
    html_out = None

    fromframe, toframe = -1e8, 1e8

    ii = 1

    def Usage():
        print("""Usage: %s [-ymd yyyymmdd] [-o sattracks.bgeo] [-w trackwidth] [-f fromframe-toframe] [-html out.html]
Process satellite tracks for given date, report visualization frame numbers for each overflight.
With -f, only keep tracks if overflight happens during that frame range.
With -o, also write .bgeo output of ribbon with given trackwidth (in km) (default -w 5).
Each point of ribbon has a 'frameno' attribute
Each ribbon (Nx2 mesh primitive) has a 'trackno' per-primitive attribute
E.g.:  %s -ymd 20190915 -o sattracks.bgeo -w 5""" % (sys.argv[0], sys.argv[0]))
        sys.exit(1)


    while ii < len(sys.argv):
        opt = sys.argv[ii]; ii += 1
        if opt == '-ymd':
            ymd = sys.argv[ii]; ii += 1
        elif opt == '-o':
            outname = sys.argv[ii]; ii += 1
        elif opt == '-w':
            trackwidth = float( sys.argv[ii] ); ii += 1
        elif opt == '-f':
            fromframe, toframe = [ int(s) for s in sys.argv[ii].replace('-',' ').split() ]; ii += 1
        elif opt == '-html':
            html_out = sys.argv[ii]; ii += 1
        else:
            if opt != '-h':
                print("Unknown option: ", opt)
            Usage()
    
    if ymd is None:
        Usage()


    sattracks = SatTracks( camp2exvis.sattrackdata[ ymd ], ymd )

    timebase_h = (sattracks.timebase - sattracks.Day0).total_seconds() / 3600.0

    meshes_f = []   # list of N_on_track x K x 3 XYZ (K=1 or maybe K=2).  Each mesh is treated as a polygon.
    times_f = []    # list of N_on_track x K x 1 attribute values for each point.   Just one attribute: frameno.
    tracknos = []
    framev0 = []
    framev1 = []
    colors = []

    htmlf = None
    if html_out is not None:
        tlist = sattracks.ticklist()

        htmlf = open(html_out, 'w')
        print("<table border=1>", file=htmlf)
        print("<caption><b>RF09 Satellite overpasses<br>(2019-09-15 00:00UTC = time 00:00:00)</b></caption>", file=htmlf)
        print("<tr><th>Color <th>Time <th>Abbrev <th>Full name", file=htmlf)

        scolor = [ "#%02X%02X%02X" % (int(c[0]*255.999),int(c[1]*255.99),int(c[2]*255.99)) for c in tlist['color'] ]
        stime  = [ hmstick( time, 0 ) for time in tlist['time_h'] ]
        stag   = [ s if len(s)==4 else s+"&nbsp;" for s in tlist['tag'] ]
        sname  = tlist['fullname']

        print("<tr>", file=htmlf)
        print("<td>\n\t" + "\n\t".join(["<span style=\"background:%s\">&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; </span><br>" % color for color in scolor]), file=htmlf)
        print("<td>\n\t" + "\n\t".join(["&nbsp; %s &nbsp;<br>" % s for s in stime]), file=htmlf)
        print("<td>\n\t" + "\n\t".join(["&nbsp; %s <br>" % s for s in stag]), file=htmlf)
        print("<td>\n\t" + "\n\t".join(["&nbsp; %s &nbsp;<br>" % s for s in sname]), file=htmlf)
        
        print("</table>", file=htmlf)
    
    # ordered by time
    for satobj in sorted( sattracks.sat.values(), key = lambda satobj: satobj.timerange[0] ):
        frange0, frange1 = (satobj.timerange - timebase_h) / camp2exvis.time_incr
        if frange0 < fromframe or frange1 > toframe:
            continue        # ignore if beyond time limits
        lons, lats = satobj.lonlats[:,0], satobj.lonlats[:,1]
        numpts = len(lons)
        mpts = ll2sphere( lons, lats, R_sphere=1, basepoint=(0,0,0) )
        vvec = numpy.empty( (numpts, 3) )
        vvec[:-1] = mpts[1:] - mpts[:-1]
        vvec[-1] = vvec[-2]
        uvec = numpy.cross( mpts, vvec )
        # normalize and scale to track width (as fraction of Earth's radius)
        uvec *= (0.5*trackwidth / camp2exvis.R_earth) / numpy.sqrt( numpy.square(uvec).sum(axis=1) ).reshape(-1,1)
        # construct ribbon across track
        mpts2 = numpy.empty( ( numpts, 2, 3 ) )
        mpts2[:,0,:] = mpts - uvec
        mpts2[:,1,:] = mpts + uvec
        mpts2 *= camp2exvis.R_earth / numpy.sqrt( numpy.square(mpts2).sum(axis=2) ).reshape(numpts,2,1)
        mpts2 -= camp2exvis.basepoint.reshape(1,1,3)

        # Construct corresponding time-per-point
        frameno = (satobj.times_h - timebase_h) / camp2exvis.time_incr # convert from hours to animation-frames
        frameno = frameno.reshape( (numpts, 1, 1) )                    # one value per row in v-direction of ribbon
        frameno = numpy.broadcast_to( frameno, (numpts, 2, 1) )        # broadcast to one value per mesh vertex

        meshes_f.append( mpts2 )  # add an N x 2 x 3 mesh (strip of width 'trackwidth' along track)
        times_f.append( frameno ) # add corresponding N x 2 x 1 array of per-point attributes (frame number for that frame)
        framev0.append( frameno[0,0,0] )
        framev1.append( frameno[-1,0,0] )

        tracknos.append( satobj.trackno ) # heh.  Each mesh has a per-primitive 'trackno' attribute.
        colors.append( satobj.color_rgba()[0:3] )

        hhmmss = hmstick(numpy.mean(satobj.timerange),0)
        print("Trackno %2d [%s] (%d): frames %05d .. %05d / %s [%.3f %.3f %.3f] -- %s" % (satobj.trackno, "NS"[int(satobj.southbound)], len(satobj.swaths), frameno.min(), frameno.max(), hhmmss, *(satobj.color_rgba()[0:3]), satobj.namekey))



    if outname is not None:
        polyattrnames = [ 'trackno', 'framev0', 'framev1', ('Ctrack',3) ]
        pointattrnames = [ 'frameno' ]

        # Concatenate all the per-point attr arrays (one for each per-track mesh) into one big lump
        pointattrs = numpy.concatenate( times_f, axis=0 ).reshape( -1, len(pointattrnames) )
        polyattrs = numpy.empty( (len(tracknos), 6) )
        polyattrs[:,0] = tracknos
        polyattrs[:,1] = framev0
        polyattrs[:,2] = framev1
        polyattrs[:,3:6] = colors

        detailattrnames = [ 'yyyy', 'mmdd', 'trackwidth', ('basepoint',3) ]
        detailattrs = [ sattracks.Day0.year,
                        sattracks.Day0.month*100 + sattracks.Day0.day,
                        trackwidth,
                        camp2exvis.basepoint ]

        bgeo.BGeoPolyWriter( outname, polyattrnames=polyattrnames, polyattrs=polyattrs, polyverts=meshes_f, pointattrnames=pointattrnames, pointattrs=pointattrs, detailattrnames=detailattrnames, detailattrs=detailattrs )

        print("")
        print("Wrote %d satellite tracks to %s" % (len(meshes_f), outname))
