#! /usr/bin/env python3

import sys, os
import datetime

import h5py
import numpy

import matplotlib.transforms    # for Bbox

import ict

import camp2exvis

import cartopy.crs
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter

class MergedScalars(object):

    scalarsets = {}

    MISSING = -999999

    def __init__(self, filename, Day0yyyymmdd):

        if filename in self.scalarsets:
            # short-circuit.   Make shallow copy
            self.__dict__.update(self.scalarsets[filename].__dict__)
            return

            
        self.scalarsets[filename] = self

        self.Day0, self.Day0time0, self.Day0time1 = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )
        #self.Day0 = datetime.datetime( self.Day0time0.year, self.Day0time0.month, self.Day0time0.day, 0, 0, 0 )

        self.datetag = Day0yyyymmdd

        self.time0_hours_within_day = (self.Day0time0 - self.Day0).total_seconds() / 3600.0
        self.time1_hours_within_day = (self.Day0time1 - self.Day0).total_seconds() / 3600.0

        self.times = None
        self.timerange = None
        self.hf = None

        if filename.endswith('.ict'):
            h5name = filename.replace('.ict','.h5')
            print("For speediest results, run ict2h5.py %s %s  and call MergedScalars('%s') instead of on this .ict file" % (filename, h5name, h5name))
            with open(filename, 'r') as ictf:
                ictdr = ict.ICTReader( ictf )
                self.headlines = ictdr.headlines
                self.rawfieldnames = ictdr.fieldnames
                self.fieldnames = [s.strip() for s in self.rawfieldnames]

                data = []
                for row in ictdr:
                    vrow = numpy.zeros( len(self.rawfieldnames) )
                    for i, field in enumerate(self.rawfieldnames):
                        try:
                            vrow[i] = float(row[field])
                        except:
                            vrow[i] = self.MISSING
                    data.append(vrow)

                data = numpy.array(data)

                self.fieldata = {}
                for fldno, field in enumerate(self.fieldnames):
                    self.fieldata[field] = numpy.ma.masked_array( data[:, fldno], data[:, fldno] == self.MISSING )

        else:
            self.hf = h5py.File( filename, 'r' )
            self.fieldata = dict( self.hf.items() )  # inhale hdf5 references to all the data (but don't load data yet)
            self.fieldnames = list( self.hf.keys() )

        if 'Time_Start' in self.fieldata:
            self.times = self.fieldata['Time_Start'][:] / 3600.0  # timescale: hours since 0h UTC on Day0 date
            self.timerange = (self.times[0], self.times[-1])

    def __getitem__(self, fieldname):
        if fieldname not in self.fieldata:
            if fieldname.strip() in self.fieldata:
                fieldname = fieldname.strip()
            else:
                raise IndexError("No field named %s" % fieldname)

        val = self.fieldata[fieldname]
        if not isinstance(val, numpy.ndarray):
            # Lazy access: turn HDF5 dataset reference into numpy data (masked-array) when first referenced.
            val = val[:]
            val = numpy.ma.masked_array( val, val == self.MISSING )
            self.fieldata[fieldname] = val

        return val

    def __contains__(self, fieldname):
        return fieldname.strip() in self.fieldata

    def keys(self):
        return self.fieldata.keys()


    DegFormatter = matplotlib.ticker.FuncFormatter( lambda deg, etc: "$%g\degree$" % deg )
    DegCFormatter = matplotlib.ticker.FuncFormatter( lambda deg, etc: "$%g\degree C$" % deg )

    ###

    plotfield =  [ 'Total_Air_Temp_YANG', 'Dew_Point_YANG', 'GPS_Altitude_YANG', 'Pitch_Angle_YANG', 'Roll_Angle_YANG' ]  # XXX where is yaw?
    plotvrange=  [   (-25,37),              (-25,37),           (0,7),     (-7,7),       (-7,7)    ]
    plotvscale=  [     1,                     1,                 0.001,       1,           1       ]
    plottitle =  [ '',        'MetNav\nTemp, Dewpoint, Altitude',  '',        '',     'MetNav Pitch/Roll'  ]
    plotcolor =  [   None,                   None,               'r',         None,       None     ]
    plotlabel =  [ 'Temperature',          'Dew point',       'GPS Altitude', 'Pitch',    'Roll'   ]
    plotylabel=  [     '',                    '',              '$km$',          '',         ''     ]

    def make_plot_metnav( self, plotno, onaxis, trange, tcenter, legends=None, loc=None ):
        """MergedScalars.make_plot_metnav()  0:Temp 1:Dewpoint 2:Altitude 3:Pitch 4:Roll"""

        values = self[ self.plotfield[plotno] ] * self.plotvscale[plotno]

        if 'Roll' in self.plotfield[plotno]:
            values[ values>180 ] -= 360.0       # small negative roll angles get recorded as just below 360 degrees

        vrange = self.plotvrange[plotno]
        extent = ( trange[0], vrange[0], trange[1], vrange[1] )
        bbox = matplotlib.transforms.Bbox.from_extents( *extent )

        ## Don't do this here ## camp2exvis.axis_set_title_styled( onaxis, self.plottitle[plotno] )  # or plt.rcParams['axes.titley'] = 1.0 and ['axes.titlepad'] = -14 -- put title just below top of plot box
        onaxis.set_xlim( *trange )
        onaxis.set_ylim( *vrange )

        TempDew   = (plotno in [0,1])
        PitchRoll = (plotno in [3,4])

        if TempDew:       # Temp, Dewpoint
            ##onaxis.yaxis.set_major_formatter( self.DegCFormatter )
            onaxis.yaxis.set_major_formatter( self.DegFormatter )
        elif PitchRoll:   # Pitch, Roll
            onaxis.yaxis.set_major_formatter( self.DegFormatter )

        onaxis.set_adjustable('box')

        label = self.plotlabel[plotno]
        color = self.plotcolor[plotno]
        if PitchRoll:
            artist = onaxis.scatter( self.times, values, s=2, c=color, label=label, clip_box=extent, clip_on=True )  ## color=self.plotcolor[plotno],
        else:
            artist = onaxis.plot( self.times, values, c=color, label=label, clip_box=extent, clip_on=True )  ## color=self.plotcolor[plotno],

        if legends is not None:
            legends.append( (artist,label) )
        else:
            onaxis.legend( loc=('upper right' if loc is None else loc) )

        onaxis.set_ylabel( self.plotylabel[plotno] )


    ###

    hmstick = lambda hours, etc: "%02d:%02d:%02d" % (int(hours), int(60*(hours-int(hours))), (3600*hours+0.5)%60)
    HMSFormatter = matplotlib.ticker.FuncFormatter( hmstick )


    gasfield =  [
        'RHw_DLH_DISKIN',
        'O3_ppbv_DISKIN',   # maybe add NO, NOx, NOy, SO2?   But their ranges are much smaller than O3's...
    ]

    gasvrange = [ (0,120), (0,100), ]

    gasleg    = [ 'upper left', 'upper right', ]

    gascolor  = [ None, 'r' ]

    gastitle = [ '$Rel Hum$',  '$O_3$', ]

    gasylabel = [ '$\%$',    '$ppbv$' ]


    def make_plot_somegas( self, plotno, onaxis, trange, tcenter, legends=None ):
        """MergedScalars.make_plot_somegas()  0:RH  1:O_3"""

        gasfield = self.gasfield[plotno]

        lefttick = numpy.ceil( trange[0] * (3600/60) ) / (3600/60)  # round up to nearest 60 seconds
        # plant ticks at 5-second intervals, but not too far to the right to keep margin stable
        onaxis.set_xticks( numpy.arange( lefttick, trange[1]-0.5/60, 60/3600.0 ) )

        onaxis.xaxis.set_major_formatter( self.HMSFormatter )

        values = self[ gasfield ]
        vrange = self.gasvrange[plotno]
        extent = ( trange[0], vrange[0], trange[1], vrange[1] )
        bbox = matplotlib.transforms.Bbox.from_extents( *extent )

        ###onaxis.set_title( self.tracetitle[plotno] )
        onaxis.set_xlim( *trange )
        onaxis.set_ylim( *vrange )

        onaxis.set_adjustable('box')

        label = self.gastitle[plotno]
        artist = onaxis.scatter( self.times, values, s=5, c=self.gascolor[plotno], label=label, clip_box=extent, clip_on=True )

        if legends is not None:
            legends.append( ( artist, self.gastitle[plotno] ) )
        else:
            onaxis.legend( loc=self.gasleg[plotno] )  # legend is at upper right, except low-level trace gases at upper left

        onaxis.set_ylabel( self.gasylabel[ plotno ] )


    tracevrange = [ (0,100) ]

    traceleg    = [ 'upper left' ]

    tracefield = [
        'Org_Ave_IsoK_STP_ZIEMBA',
        'SO4_Ave_IsoK_STP_ZIEMBA',
        'NO3_Ave_IsoK_STP_ZIEMBA',
        'NH4_Ave_IsoK_STP_ZIEMBA',
        'Chl_Ave_IsoK_STP_ZIEMBA',
      ]

    tracecolor  = [ "green",  "red", "blue", "orange", "purple" ]  # Org = green; SO4 = red; NO3 = blue; NH4 = orange; Chl = purple, says Ziemba

    tracelabel = [ 'Org',   '$SO_4$', '$NO_3$', '$NH_4$', '$Chl$' ]

    traceylabel = [ '$\mu{g}/m^3$ at STP' ]

    def make_plot_tracegas( self, plotno, onaxis, trange, tcenter, legends=None ):
        """MergedScalars.make_plot_tracegas()  0:RH  1:O_3  2:Org  3:SO_4  4:NO_3  5:NH_4  6:Chl"""

        values = [ self[fieldname] for fieldname in self.tracefield ]

        lefttick = numpy.ceil( trange[0] * (3600/60) ) / (3600/60)  # round up to nearest 60 seconds
        # plant ticks at 5-second intervals, but not too far to the right to keep margin stable
        onaxis.set_xticks( numpy.arange( lefttick, trange[1]-0.5/60, 60/3600.0 ) )

        onaxis.xaxis.set_major_formatter( self.HMSFormatter )

        vrange = self.tracevrange[plotno]
        extent = ( trange[0], vrange[0], trange[1], vrange[1] )
        bbox = matplotlib.transforms.Bbox.from_extents( *extent )

        ###onaxis.set_title( self.tracetitle[plotno] )
        onaxis.set_xlim( *trange )
        onaxis.set_ylim( *vrange )

        onaxis.set_adjustable('box')

        artist = onaxis.stackplot( self.times, values, colors=self.tracecolor, labels=self.tracelabel )

        if legends is not None:
            legends.append( ( artist, "5TraceGases" ) )
        else:
            onaxis.legend( loc=self.traceleg[plotno] )  # legend is at upper left

        onaxis.set_ylabel( self.traceylabel[ plotno ] )


    def make_plot_aod( self, plotno, onax, trange, tcenter, legends=None ):

        j0, jnow, j1 = numpy.searchsorted( self.times, [trange[0], tcenter, trange[1]], side='left' )
        j0, j1 = sorted( [j0,j1] )
        
        j0, jnow, j1 = [min(j, len(self.times)-1) for j in (j0, jnow, j1)]

        t0 = self.times[j0]
        t1 = self.times[j1]


        tau_var = [ 'tau_c_0532_VAN_DIEDENHOVEN', 'tau_f_0532_VAN_DIEDENHOVEN' ]
        #tau_label = [ 'RSP $\\tau_{coarse 532}$',   'RSP $\\tau_{fine 532}$',     'RSP $\\tau_{total 532}$' ]
        tau_label = [ 'RSP $\\tau_{532}coarse$',   'RSP $\\tau_{532}fine$',     'RSP $\\tau_{532}total$' ]

        tau_title = 'Aerosol Optical Thickness'

        vrange = (0, 2)

        tau_total = None

        for varname, label in zip(tau_var, tau_label):
            values = self[ varname ][j0:j1]
            artist = onax.scatter( self.times[j0:j1], values, s=5, label=label )
            tau_total = values if tau_total is None else values+tau_total
            if legends is not None:
                legends.append( (artist, label) )

        label = tau_label[2]
        artist = onax.scatter( self.times[j0:j1], tau_total, s=5, label=label )
        if legends is not None:
            legends.append( (artist, label) )

        onax.set_ylabel( '$AOD (\\tau)$' )
        onax.set_xlim( *trange )
        onax.set_ylim( *vrange )
        
        camp2exvis.axis_set_title_styled( onax, tau_title )

        onax.set_adjustable('box')

        if legends is None:
            onax.legend(loc='upper right')

    LARGEfield = [ 'nLAS_ZIEMBA', 'nAPS_ZIEMBA', 'U_ms-1_THORNHILL', 'V_ms-1_THORNHILL', 'w_ms-1_THORNHILL' ]  # yes lowercase w_ms...
    LARGEvrange= [  (0,10000),     (0,10000),      (-50,50),            (-50,50),            (-10,10)       ]
    LARGEylabel= [  '$n/cm^3$',     '$n/cm^3$',     '$m/s$',             '$m/s$',             '$m/s$'       ]
    LARGEcolor=  [   'y',             'm',            'r',                'g',                  'c'         ]
    LARGElabel=  [  'nLAS',          'nAPS',        'U wind',          'V wind',             'W wind'       ]
    LARGElinthresh= [  None,           None,          None,               None,                 1.0         ]

    def make_plot_LARGE( self, plotno, onax, trange, tcenter, legends=None ):
        """0: nLAS  1: nAPS   2: Uwind  3: Vwind  4: Wwind"""

        j0, jnow, j1 = numpy.searchsorted( self.times, [trange[0], tcenter, trange[1]], side='left' )
        j0, j1 = sorted( [j0,j1] )
        
        j0, jnow, j1 = [min(j, len(self.times)-1) for j in (j0, jnow, j1)]

        t0 = self.times[j0]
        t1 = self.times[j1]

        onax.set_xlim( *trange )
        onax.set_ylim( *self.LARGEvrange[ plotno ] )

        if self.LARGElinthresh[plotno] is not None:   # W wind
            onax.set_yscale( 'symlog', linthreshy=self.LARGElinthresh[plotno], linscaley=1.0 )
            yticks = [0, 0.5, 1, 3, 10]
            yticks = [ -v for v in yticks[:0:-1] ] + yticks
            ymticks = [0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 0.9, 2, 4, 5, 6, 7, 8, 9]
            ymticks = [-v for v in ymticks[::-1] ] + ymticks
            onax.set_yticks( yticks )
            onax.set_yticklabels( ["%g" % v for v in yticks] )
            onax.set_yticks( ymticks, minor=True )

        # onax.set_title( self.LABELtitle, y=0.95, pad=-14 )

        if j0 >= j1:
            return

        times = self.times[j0:j1]
        values = self[ self.LARGEfield[plotno] ][j0:j1]

        onax.scatter( times, values, s=2, c=self.LARGEcolor[plotno], label=self.LARGElabel[plotno] )

        onax.set_adjustable('box')
        onax.legend( loc='upper right' )

    ###

    Dgasfield =   [ 'CO_ppm_DISKIN', 'CH4_ppm_DISKIN'  ]
    Dgasvrange=   [  (0.04,1.0),        (1.8,2.1)      ]
    Dgasyscale =  [   'log',            'linear'       ]
    Dgasylabel =  [  '$ppm$ (log)',      '$ppm$'       ]
    Dgascolor  =  [   'y',                 'r'         ]
    Dgaslabel  =  [    'CO',             '$CH_4$'      ]

    def make_plot_DISKIN( self, plotno, onax, trange, tcenter, legends=None ):
        """0: CO  1: CH4"""

        j0, jnow, j1 = numpy.searchsorted( self.times, [trange[0], tcenter, trange[1]], side='left' )
        j0, j1 = sorted( [j0,j1] )

        j0, jnow, j1 = [min(j, len(self.times)-1) for j in (j0, jnow, j1)]

        t0 = self.times[j0]
        t1 = self.times[j1]

        onax.set_xlim( *trange )
        onax.set_ylim( *self.Dgasvrange[ plotno ] )

        values = self[ self.Dgasfield[plotno] ] 

        onax.set_yscale( self.Dgasyscale[plotno] )

        onax.set_ylabel( self.Dgasylabel[plotno] )
    
        if j0 < j1:
            label = self.Dgaslabel[plotno]
            artist = onax.scatter( self.times[j0:j1], values[j0:j1], c=self.Dgascolor[plotno], s=3, label=label )
            if legends is not None:
                legends.append( (artist, label) )


    # Sizes in nm of LAS Bin01..Bin26.  Copied from header of ../CAMP2Ex/FIMS/CAMP2EX-LARGE-LAS_P3B_20190915_R0.ict
    LASbinsizes = [ 100.0, 112.2, 125.9, 141.3, 158.5, 177.8, 199.5, 223.9, 251.2, 281.8, 316.2, 354.8, 398.1, 446.7, 501.2, 562.3, 631.0, 707.9, 794.3, 891.3, 1000.0, 1258.9, 1584.9, 1995.3, 2511.9, 3162.3 ]

    bspad = numpy.array( [LASbinsizes[0]] + LASbinsizes + [LASbinsizes[-1]] )
    LASbintops = 0.5*(bspad[1:] + bspad[:-1])
    LASbinbots = LASbintops[:-1]
    LASbinbots[0] -= 0.5*(LASbinbots[1]-LASbinbots[0])
    LASbinwidths = LASbintops[1:] - LASbintops[:-1]


    def make_plot_LAS( self, plotno, onax, trange, tcenter, color='b', label='LAS', legends=None ):

        j = min( numpy.searchsorted( self.times, tcenter ), len(self.times)-1 )


        onax.set_xscale( 'log' )
        onax.set_yscale( 'log' )
        onax.set_xlim( self.LASbinbots[0], self.LASbintops[-1] )
        onax.set_ylim( 1.0e-1, 1.0e4 )

        timethresh = 0.005 # hours
        if (abs( self.times[j] - tcenter ) < timethresh):
            fimsraw = numpy.array( [ self["LAS_Bin%02d_ZIEMBA" % (b+1)][j] for b in range(26) ] )
            fimsbins = numpy.ma.masked_array( fimsraw, fimsraw < 0 )
            if numpy.nanmax(fimsbins) > 0:
                onax.bar( self.LASbinbots, fimsbins, width=self.LASbinwidths, linewidth=2, align='edge', color=(0,0,0,0), edgecolor=color, alpha=0.67 )
            ##onax.plot( self.LASbinsizes, fimsbins, color=color, linewidth=2, label=label.replace('@', 'size dist') )

        if legends is not None:
            artist = matplotlib.lines.Line2D([], [], color=color, label=label)
            legends.append( (artist,label) )

        onax.set_ylabel( '$n/cm^3$' )
        onax.set_xlabel( 'size $(nm)$' )

        camp2exvis.axis_set_title_styled( onax, 'FIMS/LAS pcle size dist' )

    ####

    def make_plot_track( self, plotno, onax, trange, tcenter, legends=None, retitle=None, titleopts={} ):
        """Plot 2-D track curve on a map.   Determines extent of map from ... either a fattened bounding box around the track, or config data from camp2exvis."""

        timepoints = [ self.time0_hours_within_day, trange[0], tcenter, trange[1], self.time1_hours_within_day ]

        jstart, j0, jnow, j1, jend = [min(j, len(self.times)-1) for j in numpy.searchsorted( self.times, timepoints, side='left' )]
        j0, j1 = sorted( [j0,j1] )


        t0 = self.times[j0]
        t1 = self.times[j1]

        lons = self['Longitude_YANG']
        lats = self['Latitude_YANG']
        heading = self['True_Heading_YANG']
        altitude = self['GPS_Altitude_YANG']
        sza = self['SZA_SCHMIDT']   # solar zenith-angle

        if False:
            onax.plot( lons[jstart:jnow], lats[jstart:jnow], self.times[jstart:jnow], color='yellow', zorder=2.5 )
            onax.plot( lons[jnow], lats[jnow], self.times[jnow], '*', zorder=2.8 )
            onax.plot( lons[jnow:jend], lats[jnow:jend], self.times[jnow:jend], color='green', zorder=2.4 )
        elif False:
            # This works OK, but we want more
            onax.plot( lons[jstart:jnow], lats[jstart:jnow], color='yellow', zorder=2.5 )
            onax.plot( lons[jnow], lats[jnow], '*', zorder=2.8 )
            onax.plot( lons[jnow:jend], lats[jnow:jend], color='green', zorder=2.4 )
        else:
            # Draw the curve, with the currently-in-animation-window segment emphasized.
            # For some reason plot() doesn't provide a way to draw a curve whose color varies along its length,
            # so we'll make do with lots of colored dots.

            cmap = matplotlib.cm.viridis
            s_past = 0.5
            s_future = 0.3
            
            s_emph = 10.0
            s_moreemph = 18.0

            vrange = (0.0, 7.0)
            onax.scatter( lons[j1:jend:4], lats[j1:jend:4], s=s_future, c=altitude[j1:jend:4]*0.001, cmap=cmap, vmin=vrange[0], vmax=vrange[1], zorder=2.2 )
            onax.scatter( lons[jstart:j0], lats[jstart:j0], s=s_past, c=altitude[jstart:j0]*0.001, cmap=cmap, vmin=vrange[0], vmax=vrange[1], zorder=2.4 )
            onax.scatter( lons[j0:jnow], lats[j0:jnow], s=s_moreemph, c=altitude[j0:jnow]*0.001, cmap=cmap, vmin=vrange[0], vmax=vrange[1], zorder=2.6 )
            onax.scatter( lons[jnow:j1], lats[jnow:j1], s=s_emph, c=altitude[jnow:j1]*0.001, cmap=cmap, vmin=vrange[0], vmax=vrange[1], zorder=2.6 )
            onax.plot( lons[jnow], lats[jnow], self.times[jnow], '*', zorder=2.8 )
            
            

        head_r = numpy.radians( heading[jnow] )

        planearrow = 0.55
        planearrowwidth = 1.0

        # arrow x = sin(heading), y=cos(heading), since heading=0 points north (+Y) instead of east (+X) and it increases CW not CCW.
        onax.add_patch( matplotlib.patches.Arrow( lons[jnow], lats[jnow], planearrow*numpy.sin(head_r), planearrow*numpy.cos(head_r), width=planearrow*planearrowwidth, color='r', zorder=2.7 ) )

        def widen( v0, v1, frac ):
            delta = (v1 - v0) * frac
            return (v0 - delta, v1 + delta)

        widen_by = 0.05

        cenlon, cenlat = camp2exvis.trackboxcen[self.datetag]
        wlon, wlat = camp2exvis.trackwbox
        lon0,lat0, lon1,lat1 = cenlon-0.5*wlon,cenlat-0.5*wlat,  cenlon+0.5*wlon,cenlat+0.5*wlat

        extent = ( *widen(lon0, lon1, widen_by), *widen(lat0, lat1, widen_by) )

        platecarree = cartopy.crs.PlateCarree()
        onax.set_extent( extent, crs=platecarree )


        lonticks = numpy.arange( int(extent[0]), int(extent[1])+2, 2.0 )
        latticks = numpy.arange( int(extent[2]), int(extent[3])+2, 2.0 )

        if False:
            # Disable gridlines.    They *were* cute though.
            onax.gridlines( xlocs=lonticks, ylocs=latticks, crs=platecarree, color=(.6, .4, .2, .6) )

        onax.set_xticks( lonticks, crs=platecarree )
        onax.set_yticks( latticks, crs=platecarree )

        bbox = matplotlib.transforms.Bbox.from_extents( *extent )
        onax.set_clip_box( bbox )

        def tidyfunc( deg, etc ):
            return "%g" % deg

        # special formatters for carto tick marks, from cartopy.mpl.ticker
        lon_formatter = matplotlib.ticker.FuncFormatter( tidyfunc ) # LongitudeFormatter( zero_direction_label=True )
        lat_formatter = matplotlib.ticker.FuncFormatter( tidyfunc ) # LatitudeFormatter()

        onax.xaxis.set_major_formatter( lon_formatter )
        onax.yaxis.set_major_formatter( lat_formatter )

        datenow = self.Day0 + datetime.timedelta( hours=tcenter )

        kwtitle = { 'y':0.82, 'pad':-14, 'loc':'left' }   # default title placement, overlying plot just below top center
        kwtitle.update( titleopts )
        if 'title' in kwtitle:
            title = kwtitle.pop('title')
        else:
            sza_now = 90 if hasattr(sza,'mask') and sza.mask[jnow] else sza[jnow]  # tricky.  Can't just print sza[jnow], lest that give an error if it's masked, as before sunrise.
            title = "%s\n%.3fE %.3fN  alt %4dm\nT %06d  SZA %2d  heading %03d" % ( datenow.strftime("%Y-%m-%d %H:%M:%S"), lons[jnow], lats[jnow], altitude[jnow], int(0.5+tcenter*3600), sza_now, heading[jnow] )
        if 'pretitle' in kwtitle:
            title = kwtitle.pop('pretitle') + title

        camp2exvis.axis_set_title_styled( onax, title, **kwtitle )

        


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("Usage: %s merged_datafile.ict_or_h5" % sys.argv[0])
        sys.exit(1)

    import re
    pat = re.compile('_(\d\d\d\d\d\d\d\d)')

    filename = sys.argv[1]
    m = pat.search(filename)
    if m is None:
        print("Can't guess yyyymmdd date from input filename %s" % filename)
        sys.exit(1)

    ymdate = m.group(1)
    ms = MergedScalars( filename, ymdate )
    for field in ms.fieldnames:
        print("\t" + field)
