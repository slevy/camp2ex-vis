#! /usr/bin/env python3

import sys, os
import h5py
import numpy
import PIL.Image

outdir = "../data/AHI/tb11"

def Usage():
    print("Usage: %s [-o outdir] ../CAMP2Ex/AHI/NC_H08_20190916_0150_R21_FLDK*.nc" % sys.argv[0])
    sys.exit(1)

ii = 1
while ii < len(sys.argv) and sys.argv[ii][0] == '-':
    opt = sys.argv[ii]; ii += 1
    if opt == '-o':
        outdir = sys.argv[ii]; ii += 1
    else:
        Usage()

if len(sys.argv) <= ii:
    Usage()

for ncf in sys.argv[ii:]:
    hf = h5py.File(ncf, 'r')
    tb11 = hf['tbb_11'][:]
    lons = hf['longitude'][:]
    lats = hf['latitude'][:]

    # tb is encoded as a signed 16bit int.  ncdump -h reports it's in temperature units:
    #    0 degrees C = 16-bit 0, and scaling of 0.01 C per int16 unit.
    # The scaling below maps this to a 16-bit unsigned int, with 0 C = 32767
    # and range of -81.92 C = uint16 0, +81.92 C = uint16 65535.
    # put tb of 0.0 in the middle of the range (32767/65535), and scale so data range covers most of 16-bit range.  I assume this is a relative brightness-temperature measurement, with some (arbitrary?) value assigned to 0
    tb11pos = numpy.clip( tb11*(65535/16384.0) + 32767.0, 0, 65535 ).astype(numpy.uint16)
    imp = PIL.Image.frombytes('I;16', (2401,2401), tb11pos.tobytes())
    outstem = os.path.join( outdir, os.path.basename( ncf.replace('.nc','') ))
    imp.save( outstem + '.raw.tif' )
    print("Writing to %s.tif with %d 0-pixels and %d 65535-pixels" % (outstem, (tb11pos==0).sum(), (tb11pos==65535).sum()))
    rslt = os.system( "gdal_translate -co COMPRESS=LZW -co PREDICTOR=2 -a_srs '+proj=longlat +datum=WGS84 +no_defs' -a_ullr %.10g %.10g  %.10g %.10g  %s.raw.tif %s.pos.tif && rm %s.raw.tif" % (lons[0],lats[0], lons[-1],lats[-1], outstem, outstem, outstem) )
    if rslt != 0:
        break
