#! /usr/bin/env python3

import sys, os

import h5py
import numpy

import camp2exvis

import matplotlib.pyplot as mp

class SSFR(object):
    """
    SSFR zenith (down) -- zen_flux for y, zen_wvl for x         
    SSFR nadir  (up)   -- nad_flux for y, nad_wvl for x         
    tmhr for time 
    with y range of 0 .. 2.25 and x range of say 350 .. 2200

    flux = time vs wavelength

    Time        shape = (38412)             range = 19.2  .. 29.9   (we want 21.71666667 to 29.71666667)
    nad_flux    shape = (38412, 424)        range = -0.88 .. 2.28   (watch out for NaNs)
    zen_flux    shape = (38412, 424)        range = 352.7 .. 2196.5
    nad_wvl     shape = (424)               range = -0.11 .. 3.08   (watch out for NaNs)
    zen_wvl     shape = (424)               range = 351.8 .. 2198.0

    """

    def __init__(self, fname, Day0yyyymmdd):

        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

        # time (hours) from 0h UTC on flight day, to visualization-frame 00000
        self.start_frame_hours = (self.timebase - self.Day0).total_seconds() / 3600.0  # tmhr of frame 0

        self.stemname = "ssfr"
        self.fname = fname
        self.datetag = fname[-14:-6]
        self.hf = h5py.File(fname, 'r')
        self.times = self.hf['tmhr'][:]
        self.nad_flux = self.hf['nad_flux'][:]
        self.nad_wvl  = self.hf['nad_wvl'][:]
        self.zen_flux = self.hf['zen_flux'][:]
        self.zen_wvl  = self.hf['zen_wvl'][:]

    def hour2index(self, timenow):
        """Returns closest index to current time (in hours from 0h UTC on flight day). (Maybe we'll want to change this to rounding down to the nearest index?)"""
        idx = (numpy.abs(self.times - timenow)).argmin()
        return idx

    def frame2index(self, curr_frame, start_frame=0):
        """Returns closest index to current frame. (Maybe we'll want to change this to rounding down to the nearest index?)"""

        # Convert frame to time (in hours)
        seconds_elapsed = curr_frame - start_frame
        hours_elapsed = seconds_elapsed / 60.0 / 60.0
        curr_hour = self.start_frame_hours + hours_elapsed

        # Convert time to index
        idx = self.hour2index(curr_hour)

        return idx

    y_range_plot = (0, 2.25)
    x_range_plot = (300, 2200)

    def make_plot(self, plotno, axis, trange, timenow, legends=None):

        # there is only one plot - ignore plotno
        # we only display the current instant - ignore trange

        # Convert timenow to closest tmhr index
        timeidx = self.hour2index(timenow)
        curr_zen_flux = self.zen_flux[timeidx]
        curr_nad_flux = self.nad_flux[timeidx]

        # Set plot axes
        axis.set_xlim( self.x_range_plot )
        axis.set_ylim( self.y_range_plot )

        # Draw plot lines
        artistu = axis.scatter(self.zen_wvl, curr_zen_flux, s=1, c='c', label="SSFR Zenith")
        artistd = axis.scatter(self.nad_wvl, curr_nad_flux, s=1, c='r', label="SSFR Nadir")

        # Set labels
        camp2exvis.axis_set_title_styled( axis, "Solar Spectral Flux\nRadiometer (SSFR)" )
        axis.set_xlabel("Wavelength ($nm$)")
        axis.set_ylabel("Flux ($Wm^{-2}nm^{-1}$)") # Is this right? Units copied from Larry's slide

        # Set legend

        if legends is not None:
            legends.append( (artistu, "SSFR Zenith (up)") )
            legends.append( (artistd, "SSFR Nadir (down)") )
        else:
            legend = axis.legend(loc="upper right")


