#! /usr/bin/env python3

import sys, os
from intervaltree.intervaltree import IntervalTree
from intervaltree.interval import Interval
import pickle
import glob

# Save/Load a time-indexed list of stuff.
# Stuff will generally be data-type-specific objects, holding keys to the source data -- not the values of the source data themselves.
# The per-dataset time indexes get pickled for fast access.


class DGroup(object):
    def __init__(self, groupname):
        self.groupname = groupname
        self.itimes = IntervalTree()

    def addi(self, tbegin, tend, tdata=None):
        self.itimes.addi( tbegin, tend, tdata )

    def timerange(self):
        return self.itimes.begin(), self.itimes.end()

    def during(self, tbegin, tend, frequency ):
        pieces = sorted( list( self.itimes.overlap(tbegin, tend) ) )
        return pieces # or maybe call something on the data objects for each piece?  Or maybe our subclasses can override during() to do that
