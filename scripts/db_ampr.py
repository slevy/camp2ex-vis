#! /usr/bin/env python3

import sys, os
import datetime
import time
import numpy

import h5py

import pyampr

from camp2expages import ribbon

import camp2exvis

import matplotlib.ticker

class AMPR(object):

    nplots = 5

    plotlabels = [ "$T_{b} 37 (K)$",  "$T_{b} 85 (K)$",   "$T_{b} 10 (K)$", "$T_{b} 19 (K)$", "Surface Wind $(m/s)$",  "LWP $(kg/m^2)$" ]   # AMPR microwave temperatures (K) at 37, 85, 10, and 19 GHz, and liquid water content
#   vranges =   [   (160,200),     (230,300),      (100,200),      (0, 1.5)     ]
    vranges =   [   (160,330),     (250,320),      (100,300),    (150,320),                      (0,15),                  (0,1.5)     ]
    do_log10 =  [     False,         False,          False,         False,                       False,                      False      ]

    anglerange = [ 35, -35 ]

    # AMPR wind should only be believed during high altitude, level flight.  Set thresholds.
    wind_alt_thresh = 4000  # high altitude?  How high is high?  XXX
    wind_pitch_thresh = 2
    wind_roll_thresh = 2

    DegFormatter = matplotlib.ticker.FuncFormatter( lambda deg, etc: "$%g\degree$" % deg )

    def __init__(self, fname, Day0yyyymmdd='ignored', needgeo=False):
        amprhf = h5py.File( fname, 'r' )    # now re-open it directly, to read non-pyampr 'atmosphere_cloud_liquid_water_content' field.
        raw_LWC = amprhf['atmosphere_cloud_liquid_water_content'][:]   # "LWC" = cloud liquid water content in mm, which is about equal to kg/m^2.
        raw_amprwind = amprhf['wind_speed_near_surface'][:] # (AlongTrackDim, CrossTrackDim)
        # wind
        #altitude = amprhf['GPSAltitude'][:]
        #pitch = amprhf['Pitch'][:]
        #roll = amprhf['Roll'][:]
        level_flight_flag = amprhf['level_flight_flag'][:]
        nadir_flag = amprhf['NadirFlag'][:]
        QC = amprhf['QC'][:]
        QC2 = numpy.max( QC, axis=(0,1) )   # Discard where QC > 3
        wind_invalid = numpy.logical_not( level_flight_flag )

        def masked_invalid( v, QCthresh=None, wind_invalid=wind_invalid ):
            if QCthresh is None:
                return numpy.ma.masked_array( v, (wind_invalid | nadir_flag).reshape(-1,1) | numpy.logical_not( numpy.isfinite(v) ) )
            else:
                return numpy.ma.masked_array( v, (wind_invalid | nadir_flag).reshape(-1,1) | (QC2>QCthresh) | numpy.logical_not( numpy.isfinite(v) ) )

        #self.amprwind = masked_invalid( raw_amprwind, QCthresh=2 )

        # always apply level_flight_flag filtering to AMPR 2D wind
        self.amprwind = masked_invalid( raw_amprwind )

        # possibly don't apply level_flight_flag to other fields, depending on env var AMPRNOFILTER
        if os.getenv('AMPRNOFILTER') is not None:
            wind_invalid = numpy.array([0])   # ignore level_flight_flag if AMPRNOFILTER is set.

        self.LWC  = masked_invalid( raw_LWC )
        print("amprwind: max %g mean %g" % (numpy.nanmax(self.amprwind), numpy.nanmean(self.amprwind)))

        if needgeo:
            self.meshlon = amprhf['/Lon'][:,:] # nscans, swathangle
            self.meshlat = amprhf['/Lat'][:,:] # nscans, swathangle
            self.heading = amprhf['/Head'][:]

        amprhf.close() # We need to close this before re-opening it below, or HDF5 library will choke later.  Sigh.

        amprtb = pyampr.AmprTb( fname, 'CAMP2EX' )

        # copy the needed values out - don't leave the AMPR file open after __init__()
        self.swath_angle = amprtb.swath_angle

        # Show raw brightness temperatures even if level_flight_flag isn't set.  But still suppress if values are invalid, or if in nadir scan mode. 
        self.tb37 = masked_invalid( 0.5 * (amprtb.TB37H + amprtb.TB37V), wind_invalid=0 )
        self.tb85 = masked_invalid( 0.5 * (amprtb.TB85H + amprtb.TB85V), wind_invalid=0 )
        self.tb10 = masked_invalid( 0.5 * (amprtb.TB10H + amprtb.TB10V), wind_invalid=0 )
        self.tb19 = masked_invalid( 0.5 * (amprtb.TB19H + amprtb.TB19V), wind_invalid=0 )

        ymdbase = "%04d%02d%02d" % (amprtb.Year[0], amprtb.Month[0], amprtb.Day[0])
        self.Day0, self.timebase, self.endtime = camp2exvis.timerange_from( ymdbase, 'datetime' )

        # times[] is an array of floating-point hours since this day's timebase.
        os.environ['TZ'] = 'UTC0'; time.tzset()  # need to do this or strftime('%s') won't give correct answer.
        time_since_epoch = float( self.timebase.strftime('%s') ) # %s => seconds since UNIX epoch of 1970.0 to this day's visualization timebase
        self.times = (amprtb.netcdfTimes[:] - time_since_epoch) / 3600.0

        self.time0_secs_within_day = (self.timebase - self.Day0).total_seconds()

        del amprtb  # close this HDF file (opened indirectly via pyampr)


# 'TB10A', 'TB10B', 'TB10H', 'TB10V', 'TB19A', 'TB19B', 'TB19H', 'TB19V', 'TB37A', 'TB37B', 'TB37H', 'TB37V', 'TB85A', 'TB85B', 'TB85H', 'TB85V', 

    def make_plot(self, plotno, onaxis, timerange, tnow, cmap='plasma', legends=None, cbaropts={}):
        """AMPR make_plot.  0:37GHz  1:85GHz  2:10GHz  3:19GHz 4:surfacewind (ribbons) 5:LWP liquid water (curveplot)"""

        values = [ self.tb37, self.tb85, self.tb10, self.tb19, self.amprwind, self.LWC ][plotno]

        times_since_0h = self.times + self.time0_secs_within_day / 3600.0

        maintitle = 'AMPR %s' % self.plotlabels[plotno]
        title = maintitle.replace('AMPR ','').replace(' (K)','')

        artist = ribbon( onaxis, timerange, tnow, \
                vrange=self.vranges[plotno], values=values, \
                altrange=self.anglerange, alts=self.swath_angle, 
                times=times_since_0h, \
                cmap=cmap, do_log10=self.do_log10[plotno], aspectscale=None, \
                title=title, maintitle=maintitle,
                cbaropts=cbaropts )

        if legends is not None:
            legends.append( (artist,title) )

        onaxis.set_yticks( [-35, -20, 0, 20, 35], minor=False )
        onaxis.set_yticks( numpy.arange( -30, 30+1, 10 ), minor=True )

        #if self.plotylabels[plotno] is not None:
        #    onaxis.set_ylabel( self.plotylabels[plotno] )

        onaxis.yaxis.set_major_formatter( self.DegFormatter )

    def make_plot_RGB(self, plotno, onaxis, timerange, tnow, legends=None, chancodes=None, axmatch=None):
        """AMPR make_plot.  0:37GHz  1:85GHz  2:10GHz  3:19GHz (ribbons) 4:LWC liquid water (curveplot)"""

        valsrc  = {37:self.tb37,     85:self.tb85,      10:self.tb10,    19:self.tb19}
        vranges = {37:(160,330),     85:(250,320),      10:(100,300),    19:(150,320)}

        ccodes = [ 19, 37, 85 ] if chancodes is None else chancodes

        times_since_0h = self.times + self.time0_secs_within_day / 3600.0

        title = 'AMPR RGB ~ %d,%d,%d GHz Tb' % tuple(ccodes)

        j0, j1 = sorted( [min(j, len(times_since_0h)-1) for j in numpy.searchsorted( times_since_0h, timerange ) ] )

        if j0 < j1:
            def slicedchan(chancode, j0, j1):
                v0, v1 = vranges[chancode]
                return numpy.clip( (valsrc[chancode][j0:j1] - v0) / (v1 - v0), 0.0, 1.0 )

            j0 = max(j0-1,0) # include one extra sample if available, to avoid "bobble" at left edge of ribbon
            valslice = numpy.stack( [ slicedchan(chancode, j0, j1) for chancode in ccodes ], axis=-1 )

            ## artist = onaxis.pcolormesh( times_since_0h[j0:j1], self.swath_angle, valslice, vmin=0,vmax=1 )
            extent = (times_since_0h[j0], self.swath_angle[-1], times_since_0h[j1], self.swath_angle[0])
            #XXX This isn't right.   imshow assumes uniform spacing, which for AMPR is true in scan-angle but is *NOT* true in time.   Need to resample to a uniform grid to get this right... or possibly search for blocks with nearly-uniform time intervals and render each block separately.
            #artist = onaxis.imshow( valslice, aspect='auto', extent=extent )
            #print("ampr rgb extent %g %g  %g %g j0 j1 %d %d" % (extent + (j0,j1)))

            valslice **= 0.45   # gamma-shift to lighten dark things

            artist = onaxis.imshow( valslice[:, ::-1, :].transpose(1, 0, 2), aspect='auto', extent=extent, origin='upper' )
            onaxis.set_xlabel( 'time (approx)' )

        #onaxis.set_xlim( *timerange )
        #onaxis.set_ylim( self.swath_angle[0], self.swath_angle[-1] )
        camp2exvis.axis_set_title_styled( onaxis, title )
        
        #if self.plotylabels[plotno] is not None:
        #    onaxis.set_ylabel( self.plotylabels[plotno] )

        onaxis.yaxis.set_major_formatter( self.DegFormatter )

        if axmatch is not None:
            otherxywh = axmatch.get_position().bounds
            myxywh = onaxis.get_position().bounds

            # match the X-position of the other axis, keep our Y-position.
            onaxis.set_position( (otherxywh[0], myxywh[1], otherxywh[2], myxywh[3]) )
            

if __name__ == "__main__":

    try:
        import bgeo
    except ImportError:
        sys.path.append('/fe3/demmapping/scripts')
        import bgeo

    R_earth = 6371.0

    def ll2sphere(lons, lats, alts=0, R_sphere=R_earth, basepoint=None):
        """given two N-element lists in degrees, and optional altitude for each, return Nx3 array of points on sphere of radius R_sphere"""
        flat = numpy.radians( numpy.ravel(lats) )
        flon = numpy.radians( numpy.ravel(lons) )
        coslat = numpy.cos( flat )
        sinlon = numpy.sin( flon )
        coslon = numpy.cos( flon )

        mesh = numpy.empty( shape=(flat.size, 3) )
        R_with = R_sphere + alts
        mesh[:,0] = R_with * coslon * coslat
        mesh[:,1] = R_with * sinlon * coslat
        mesh[:,2] = R_with * numpy.sin(flat)
        if basepoint is not None:
            mesh -= numpy.array(basepoint).reshape(1,3)
        return mesh

    def lolaaltaz2ptvec(lon, lat, altangle, azim):
        """turn lat, lon, zenith-angle and azimuth into 3-D unit-sphere surface-point(s) plus direction vector(s)"""
        # adapted from /fe2/misr/scripts/terraextract.py

        earthpts = ll2sphere( lon, lat, R_sphere=1 )
        az0vecs = numpy.array([0,0,1]).reshape(1,-1) - ( earthpts * earthpts[:,2].reshape(-1,1) )
        az0vecs /= numpy.sqrt( numpy.square(az0vecs).sum(axis=1).reshape(-1,1) )  # normalized surface vectors pointing to azimuth=0 (north)

        az90vecs = numpy.cross( az0vecs, earthpts )
        az90vecs /= numpy.sqrt( numpy.square(az90vecs).sum(axis=1).reshape(-1,1) )  # normalized surface vectors pointing to azimuth=90 (east)

        zenangr = numpy.radians( 90 - altangle )
        cosza = numpy.cos( zenangr )
        sinza = numpy.sin( zenangr )

        azimr = numpy.radians( azim )
        cosaz = numpy.cos( azimr )
        sinaz = numpy.sin( azimr )
        vecs = cosza.reshape(-1,1)*earthpts + (sinza*cosaz).reshape(-1,1)*az0vecs + (sinza*sinaz).reshape(-1,1)*az90vecs
        return earthpts, vecs



    def Usage():
        print("Usage: db_ampr.py -o outstem  -ymd yyyymmdd [-alt km_above_earth] [-f fromframe-toframe]")
        sys.exit(1)


    ymd = None
    outstem = None
    ffrom, fto = 0, 99999
    alt = 0
    writer = bgeo.BGeoPolyWriter

    ii = 1
    while ii < len(sys.argv) and sys.argv[ii][0] == '-':
        opt = sys.argv[ii]; ii += 1
        if opt == '-o':
            outstem = sys.argv[ii]; ii += 1
        elif opt == '-ymd':
            ymd = sys.argv[ii]; ii += 1
        elif opt == '-alt':
            alt = float( sys.argv[ii] ); ii += 1
        elif opt == '-f':
            ss = sys.argv[ii].replace('-',' ').replace(',',' ').split()
            ffrom, fto = [int(s) for s in ss]
        else:
            Usage()

    if outstem is None or ymd is None:
        Usage()

    if outstem.endswith('/'):
        outstem += 'amprribbon'
    if not os.path.isdir( os.path.dirname(outstem) ):
        os.makedirs( os.path.dirname(outstem) )

    basepoint = camp2exvis.basepoint
    incr_seconds = camp2exvis.time_incr * 3600

    ampr = AMPR( camp2exvis.amprdata[ ymd ], ymd, needgeo=True )

    # ampr.times is in seconds since start-of-day's-visualization


    times_s = ampr.times * 3600
    n_1 = len(times_s) - 1
    j0, j1 = [ min(j, n_1) for j in numpy.searchsorted( times_s, incr_seconds*numpy.array([ffrom,fto]) ) ]
    j0, j1 = max(0, j0-1), min(j1, n_1)+1

    mmdd = 100*ampr.timebase.month + ampr.timebase.day
    hms = 10000*ampr.timebase.hour + 100*ampr.timebase.minute + ampr.timebase.second # hhmmss 6-digit int

    detailattrnames = [ 'version', 'timebase_anim_yyyy', 'timebase_anim_mmdd', 'timebase_anim_hms', 'timebase_seconds_past_0h', ('basepoint',3), 'alt', 'fromtime_h_since0h', 'totime_h_since0h', 'fromframe', 'toframe' ]
    fromtime_h_since0h = ampr.times[j0] + ampr.time0_secs_within_day / 3600.0
    totime_h_since0h = ampr.times[j1-1] + ampr.time0_secs_within_day / 3600.0
    detailattrs =  [ 1.1, ampr.timebase.year, mmdd, hms, ampr.time0_secs_within_day, basepoint, alt, fromtime_h_since0h, totime_h_since0h, times_s[j0], times_s[j1-1] ]

    meshpts = ll2sphere( ampr.meshlon[j0:j1, 5:46:10], ampr.meshlat[j0:j1, 5:46:10], alts=alt, R_sphere=R_earth, basepoint=basepoint ).reshape(j1-j0, -1, 3)

    meshattrnames = [ 'time_anim', ('uv',3) ]
    nv, nu = meshpts.shape[0:2]

    meshptattrs = numpy.empty( (nv, nu, (1+3)), dtype='>f4' )
    meshptattrs[:,:,0] = times_s[j0:j1].reshape(-1,1)
    meshptattrs[:,:,1] = numpy.linspace(0, 1, num=nu, endpoint=True).reshape(1,nu) # "u" value
    meshptattrs[:,:,2] = numpy.linspace(0, 1, num=nv, endpoint=True).reshape(nv,1) # "v" value
    meshptattrs[:,:,3] = 0.0

    ribbonfname = "%s.%s%s" % (outstem, ymd, writer.suffix)
    writer( fname=ribbonfname,
            polyverts = [ meshpts ],
            polyattrnames = [ ],
            polyattrs = [ [] ],     # one (empty) list per polyverts[] entry
            pointattrnames = meshattrnames,
            pointattrs = meshptattrs.reshape(nv*nu, -1),
            detailattrnames = detailattrnames,
            detailattrs = detailattrs )

    # Write image using colormap.   Use T_b at 85GHz

    amprvals = ampr.tb85[j0:j1, 5:46]
    amprvrange = ampr.vranges[1]

    texturefname = "%s.%s.png" % (outstem, ymd)
    amprcmap = camp2exvis.cmap_for( 'ampr' )

    invalid = numpy.nan_to_num( amprvals ) <= 0   
    amprvalsma = numpy.ma.masked_array( amprvals, invalid )
    amprvalsma.data[ invalid ] = 0
    norm = matplotlib.colors.Normalize( vmin=amprvrange[0], vmax=amprvrange[1] )
    ##matplotlib.pyplot.imsave( texturefname, norm( amprvalsma ), vmin=0, vmax=1, cmap=amprcmap, origin='lower' )
    matplotlib.pyplot.imsave( texturefname, amprvalsma, vmin=amprvrange[0], vmax=amprvrange[1], cmap=amprcmap, origin='lower' )

    print("Wrote %s and %s" % (ribbonfname, texturefname))
