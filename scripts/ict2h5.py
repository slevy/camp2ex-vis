#! /usr/bin/env python3

import sys, os

import h5py
import numpy

import ict

namegroups = [
    "YANG",
    "DIGANGI",
    "THORNHILL",
    "LAWSON",
    "DISKIN",
    "FLYNN",
    "ZIEMBA",
    "WEBER",
    "WANG",
    "HOSTETLER",
    "VAN_DIEDENHOVEN",
    "LAWSON",
    "SCHMIDT",
    "BUCHOLTZ",
    "STAMNES",
]

traps = 0

def ict2h5( infile, outfile ):

    with open(infile, 'r') as ictf:
        ictr = ict.ICTReader( ictf )
        headlines = ictr.headlines
        fieldnames = ictr.fieldnames

        data = []
        for row in ictr:
            vrow = numpy.zeros( len(fieldnames) )
            for i, field in enumerate(fieldnames):
                try:
                    vrow[i] = float(row[field])
                except:
                    vrow[i] = -999999
                    global traps
                    traps += 1
            data.append(vrow)

    datar = numpy.array( data )

    dss = {}
    with h5py.File(outfile, 'w') as hff:

        hff.create_dataset('ict_headlines', data=numpy.array([s.encode() for s in headlines]))

        for fldno, field in enumerate(fieldnames):
            data_now = datar[:, fldno]
            dss[field] = hff.create_dataset( field.strip(), data=data_now )
            # set any attributes?
            #if numpy.any(data_now == -999999):
            #    dss[field].create('

if len(sys.argv) <= 2 or not sys.argv[2].endswith('.h5'):
    print("Usage: %s   ../CAMP2Ex/RF09_01Hz_Merge/camp2ex-mrg01-p3b_merge_20190915_RI.ict   ../CAMP2Ex/RF09_01Hz_Merge/camp2ex-mrg01-p3b_merge_20190915_RI.h5" % sys.argv[0])
    print("Read big .ict file with list of scalar quantities, write same data (minus icartt attributes and datatype info) as HDF5")
    print("Includes extra field 'ict_headlines', an array of bytestrings, one per .ict header line.   Use .decode() to turn each one back to a python3 string.")
    sys.exit(1)

ict2h5( sys.argv[1], sys.argv[2] )
print("Wrote to %s, caught %d traps" % (sys.argv[2], traps))
