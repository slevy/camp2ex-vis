# Creates a .chan file that maps frame number to time.
# This script is meant to be run inside of Houdini, since it references a node.

import math

def createTimeChan():

    # Hardcoded parameters
    seconds_per_frame = 10
    date = "09-15-2019"

    # Create output chan file
    out_file_path = "/hi0/camp2ex/data/chan/flightTime_" + date + ".chan"
    out_file = open(out_file_path, "w")

    # Retrieve bgeo data
    node = hou.node("/obj/track_full_" + date + "/OUT_TRACK_FULL_" + date)
    geo = node.geometry()
    points = geo.points()
    
    # Calculate total number of frames
    start_time = points[0].attribValue("time_since")
    end_time = points[len(points)-1].attribValue("time_since")
    total_frames = (end_time - start_time) / seconds_per_frame
    print(str(total_frames) + " total frames")

    # Loop through frames
    # Write out frame, seconds, pointID
    i_points = 0
    for i_frames in range(int(math.ceil(total_frames))):
        seconds_passed = i_frames*seconds_per_frame
        data_time = points[i_points].attribValue("time_since")
        while data_time < (start_time + seconds_passed):
            i_points += 1
            data_time = points[i_points].attribValue("time_since")

        out_file.write(str(i_frames) + " " + str(seconds_passed) + " " + str(i_points) + "\n")
        
        
        

    """
    # Loop through data
    prev = start_time
    for i in range(len(points)):
        curr_time = points[i].attribValue("time_since")
        
        diff = curr_time - prev_time
        if diff != 10:
            print(curr_time - prev_time)
        
        prev_time = curr_time
    """
    out_file.close()
