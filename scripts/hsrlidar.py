#! /usr/bin/env python3

from __future__ import print_function

import sys, os
import numpy
import h5py

import matplotlib
import matplotlib.pyplot as mp
matplotlib.use('Agg')

import datetime

import camp2exvis   # vis parameters -- 3D basepoint, timebase times for dates, vertical-exaggeration

sys.path.append('/fe3/demmapping/scripts')
import bgeo
import suninfo

R_earth = 6371.0

def ll2sphere(lons, lats, alts=0, R_sphere=R_earth, basepoint=None):
    """given two N-element lists in degrees, and optional altitude for each, return Nx3 array of points on sphere of radius R_sphere"""
    flat = numpy.radians( numpy.ravel(lats) )
    flon = numpy.radians( numpy.ravel(lons) )
    coslat = numpy.cos( flat )
    sinlon = numpy.sin( flon )
    coslon = numpy.cos( flon )

    mesh = numpy.empty( shape=(flat.size, 3) )
    R_with = R_sphere + alts
    mesh[:,0] = R_with * coslon * coslat
    mesh[:,1] = R_with * sinlon * coslat
    mesh[:,2] = R_with * numpy.sin(flat)
    if basepoint is not None:
        mesh -= numpy.array(basepoint).reshape(1,3)
    return mesh

def lolaaltaz2ptvec(lon, lat, alt, azim):
	"""turn lat, lon, zenith-angle and azimuth into 3-D unit-sphere surface-point(s) plus direction vector(s)"""
    # adapted from /fe2/misr/scripts/terraextract.py

	earthpts = ll2sphere( lon, lat, R_sphere=1 )
	az0vecs = numpy.array([0,0,1]).reshape(1,-1) - ( earthpts * earthpts[:,2].reshape(-1,1) )
	az0vecs /= numpy.sqrt( numpy.square(az0vecs).sum(axis=1).reshape(-1,1) )  # normalized surface vectors pointing to azimuth=0 (north)

	az90vecs = numpy.cross( az0vecs, earthpts )
	az90vecs /= numpy.sqrt( numpy.square(az90vecs).sum(axis=1).reshape(-1,1) )  # normalized surface vectors pointing to azimuth=90 (east)

	zenangr = numpy.radians( 90 - alt )
	cosza = numpy.cos( zenangr )
	sinza = numpy.sin( zenangr )

	azimr = numpy.radians( azim )
	cosaz = numpy.cos( azimr )
	sinaz = numpy.sin( azimr )
	vecs = cosza.reshape(-1,1)*earthpts + (sinza*cosaz).reshape(-1,1)*az0vecs + (sinza*sinaz).reshape(-1,1)*az90vecs
	return earthpts, vecs


class SpeckWriter(object):

    suffix = ".speck"

    def __init__(self, fname, pointattrnames=[], pointattrs=None, points=None, detailattrnames=[], detailattrs=None ):
        """ ... """

        self.fname = fname

        def asb(s):
            return s.encode() if hasattr(s,'encode') else s

        with open(fname, 'wb') as outf:
            outf.write(b'#! /usr/bin/env partiview\n\n')

            if isinstance(detailattrs, dict):
                for k in sorted( detailattrs.keys() ):
                    outf.write(b'##= %s = %s\n' % (asb(k), asb(str(detailattrs[k]))))

            else:
                for datt, dval in zip(detailattrnames, detailattrs):
                    attname = datt[0] if isinstance(datt, (list,tuple)) else datt
                    outf.write(b'##= %s = %s\n' % (asb(attname), asb('%s' % (dval,))))

            outf.write(b'\n')
                        
                
            if pointattrnames is None or len(pointattrnames)==0:
                for pt in points:
                    outf.write(b'%.8g %.8g %.8g\n' % tuple(pt))

            else:
                for i, attrname in enumerate(pointattrnames):
                    outf.write(b'datavar %d %s\n' % (i, asb(attrname)))
                outf.write(b'\n')

                attrfmt = b' %.8g' * len(pointattrnames)

                for pt, att in zip(points, pointattrs):
                    outf.write(b'%.8g %.8g %.8g ' % tuple(pt) + attrfmt % tuple(att) + b'\n')


def jday2date(jday):    # make a datetime object of 0h UTC on the given Jday day.   E.g. 737683 => datetime(2019, 9, 15).
    # Jday zero point is something like Jan 0 of 0 AD.  366+1 days before Jan 1 of 1 AD.
    return datetime.datetime( 1, 1, 1, 0, 0, 0 ) + datetime.timedelta(days=(jday - 366 - 1))


def mkdirfor(path):
    dirname = os.path.dirname(path)
    if not os.path.isdir(dirname):
        try:
            os.makedirs(dirname)
        except:
            pass

class MetNav(object):
    """Opens a MetNav file converted to hdf5 form as with ict2h5.py"""

    def __init__(self, fname, Day0yyyymmdd, wantfields=None):
        self.fname = fname
        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )
        self.timebase_seconds_past_0h = (self.timebase - self.Day0).total_seconds()
        self.fieldnames = wantfields

        self.loaded = False
        self.times_h = None
        self.data = {}

        self.load_data()

    def load_data(self):
        if self.loaded:
            return
        self.loaded = True

        hf = h5py.File( self.fname, 'r' )

        if self.fieldnames is None:
            self.fieldnames = []
            def gatherfield(key):
                if isinstance(hf[key], h5py.Dataset):
                    self.fieldnames.append(key)
            hf.visit( gatherfield ) # list of all entries in file
            self.fieldnames.sort()

        timefield = 'Time_Start' # this had better exist.
        times = hf[timefield][:]
        self.pscantime = times.ravel() - self.timebase_seconds_past_0h  # pscantime is time in seconds since visualization frame 0
        self.data = { fieldname: hf[fieldname] for fieldname in self.fieldnames }
        self.data['pscantime'] = self.pscantime

        self.preslice = self.postslice = None

    def keys(self):
        return self.fieldnames

    def prepsplice(self, otherpscantimes):
        before, after = otherpscantimes[0], otherpscantimes[-1]
        n_1 = len(self.pscantime)-1
        j0, j1 = [ min(j, n_1) for j in numpy.searchsorted( self.pscantime, (before,after) ) ]
        while j0 > 0 and abs(before - self.pscantime[j0-1]) < 0.5:   # Give us at least a half-second
            j0 -= 1
        self.preslice = slice(0, j0)
        self.postslice = slice(j1, len(self.pscantime))

    def spliced(self, otherfield, scafieldname=None):
        if self.preslice is None:
            raise ValueError("Must call MetNav.prepsplice() before calling .spliced()")

        if scafieldname is None:
            # Maybe there's no non-HSRL field.
            #  Construct pre-/post-padding full of NaNs,
            #  so the concatenated result has the expected number of entries, but only NaNs beyond the HSRL time range.
            othershape = list( otherfield.shape )
            othershape[0] = self.postslice.stop
            scafield = numpy.broadcast_to( numpy.nan, othershape )
        else:
            scafield = self.data[ scafieldname ]

        return numpy.concatenate( [ scafield[self.preslice], otherfield, scafield[self.postslice] ] )


class HSRLidar(object):
    """Opens an HSRL LIDAR data file."""

    # convert a UTC time to seconds-since-epoch.  Is this really the best way to do it??
    basepoint = camp2exvis.basepoint
    # 1546300800.0

    version = 20

    def __init__(self, fname):
        self.hf = h5py.File( fname, 'r' )
        self.hfh = self.hf['header']
        self.hfn = self.hf['Nav_Data']
        self.hfd = self.hf['DataProducts']
        self.hfs = self.hf['State']


        # grab start date from State/date, and combine with hms from first entry in Nav_Data/gps_time
        ymd = int(self.hfh['date'][0,0])    # /header/date == yyyymmdd -- 8-digit integer
        self.timebase = camp2exvis.timebase_from( ymd )  # find timebase datetime() for this day's animation e.g. 2019-09-15 22:43:00

        ##hhh = self.hfn['gps_time'][0,0]
        ##mmm = 60 * (hhh - int(hhh))
        ##self.date_beg = ( ymd//10000, (ymd/100)%100, ymd%100, int(hhh), int(mmm), 60 * (mmm - int(mmm)) )

        self.jday0 = self.hfn['Jday'][0,0]

        # unix time at Nav_Data/Jday = jday0, and Nav_Data/gps_time = 0.0
        ## os.environ['TZ'] = 'UTC0'; time.tzset()
        self.timebase_seconds_past_0h = (self.timebase - jday2date( self.jday0 )).total_seconds()
        ## self.unixtime0 = int( self.timebase.strftime('%s') )

    def datestr(self):
        "returns yyyymmdd_hhmmss of start time of this observation run"
        return "%04d%02d%02d" % (self.timebase.year, self.timebase.month, self.timebase.day)

    def ll2vissphere( self, lons, lats, alts_m=0 ):
        return ll2sphere( lons, lats, alts=alts_m*(0.001*camp2exvis.vertical_exaggeration), R_sphere=R_earth, basepoint=self.basepoint )


    def writegeoms(self, outstem, flat_earth=False, writer=bgeo.BGeoPolyWriter, metnav=None):
        """Write <outstem>.lidar.<suf>, <outstem>.track.<suf> files."""

        # track, indexed by time
        plon = self.hfn['gps_lon'][:,:]  # Ntimes, 1
        plat = self.hfn['gps_lat'][:,:]  # Ntimes, 1
        palt = self.hfn['gps_alt'][:,:]  # Ntimes, 1

        lidaralts = self.hfd['Altitude'][:,:]  # 1, Nsamples_per_ray


        detailattrnames = [ 'version', 'timebase_anim_yyyy', 'timebase_anim_mmdd', 'timebase_anim_hms', 'Jday0', 'timebase_seconds_past_0hJday0', ('basepoint',3), 'flatearth', 'vertical_exaggeration' ]
        mmdd = 100*self.timebase.month + self.timebase.day  # mmdd 8-digit int
        hms = 10000*self.timebase.hour + 100*self.timebase.minute + self.timebase.second # hhmmss 6-digit int
        detailattrs = [ self.version, self.timebase.year, mmdd, hms, self.jday0, self.timebase_seconds_past_0h, self.basepoint, flat_earth, camp2exvis.vertical_exaggeration ]
 

        # airplane "p": track
        # scantime is in seconds since timebase (start-of-today's-animation time)
        pscantime = ( (- self.timebase_seconds_past_0h) + (86400 * (self.hfn['Jday'][:] - self.jday0)) + 3600 * self.hfn['gps_time'][:] ).ravel()

        ## phh = 24 * (self.hfn['Jday'][0,0] - self.jday0) + self.hfn['gps_time'][:].ravel()
        ## pmm = 60 * (phh - numpy.floor(phh))
        ## phhmmss = 10000*numpy.floor(phh) + 100*numpy.floor(pmm) + 60*(pmm - numpy.floor(pmm))

        pheading = ( self.hfn['true_heading'][:] ).ravel()
        ppitch =   ( self.hfn['pitch'][:] ).ravel()
        proll =    ( self.hfn['roll'][:] ).ravel()

        pointattrnames = ['time_anim', 'Aerosol_ID', 'bsc_532', 'bscratio_532_1064', 'dep_532', 'dust_mixing_ratio', 'AOT_above_cloud_532']

        aid = self.hfd['Aerosol_ID'][:]     # aerosol ID, 1=Ice, 2=Dusty Mix, 3=Marine, 4=Urban/Pollution, 5=Smoke, 6=Fresh Smoke, 7=Pol. Marine, 8=Dust, None
        #depol532 = self.hfd['532_aer_dep'][:] # depolarization ratio
        ##dep532 = self.hfd['532_dep'][:]
        ##bsc355 = self.hfd['355_bsc'][:]     # backscattering coef, km^-1 sr^-1
        bsc532 = self.hfd['532_bsc'][:]
        ##bsc1064 = self.hfd['1064_bsc'][:]
        AOT_above_cloud_532 = self.hfd['532_AOT_above_cloud'][:].ravel()

        ext532 = self.hfd['532_ext'][:]    # extinction, km^-1 

        dust_mixing_ratio = self.hfd['Dust_Mixing_Ratio']

        alts = self.hfd['Altitude'][0,:]   # altitudes of each bin in meters
        cloud_top_height = self.hfd['cloud_top_height'][:,0]

        if metnav is not None:
            metnav.prepsplice( pscantime )
            print("prepsplice() -> %s  ... %s" % (metnav.preslice, metnav.postslice))

            # extend every single variable.
            #   Use corresponding dataset from "metnav" where it exists;
            #   if there isn't one, pass None and splice() will pad with NaNs.
            # These names assume we're using the MetNav data directly (../CAMP2Ex/metnav/CAMP2EX-MetNav_P3B_yyyymmdd_R0.h5)

            pscantime = metnav.spliced( pscantime, 'pscantime' )

            plon     = metnav.spliced( plon.ravel(),     'Longitude' )
            plat     = metnav.spliced( plat.ravel(),     'Latitude' )
            palt     = metnav.spliced( palt.ravel(),     'GPS_Altitude' )
            pheading = metnav.spliced( pheading.ravel(), 'True_Heading' )
            ppitch   = metnav.spliced( ppitch.ravel(),   'Pitch_Angle' )
            proll    = metnav.spliced( proll.ravel(),    'Roll_Angle' )

            cloud_top_height = metnav.spliced( cloud_top_height, None )
            bsc532   = metnav.spliced( bsc532, None )
            AOT_above_cloud_532 = metnav.spliced( AOT_above_cloud_532, None )


        vvalid = numpy.any( numpy.isfinite( bsc532 ), axis=1 )
        npoints = vvalid.sum()


        def chosen( v ):
            return v[ vvalid ].ravel()

        def chosen_nonan( v, default=0.0 ):
            return numpy.nan_to_num( v[ vvalid ].ravel(), nan=default )


        ## pplon = chosen( plon )
        ## pplat = chosen( plat )
        ## ppalt = chosen( palt )
        ## ppscantime = chosen( pscantime )

        ## psunalt = self.hfn['sun_elev_aircraft'][:].ravel()
        ## psunaz  = self.hfn['sun_azimuth_ground'][:].ravel()
        ## # turn alt+az and geo position into 3-D sun direction vector(s)
        ## spt, sunvecs = lolaaltaz2ptvec( plon, plat, psunalt, psunaz )
        # turn sun vectors into decl + longitude
        ## psunlon = numpy.degrees( numpy.arctan2( sunvecs[:,1], sunvecs[:,0] ) )

        # Nah, that doesn't work.   sun_elev_aircraft is probably relative to the aircraft's instantaneous orientation, not to the horizon.
        # Use a (very rough) solar ephemeris instead.

        sni = suninfo.SunInfo()
        day0date = jday2date( self.jday0 )
        sunha0 = sni.sunha_deg( day0date, lon_deg=0.0, utc_hours=0.0 )
        sundec = sni.sundec( day0date )
        psundecl = numpy.broadcast_to( sundec, pscantime.shape )

        # sun's longitude relative to Greenwich meridian (Earth's +X axis)
        psunlon = numpy.mod( -sunha0 - (pscantime + self.timebase_seconds_past_0h)*(360.0/86400), 360.0 )
        


        # nan_to_num(x, copy=True, nan=0.0, posinf=None, neginf=None)

        ### pointattrnames = ['time_anim', 'Aerosol_ID', 'bsc_532', 'bscratio_532_1064', 'dep_532', 'dust_mixing_ratio', 'AOT_above_cloud_532']

        #if False:
        #    npoints = vvalid.sum()
        #    attrs = numpy.empty( (npoints, len(pointattrnames)), dtype='>f4' )
        #    attrs[:,0] = chosen( scantime )
        #    attrs[:,1] = chosen_nonan( aid, default=0 )
        #    attrs[:,2] = chosen_nonan( bsc532 )
        #    attrs[:,3] = chosen_nonan( bsc532 / bsc1064 )
        #    attrs[:,4] = chosen_nonan( dep532 )
        #    attrs[:,5] = chosen_nonan( dust_mixing_ratio )
        #    attrs[:,6] = chosen_nonan( AOT_above_cloud_532 )

        # Select the 3-D points for the above 
        if flat_earth:
            ## points = numpy.empty( (len(attrs), 3), dtype=numpy.float32 )
            ## points[:,0] = chosen( lon3D )
            ## points[:,1] = chosen( lat3D )
            ## points[:,2] = chosen( alt3D )

            trackpoints = numpy.empty( (len(plon), 3), dtype=numpy.float32 )
            trackpoints[:,0] = plon.ravel()
            trackpoints[:,1] = plat.ravel()
            trackpoints[:,2] = palt.ravel() * camp2exvis.vertical_exaggeration * 0.001  # in km
        else:
            # altitudes are in meters, other units in km, so scale the altitudes
            ## points = ll2sphere( chosen( lon3D ), chosen( lat3D ), alts=.001*chosen( alt3D ), R_sphere=R_earth ) - self.basepoint.reshape(1,3)

            print("HSRL ribbon altitudes(m): %g .. %g" % (palt.min(), palt.max()))
            if metnav is not None:
                print("Mergerdata: time %g .. %g  altitude %g .. %g at start&end" % (metnav.pscantime[0], metnav.pscantime[-1], metnav.data['GPS_Altitude'][0], metnav.data['GPS_Altitude'][-1])) 

            trackpoints = self.ll2vissphere( plon.ravel(), plat.ravel(), alts_m=palt.ravel() )

        def unwrap_heading( pheading ):
            pheading = pheading.copy()
            dhead = 0
            prev = pheading[0]
            for i, head in enumerate(pheading):
                if abs(head-prev) > 180:
                    if head < prev:
                        dhead += 360
                    else:
                        dhead -= 360
                prev = pheading[i]
                pheading[i] += dhead
            # check
            glitches = numpy.argwhere( numpy.abs( pheading[1:] - pheading[:-1] ) >= 90 ).ravel()
            if len(glitches) > 0:
                print("unwrap_heading: Still have %d glitches at %s" % (len(glitches), tuple(glitches)))
            return pheading

        trackattrnames = ['time_anim', 'heading', 'pitch', 'roll', 'sun_declination', 'sun_longitude', 'AOT_above_cloud_532']
        trackattrs = numpy.empty( ( len(trackpoints), len(trackattrnames) ), dtype=numpy.float32 )
        trackattrs[:,0] = pscantime
        trackattrs[:,1] = unwrap_heading( pheading )
        trackattrs[:,2] = ppitch
        trackattrs[:,3] = proll
        trackattrs[:,4] = psundecl
        trackattrs[:,5] = psunlon
        trackattrs[:,6] = AOT_above_cloud_532


        trackfname = "%s.track%s" % (outstem, writer.suffix)
        writer( fname=trackfname,
                    points = trackpoints,
                    pointattrnames = trackattrnames,
                    pointattrs = trackattrs, 
                    detailattrnames = detailattrnames,
                    detailattrs = detailattrs )


        
        curtainmesh = numpy.empty( (2, len(pscantime), 3), dtype=numpy.float32 )
        curtainmesh[0,:,:] = self.ll2vissphere( plon.ravel(), plat.ravel(), alts_m=0 )
        curtainmesh[1,:,:] = self.ll2vissphere( plon.ravel(), plat.ravel(), alts_m=lidaralts[:,-1] )

        print("lidar altitude range %g .. %g  scantime %g .. %g" % (lidaralts.min(), lidaralts.max(), pscantime.ravel()[0], pscantime.ravel()[-1]))

        curtainattrnames = [ 'time_anim', ('uv',3) ]
        nv, nu = 2, len(pscantime)
        curtainptattrs = numpy.empty( (nv,nu, (1+3)), dtype='>f4' )
        curtainptattrs[:,:,0] = pscantime.reshape(1,-1) # time_anim = pscantime in both v=0 and v=1 mesh rows
        ##curtainptattrs[:,:,1] = ((pscantime-pscantime[0]) / (pscantime[-1]-pscantime[0])).reshape(1,-1) # u = fraction of time range
        curtainptattrs[:,:,1] = numpy.linspace(0.0, 1.0, num=nu, endpoint=True)
        curtainptattrs[0,:,2] = 0.0 # v=0 along ground-level mesh row
        curtainptattrs[1,:,2] = 1.0 # v=1 along high-altitude mesh row
        curtainptattrs[:,:,3] = 0.0 # w=0
        meshfname = "%s.curtain%s" % (outstem, writer.suffix)
        writer( fname=meshfname,
                    polyverts = [ curtainmesh ],
                    polyattrnames = [  ],
                    polyattrs = [ [] ],     # one (empty) list per polyverts[] entry!
                    pointattrnames = curtainattrnames,
                    pointattrs = curtainptattrs.reshape(nv*nu, -1),
                    detailattrnames = detailattrnames,
                    detailattrs = detailattrs )

        # save the texture image too
        texturefname = "%s.curtain.png" % outstem
        ribbon3Dcmap = camp2exvis.cmap_for( 'hsrl-ribbon' )

        invalid = numpy.nan_to_num( bsc532 ) <= 0   
        bsc532ma = numpy.ma.masked_array( bsc532, invalid )
        bsc532ma.data[ invalid ] = 0
        norm = matplotlib.colors.LogNorm(vmin=1e-4, vmax=1e-1)
        mp.imsave( texturefname, norm( bsc532ma ).transpose(), vmin=0, vmax=1, cmap=ribbon3Dcmap, origin='lower' )

flat_earth = False
outdir = "."

def Usage():
    print("""Usage: %s [-flat]  [-bgeo|-speck] [-exaggeration N] [-o outdir] HSRLfile.h5
Converts a CAMP2Ex HSRL (LIDAR file) to .bgeo form.   Writes a set of output files, representing a full day's flight:
    <outdir>/HSRL_yyyymmdd.track.bgeo  -- list of points for airplane track
    <outdir>/HSRL_yyyymmmdd.curtain.bgeo -- quad mesh, vertically downward from aircraft track,
    <outdir>/HSRL_yyyymmmdd.curtain.png -- RGBA texture for polygonal mesh   (SOMEDAY SOON)

and these as "detail" attributes, applying globally to the .bgeo file:
    timebase_anim_ymd    32-bit-float: high-order part of timebase time (seconds since UNIX epoch of 1970.0)
    timebase_anim_hms    32-bit-float: rest of timebase time.   Add timebase_hi+timebase_lo+time_since for 64-bit UNIX time
    basepoint      3-component vector.   Add basepoint + 3-D points to get full 64-bit earth-centered position.
    flatearth      1 if all points are in (lon, lat, altitude_in_km) coordinates.  0 if 3-D Cartesian with Earth radius of 6371 (in km).

The .track.bgeo_or_speck reports contains points along the airplane's track, plus these attributes for each point:
    time_anim       time
    pitch           orientation of airplane
    roll            orientation of airplane
    sun_declination declination (latitude) of sun; positive is north
    sun_longitude   longitude of sun; positive is east
    AOT_above_cloud_532  optical-thickness between cloudtop and aircraft, in 532nm light
    
and the same timebase, basepoint, flatearth attributes as with .radar.bgeo.
Uses vertical exaggeration %g as given in camp2exvis unless -exaggeration N""" % (sys.argv[0], camp2exvis.vertical_exaggeration))
    sys.exit(1)

ii = 1
writer = bgeo.BGeoPolyWriter
wsuf = '.bgeo'
metnavfile = None

while ii < len(sys.argv) and sys.argv[ii][0] == '-':
    opt = sys.argv[ii]; ii += 1
    if opt == '-o':
        outdir = sys.argv[ii]; ii += 1
    elif opt.startswith('-flat'):
        flat_earth = True
    elif opt == '-speck':
        writer = SpeckWriter
        wsuf = '.speck'
    elif opt == '-bgeo':
        writer = bgeo.BGeoPolyWriter
        wsuf = '.bgeo'
    elif opt == '-exaggeration':
        camp2exvis.vertical_exaggeration = float( sys.argv[ii] ); ii += 1
    elif opt == '-metnav':
        metnavfile = sys.argv[ii]; ii += 1
    else:
        print("Unknown option: %s" % opt)
        Usage()

if ii >= len(sys.argv):
    Usage()

hsrl = HSRLidar( sys.argv[ii] )

datestr = hsrl.datestr()

metnavfields = ['Latitude','Longitude','GPS_Altitude','True_Heading','Pitch_Angle','Roll_Angle']
metnav = None if metnavfile is None else MetNav( metnavfile, datestr, metnavfields )

outstem = os.path.join( outdir, "HSRLidar_%s" % datestr )
if flat_earth:
    outstem += '.flat'

mkdirfor( outstem )

#print("Writing to %s.{lidar,track}%s" % (outstem, writer.suffix))
print("Writing to %s.track%s" % (outstem, writer.suffix))

hsrl.writegeoms( outstem, flat_earth=flat_earth, writer=writer, metnav=metnav )
