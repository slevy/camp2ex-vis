#! /usr/bin/env python3

"""Common parameters for CAMP2Ex visualization:
     basepoint  -- 3D point which gets subtracted from all surface points before writing to .bgeo
     vertical_exaggeration -- stretch altitudes by this factor
     timebase_from( thing ) -- given 'thing', return that day's animation timebase as a datetime.datetime object
        thing may be: a datetime.datetime, or
                      a string of form yyyymmdd, or
                      an integer of form yyyymmdd
"""

import sys, os
import numpy
import datetime
import time

import matplotlib
import matplotlib.cm
import matplotlib.colors
matplotlib.use('Agg')


basepoint = numpy.array( [ -3057, 5454, 1224 ] )    # all .bgeo files' coordinates have this point subtracted from them;  Translate by +basepoint to return to radius=6371 sphere.
R_earth = 6371.0

vertical_exaggeration = 4


## ## ## STYLE ## ## ##

# These are initialized when camp2exvis is first imported

# Install global layout default
matplotlib.rc('figure.subplot', bottom=0.08,top=0.96,hspace=0.08,wspace=0.11)  ## STYLE

# legend.scatterpoints says to use multiple dots in legends for scatter() plots
matplotlib.rc('legend', facecolor='#303038', framealpha=0.85, scatterpoints=3)  ## STYLE

title_bbox_opts = dict(boxstyle='round', facecolor='DarkSlateGray', alpha=0.75) ## STYLE # FancyBox options for nice box underlying title text.  Pass as: ax.set_title( ..., bbox=title_bbox_opts )

def axis_set_title_styled( ax, title, **kwargs ):
    # or plt.rcParams['axes.titley'] = 0.95 and ['axes.titlepad'] = -14 -- put title just below top of plot box
    #ybase = 0.95
    ybase = 0.975
    perline = 0.09
    opts = dict( y=ybase - perline*title.count('\n'), pad=-14, bbox=title_bbox_opts )
    opts.update(kwargs)
    return ax.set_title( title, **opts )

## ## ## end STYLE ## ##


### Data sources, by date

class PatternFile(object):

    allinstances = []

    def __init__(self, fmt, check=True, advice=None, label=None):
        self.fmt = fmt
        self.do_check = check
        self.advice = advice
        self.label = label

        self.allinstances.append( self )

    def __getitem__(self, datetag):
        if '%' in self.fmt:
            result = self.fmt % datetag
        else:
            result = self.fmt
        return self.check(result, datetag)

    def check(self, result, datetag):
        if self.do_check and (result is None or not os.path.exists(result)):
            msg = " ".join( [s for s in ("No", self.label, "file", result, "found for date", datetag) if s is not None] )
            print(msg)
            advice = self.advice
            if advice is not None:
                if '%' in advice:
                    n = len( advice.split('%') ) - 1
                    advice = advice % ( (datetag,) * n )
            print(advice)
            return None
        return result

class TableFile(PatternFile):

    def __init__(self, table, check=True, label=None, advice=None):
        super().__init__(table, check=check, label=label, advice=advice)
        self.table = table

    def __getitem__(self, datetag):
        result = self.table.get(datetag, None)
        return self.check(result, datetag)

RFno = { '20190915': 'RF09' }  # Research flight numbers

hsrldata = PatternFile( '../CAMP2Ex/HSRL/CAMP2EX-HSRL2_P3B_%s_R0.h5',
        label='HSRL',
        advice="From: https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#HOSTETLER.CHRIS/  fetch: CAMP2EX-HSRL2_P3B_%s_RA.h5" )

cpidata  = TableFile( {'20190915': '../CAMP2Ex/RF09_subset_CPI_images'},
        label='CPI',
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#LAWSON.PAUL/  fetch+unpack: 	CAMP2Ex-CPI-Images_P3B_%s_R0.zip" )

amprdata = PatternFile( '../CAMP2Ex/AMPR/CAMP2Ex-AMPR_P3B_%s_R0.nc',
        label='AMPR',
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#LANG.TIMOTHY/  fetch: CAMP2Ex-AMPR_P3B_%s_R0.nc" )

amprwinddata = PatternFile( '../CAMP2Ex/AMPR/CAMP2Ex-AMPRwind_P3B_20190904_R2_thru20191005.nc',
        label='AMPRwind',
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#LANG.TIMOTHY/    A single netCDF file covers lots of flights for 2019 -- note the 'seconds since 2019-09-04 00:34:37.5' time field.  This _R2 version obtained from Dongwei Fu (is it in the LARC archive, or only _R1 there?)" )

apr3data = PatternFile( '../CAMP2Ex/APR3',
        label="APR3",
        advice="From somewhere apr3-ish on https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex" )

rspdata = PatternFile( '../CAMP2Ex/RSP/%s',
        label='RSP',
        #advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#VAN_DIEDENHOVEN.BASTIAAN/	fetch+unpack into ../CAMP2Ex/RSP/%s the file CAMP2EX-RSP1-CLD_P3B_%s_R3.zip" )
        advice="From  https://data.giss.nasa.gov/pub/rsp/data/CAMP2Ex/L2CLD	fetch+unpack into ../CAMP2Ex/RSP/%s the file RSP1-L2CLD_P3_%s_R03.zip" )

rspclddata = PatternFile( '../CAMP2Ex/RSP/%s',
        label='RSPCld',
        advice="From  https://data.giss.nasa.gov/pub/rsp/data/CAMP2Ex/Wcld_cdf fetch all RSP1-P3_%s_T*.nc into ../CAMP2Ex/RSP/%s" )

largeopticaldata = PatternFile( '../CAMP2Ex/LARGE/CAMP2Ex-LARGE-OPTICAL_P3B_%s_R0.ict',
        label='LARGE Optical',
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#ZIEMBA.LUKE/ as CAMP2Ex-LARGE-OPTICAL_P3B_yyyymmdd_R0.ict" )

#mergedscalardata = TableFile( {'20190915': '../CAMP2Ex/RF09_01Hz_Merge/camp2ex-mrg01-p3b_merge_20190915_RI.h5' },
#        label='merged scalars',
#        advice="Where to get merged data? like RF09_01Hz_Merge/camp2ex-mrg01-p3b_merge_20190915_RI.h5"
#    )
mergedscalardata = PatternFile( '../CAMP2Ex/merge1hz/camp2ex-mrg01-p3b_merge_%s_RJ.h5',
        label='merged scalars',
        advice='https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#01_SECOND.P3B_MRG/ (under "Merges" tab), fetch camp2ex-mrg01-p3b_merge_%s_RJ.ict, process with ict2h5.py <file>.ict <file>.h5'
     )

AHIopticaldata = PatternFile( '../geo/AHI/optical/%s/cut',
        label='AHI optical',
        advice="Download from ftp.ptree.jaxa.jp:/jma/netcdf/2019<MM>/<DD>/NC_H08_YYYYMMDD_HHMM_R21_FLDK.06001_06001.nc, process with AHIaltrgbtotiff.py -chans 3,2,1 -te ... -o ../data/AHI/%s/rgb5-cropped"
    )

AHInaturaldata = PatternFile( '../data/AHI/%s/natrgb5-cropped-brf',
        label='AHI RGB natural',
        advice="Download from ftp.ptree.jaxa.jp:/jma/netcdf/2019<MM>/<DD>/NC_H08_YYYYMMDD_HHMM_R21_FLDK.06001_06001.nc, process with AHIaltrgbtotiff.py -chans 5,4,3 -brf -mincos 0.10 -gamma 1.6 -te ... -o ../data/AHI/%s/natrgb5-cropped-brf"
    )  # directory with AHI "natural-RGB" images, cropped

AHIAOTdata = PatternFile( '../data/AHI/%s/AOT2.5-cropped',
        label='AHI AOT',
        advice="Download from ftp.ptree.jaxa.jp:/pub/himawari/L2/ARP/021/201909/15/23/NC_H08_20190915_2300_L2ARP021_FLDK.02401_02401.nc, process with AHIaltrgbtotiff.py -chans AOT -scale 2.5 -te ... -o ../data/AHI/%s/AOT2.5-cropped"
    )  # directory with AHI "aerosol optical thickness" images, cropped

AHIIRdata = PatternFile( '../data/AHI/%s/IRtbb11-cropped',
        label='AHI IR 11$\mu{m}',
        advice="Download from ftp.ptree.jaxa.jp:/pub/himawari/L2/ARP/021/201909/15/23/NC_H08_20190915_2300_L2ARP021_FLDK.02401_02401.nc, process with AHIaltrgbtotiff.py -chans tbb_11 -scale 3,30000  -te ... -o ../data/AHI/%s/IRtbb11-cropped"
    )  # directory with AHI "11-micron IR" images, cropped

ssfrdata = PatternFile( '../CAMP2Ex/Radiation/CAMP2EX-SSFR_P3B_%s_RA.h5',
        label="SSFR",
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#SCHMIDT.SEBASTIAN/  fetch: CAMP2EX-SSFR_P3B_%s_R0.h5"
    )

spnsdata = PatternFile( '../CAMP2Ex/SPN/SSFR_%s_IWG.h5',
        label="SPNS",
        advice="Where to get SPNS data for %s?  Not publicly released - 20190915 received from Yulan Hong."
    )

spntransdata = PatternFile( '../CAMP2Ex/SPN/spn_transmittance_%s.h5',
        label="SPNS Transmittance",
        advice="Where to get SPNS transmittance data?  Not publicly released - 20190915 received from Yulan Hong.  Then process .txt file with table2h5.py"
    )

bbrdata = PatternFile( '../CAMP2Ex/BBR/CAMP2EX-BBR_P3B_%s_R2.h5',
        label="BBR down/upwelling broadband radiation",
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#BUCHOLTZ.ANTHONY/ fetch 	CAMP2EX-BBR_P3B_%s_R2.ict"
    )

fcdpdata = PatternFile( '../CAMP2Ex/SPEC instruments/CAMP2Ex-FCDP_P3B_%s_R1.ict',
        label="FCDP particle sizes",
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#LAWSON.PAUL/  fetch CAMP2Ex-FCDP_P3B_%s_R1.ict"
    )

fimsdata = PatternFile( '../CAMP2Ex/FIMS/CAMP2EX-FIMS-BINS_P3B_%s_R1.ict',
        label="FIMS particle sizes",
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#WANG.JIAN/  fetch CAMP2EX-FIMS-BINS_P3B_%s_R1.ict"
    )

hvpsdata = PatternFile( '../CAMP2Ex/HVPS/CAMP2Ex-HVPS_P3B_20190915_R0.ict',
        label="HVPS particle sizes",
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#LAWSON.PAUL/  fetch CAMP2Ex-HVPS_P3B_%s_R0.ict"
    )

twods10data = PatternFile( '../CAMP2Ex/SPEC instruments/CAMP2Ex-2DS10_P3B_%s_R0.ict',
        label="2DS10 particle sizes",
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#LAWSON.PAUL/  fetch CAMP2Ex-2DS10_P3B_%s_R1.ict"
    )

dropsondedata = PatternFile( '../CAMP2Ex/Skew T_Dropsonde/%s',
        label="Dropsonde",
        advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#VANDENHEEVER.SUSAN/ fetch all the CAMP2Ex-dropsondes_P3B_%s_R1_L??.ict"
    )

dropsondeblankdata = PatternFile( '../CAMP2Ex/Skew T_Dropsonde/dropsonde1125-blank.png',
        label="Dropsonde Blank",
        advice="Hand cooked to match Rose Miller's image size, 1125x1125."
    )

sattrackdata = PatternFile( '../CAMP2Ex/satellites/%s.kml',
        label="Satellite Tracks",
        advice="From Roman S. Kowch <roman.s.kowch@nasa.gov>" )

# and, for reference, video sources too, though they're not used in these scripts:

forwardquickvideodata = PatternFile( '../CAMP2Ex/video/forward-quick/%s',
    label='Forward Quick(1fps) video',
    advice="From https://asp-archive.arc.nasa.gov/CAMP2EX/N426NA/quick_video/forward/YYYY-MM-DD/N426NA-Forward-YYYYMMDD_quick.mp4" )

forwardfullvideodata = PatternFile( '../CAMP2Ex/video/forward-full/%s',
    label='Forward Full (30fps) video',
    advice="From https://asp-archive.arc.nasa.gov/CAMP2EX/N426NA/video/forward/YYYY-MM-DD/ download all N426NA-Forward-YYYYMMDDTHHMMSS.mp4" )

nadirvideodata = PatternFile( '../CAMP2Ex/video/nadir/%s',
    label='Nadir video',
    advice="From https://asp-archive.arc.nasa.gov/CAMP2EX/N426NA/video/nadir/YYYY-MM-DD/ download all N426NA-Nadir-YYYYMMDDTHHMMSS.mp4" )

allskyvideodata = PatternFile( '../CAMP2Ex/video/allsky/%s',
    label='Allsky video',
    advice="From https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex#BUCHOLTZ.ANTHONY/ download CAMP2EX-ZAC_P3B_%s_R0.avi" )

### ###

trackwbox = [ 7.72, 8.96 ]  # lon,lat diam of box big enough to enclose track for any one day's flight in 2019.
trackboxcen = {'20190915': ( 119.19, 11.21 )}  # lon,lat box center.  See metnav-track-boxes.txt for other days' centers.

# 20190824 20190829 20190830
# 20190906 20190908
# 20190913 20190915 20190916 20190919
# 20190921 20190923 20190927
# 20191001 20191003

## Things could get messed up if some datetime.datetime()'s use UTC, and others use the local timezone.
## So force our local timezone to be UTC, for anyone who imports campexvis!

os.environ['TZ'] = 'UTC0';  time.tzset()

### Time range, by date

tz0 = datetime.timezone( datetime.timedelta(hours=0) )  # UTC timezone
_timebases = {
    '20190915': [ datetime.datetime( 2019, 9, 15, 21, 43, 0 ), datetime.datetime( 2019, 9, 16, 5, 43, 0 ) ]
}

### Visible time window

time_window_width = 20 * (1.0/60) # 20 minutes' width of window.
time_incr = 1.0/3600      # 1 second per frame

####
####

def timerange_from( date, timetype='datetime' ):
    """Given a datetime(), or a 'yyyymmdd' string, or an 8-digit decimal yyyymmdd integer, returns the 3-member list: datetime(0hUTC), datetime(0-point of that day's visualization), datetime(end of that day's vis)."""

    if isinstance(date, (datetime.datetime, datetime.date)):
        date = "%04d%02d%02d" % (date.year, date.month, date.day)

    elif isinstance(date, str):         # yyyy-mm-dd or yyyymmdd
        date = date.replace('-', '')[0:8]

    elif isinstance(date, (int,float)) and date >= 20000000:
        date = "%08d" % date

    if date in _timebases:
        bases = _timebases[date]

    elif len(date) == 8:
        # If not listed, then timebase = 0h UTC, and timerange ends at 6h UTC the following day
        base = datetime.datetime( int(date[0:4]), int(date[4:6]), int(date[6:8]) )
        bases = [ base, base + datetime.timedelta( hours=30 ) ]

    else:
        return None

    Day0 = datetime.datetime( year=int(date[0:4]), month=int(date[4:6]), day=int(date[6:8]) )
    bases = [Day0, bases[0], bases[1]]

    if timetype == 'h_0h' or timetype == 'h_0v':
        zerotime = Day0 if timetype == 'h_0h' else bases[1]
        return [(dtime - zerotime).total_seconds() / 3600.0 for dtime in bases]

    elif timetype == 'datetime':
        return bases

    else:
        raise ValueError("camp2exvis.timerange_from(date, timetype): timetype must be one of 'datetime'(default), 'h_0h' (hours since 0hUTC), 'h_0v'(hours since that day's visualization start-time).  What's " + timetype)

    return bases

def timebase_from( date, timetype='datetime' ):
    trange = timerange_from( date, timetype )
    return None if trange is None else trange[1]


####
#### Colormaps
####

class COWmap(object):
    def __init__(self, cmap=None, panoply=None, badcolor=(0,0,0,0)):
        self.cm = cmap
        self.panoply = panoply
        self.badcolor = badcolor
        self.copied = False

    def cmap(self):
        if not self.copied:
            if self.panoply is not None:
                self.cm = make_panoply_cmap( self.panoply )
                self.cm.name = 'panoply'
            else:
                ccm = self.cm.__copy__()
                if hasattr(self.cm,'name'):
                    ccm.name = self.cm.name + '_copy'
                self.cm = ccm

            self.copied = True

            self.cm.set_bad(color='k', alpha=self.badcolor[3])

        return self.cm

    @staticmethod
    def cmap_glued( chunks, fillcolor=(0,0,0,0), badcolor=(0,0,0,0) ):
        """Assembles chunks of multiple source colormaps into given ranges of the new (destination) colormap.
        Given 'chunks', a list of dictionaries containing:
                drange=(dest0, dest1), # optional, defaults to (0,1)
                srange=(src0, src1),  # optional, defaults to (0,1)
                scmap=srccmap,
                gain=constant or gain=(sgain0,sgain1) # brightness multiplier, defaults to gain=1
        where dest0, dest1, and src0, src1 are all in range 0..1,
        returns a ListedColormap that contains, in range dest0..dest1, the src0..src1 portion of srccmap, and so on."""

        N = 256
        cvals = numpy.zeros( (N,4) )
        if fillcolor is not None:
            cvals[:] = numpy.array( fillcolor ).reshape(-1, 4)
        dimin, dimax = N-1, 0
        for chunk in chunks:
            dv0, dv1 = chunk.get('drange', (0,1))
            sv0, sv1 = chunk.get('srange', (0,1))
            scmap = chunk['scmap']
            gain = chunk.get('gain', 1)
            di = int(0.5+N*dv0), min( int(0.5+N*dv1)+1, N )
            dimin = min(di[0], dimin)
            dimax = max(di[1], dimax)
            sv = numpy.linspace( sv0, sv1, num=(di[1]-di[0]), endpoint=True )
            if hasattr(gain,'__len__'):  # turn a (sequence of equally spaced gain values) list into an array of enough samples
                xin = numpy.linspace( 0, 1, num=len(gain), endpoint=True )
                xout = numpy.linspace( 0, 1, num=(di[1]-di[0]), endpoint=True )
                gain = numpy.interp( xout, xin, gain ).reshape(-1,1)
                
            cvals[ di[0] : di[1] ] = gain * numpy.array( [ scmap(v) for v in sv ] )
        if dimin > 0:
            cvals[ 0:dimin ] = cvals[dimin].reshape(1,4)
        if dimax < N-1:
            cvals[ dimax+1:N ] = cvals[dimax].reshape(1,4)

        newcmap = matplotlib.colors.ListedColormap( cvals )
        newcmap.set_bad(color=badcolor, alpha=badcolor[3])
        return newcmap

Panoply_cmapfile = '../CAMP2Ex/HSRL/CAMP2EX-HSRL2_P3B_R0_PanoplyColorTable.rgb'

## special case for tb11 -- glued-together colormap
# tb11 encoding has -100C = 0/65535, 0C = 30000/65535
# Want a colormap with cool_r from -50C to -10C, then gray from -10C to +50C

tb11_vrange = -70, 50
def tb11C( degC ):
    #return (30000.0 + 300.0*degC) / 65535.0
    # This is how the db_images HIMAImages.make_plot maps the tb11 value.  It's passed the above tb11_vrange from camp2exvis PageAHIMap.prepfig().
    return (degC - tb11_vrange[0]) / (tb11_vrange[1] - tb11_vrange[0])

tb11_cmap = COWmap.cmap_glued(
            [ dict( drange=(tb11C(-70), tb11C(-10)), srange=(0,    1.0),  scmap=matplotlib.cm.cool_r, gain=0.75 ),
              dict( drange=(tb11C(-10),  tb11C(50)), srange=(0.25, 0.75), scmap=matplotlib.cm.gray )
            ],
            fillcolor=(0.2,0.2,0.3,0.5)
            )


_cmaps = {
    'hsrl-plot': COWmap( matplotlib.cm.viridis ), ## COWmap( panoply=Panoply_cmapfile ),
    'hsrl-ribbon': COWmap( matplotlib.cm.viridis ), ## COWmap( panoply=Panoply_cmapfile ),
    #'hsrl-ratio':  COWmap( matplotlib.cm.seismic ),    ## HSRL 1064/532 backscatter ratio is like a red-to-blue color!
    'hsrl-ratio': COWmap( panoply=Panoply_cmapfile ),   ## Nah, use extinction-to-backscatter ratio ("532_Sa") instead, with Panoply cmap
    'ampr':     COWmap( matplotlib.cm.plasma ),
    'ampr-lwc': COWmap( matplotlib.cm.plasma, badcolor=(0,0,0,1) ),     # AMPR liquid water
    'ampr-wind': COWmap( panoply=Panoply_cmapfile ), ## COWmap( matplotlib.cm.viridis ),
    ## 'ampr-2Dwind': COWmap( matplotlib.cm.Paired ),
    'apr3': COWmap.cmap_glued( [ dict(scmap=matplotlib.cm.spring, gain=(0.5, 1, 1)) ] ),  # pink-to-yellow, but turn the low end 50% darker.
    
    'apr3-doppler': COWmap( matplotlib.cm.seismic ),   ## doppler orange-to-purple (like red-to-blue, too)

    'AOT':  COWmap.cmap_glued( [ dict(scmap=matplotlib.cm.copper, srange=(0.3,0.9), drange=(0,0.5)),  ## aerosol optical thickness, underlain by 11-micron microwave temp
                                 dict(scmap=matplotlib.cm.afmhot, srange=(0.5,0.8), drange=(0.5,1.0)) ] ),
    'tb11': tb11_cmap,                              ## special compound colormap for Tb_11 infrared
    'rspcld': COWmap( matplotlib.cm.gist_ncar ),          ## RSP droplet size distribution, curiously with equal extent to + and - um^2 / um^3
    #'apr3-doppler': COWmap( matplotlib.cm.twilight_shifted_r ),   ## dark in middle, going redder on left, bluer on right -- doppler'y
    None:   COWmap( panoply=Panoply_cmapfile )
}

for cname, cmap in _cmaps.items():
    cmap.name = (cname if not hasattr(cmap,'name') else "%s~%s" % (cmap.name, cname))

def make_panoply_cmap(cmapfile):
    """Read a Panoply-format cmap file, create a matplotlib.colors.ListedColormap"""
    with open(cmapfile, 'r') as cmf:
        line = cmf.readline()
        ncolors = int( line.split()[2] ) # "ncolors = 64"
        cmf.readline()      # "# r g b"
        colors = []
        while len(colors) < ncolors:
            line = cmf.readline()
            r, g, b = [ float(v) for v in line.split() ]
            colors.append( (r,g,b,1) )
    cmap = matplotlib.colors.ListedColormap( colors )
    return cmap


def cmap_for( whichvar ):

    which = whichvar
    if which not in _cmaps:
        which = which.lower()

        if which not in _cmaps:
            which = None        # use default

    thatcmap = _cmaps[which]
    if isinstance(thatcmap, COWmap):
        thatcmap = thatcmap.cmap()

    return thatcmap

###
if __name__ == "__main__":

    if len(sys.argv) <= 1:
        print("Usage: %s yyyymmdd  -- check for presence of files on given date, report missing ones" % sys.argv[0])
    else:
        datetag = sys.argv[1]
        missing = []
        for pf in PatternFile.allinstances:
            what = pf[datetag]
            if what is None:
                missing.append( pf.label )

        if len(missing) == 0:
            print("All files present for %s" % datetag)
        else:
            print("\nMissing files on %s for: " % datetag, ", ".join(missing))
