#! /usr/bin/env python3

import sys, os
import datetime
import numpy
import re
import glob

import matplotlib.pyplot
import matplotlib.colors

import camp2exdb
import camp2exvis

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as mp
import matplotlib.colors as mc # for LinearSegmentedColormap
import matplotlib.cm     as cm
import matplotlib.patches
import matplotlib.lines        # for Line2D
import matplotlib.ticker

import cartopy.crs
import cartopy.feature

def ribbon(onax, timerange, tnow, vrange, values, alts, times, cmap, altrange=None, do_log10=False, title=None, maintitle=None, aspectscale=None, do_cbar=True, cut_thresh=None, meshopts={}, titleopts={}, timemarkopts={}, cbaropts={}):
    """Draw a ribbon -- time on horizontal axis, altitude on vertical, each 'values' cell colored by cmap.   Cut time series where gaps are longer than 'cut_thresh' (in hours).   Takes Y range from altrange if present, else min/max of alts.
        If alts is 2-D, then time must be the first (slow-varying) axis.
        'values' is transposed compared with the documentation of axis.pcolormesh(): first index is time(X), second axis is altitude(Y)."""

    if times is None or len(times) == 0:
        # Swallow degenerate inputs.  We still set limits, titles, colorbar even if there's no data.
        j0 = j1 = t0 = t1 = 0
    else:
        j0, j1 = numpy.searchsorted( times, timerange, side='left' )
        j0, j1 = sorted([j0,j1])
        j0 = min(j0, len(times)-1)
        j1 = min(j1, len(times)-1)

        t0 = times[j0]
        t1 = times[j1]

    if altrange is None:
        altrange = (alts.min(), alts.max())
    extent = (t0, t1, altrange[0], altrange[1])

    onax.set_xlim( timerange )
    onax.set_ylim( altrange )

    onax.set_adjustable('box')
    # alternative if we need to override:
    #   print("aspectscale", aspectscale)
    #   if aspectscale is not None:
    #       onax.set_aspect(aspectscale*(timerange[1]-timerange[0])/(extent[3]-extent[2]), adjustable='box')


    if title is not None or maintitle is not None:
        camp2exvis.axis_set_title_styled( onax, (maintitle if maintitle is not None else title), **titleopts )


    if j1 <= j0:
        if do_cbar:
            # print("timerange %g %g => ixrange %d %d => trange %g %g  ... skipping." % (*timerange, j0,j1, t0,t1))

            # image is empty, but we still want the colorbar+label to remain
            dummymappable = cm.ScalarMappable(cmap=cmap)
            dummymappable.set_clim( vrange )
            cbar = onax.figure.colorbar( dummymappable, ax=onax, orientation='vertical', **cbaropts ) #, pad=0.03, fraction=0.13, aspect=5 )
            if title is not None:
                cbar.ax.set_ylabel( title )

            return dummymappable
        return None

    vals = values[j0:j1] # time is the first (slow) axis of vals
    if do_log10:
        # rigamarole to prevent log10 warnings, since it doesn't respect the mask or fill_value of a masked array
        vvalid = numpy.nan_to_num(vals) > 0
        valsnow = numpy.ma.masked_array( numpy.zeros_like( vals ), numpy.logical_not(vvalid) )
        valsnow[vvalid] = numpy.log10( vals[vvalid] )
    else:
        valsnow = numpy.ma.masked_array( vals, numpy.isnan(vals) )
    numpy.clip( valsnow, vrange[0], vrange[1], out=valsnow )
    # print("timerange %g %g => ixrange %d %d => trange %g %g  vrange %g %g valuesrange %g %g" % (*timerange, j0,j1, t0,t1, *vrange, numpy.nanmin(valsnow), numpy.nanmax(valsnow)))
    
    ## norma = matplotlib.colors.LogNorm( vmin=vrange[0], vmax=vrange[1] )
    timesnow = times[j0 : min(j1, len(times)-2)+1]
    if len(alts.shape) == 2:
        altsnow = alts[j0 : min(j1, len(times)-2)+1]
    else:
        altsnow = alts
    if len(valsnow) > len(timesnow):    # handle off-by-one case
        valsnow = valsnow[0:len(timesnow)]

    # Does the time series have gaps bigger than cut_thresh (probably measured in hours)?
    cgaps = [] if cut_thresh is None else numpy.argwhere( timesnow[1:] - timesnow[:-1] > cut_thresh ).ravel() + 1
    # cgaps => indices where there's a gap exceeding threshold
    cuts = [0] + list(cgaps) + [len(timesnow)]

    ## print("ribbon() cuts: "," ".join(["%d"%c for c in cuts]))
    # XXX NOTE: this cutting scheme will cause the last column of values[] cells before each gap to be un-drawn.
    # XXX To change that, we'd need to guess at how long a time interval it should cover -- perhaps equal to the previous cell's duration?
    for i in range(len(cuts)-1):
        cslice = slice( cuts[i], cuts[i+1] )
        # time is a 1-D array, but alts might be a 2-D array, as for APR, where altitude of N'th grid cell varies with time.
        # If so, broadcast time to be same shape as altitude.
        timeslice = timesnow[cslice]
        if len(altsnow.shape) == 2:
            altslice = altsnow[cslice]
            timeslice = numpy.broadcast_to( timesnow[cslice].reshape(-1,1), altslice.shape )
            valslice = valsnow[cslice]  # not transposed
        else:
            altslice = altsnow
            valslice = valsnow[cslice].transpose()
        implain = onax.pcolormesh( timeslice, altslice, valslice, vmin=vrange[0], vmax=vrange[1], cmap=cmap, **meshopts )

    if tnow is not None:
        # add a gray marker showing the current time - a pair of dotted lines, or (if tnow is a list of times) a dotted line for each of the given times.
        if hasattr(tnow, '__iter__'):
            tmarks = tnow
        else:
            rectwidth = 0.01 * (t1-t0)
            tmarks = (tnow-0.5*rectwidth, tnow+0.5*rectwidth)
        for tmark in tmarks:
            mopts = dict(color='#B0C0D050', linestyle=':', linewidth=2.0)
            mopts.update( timemarkopts )  # let timemarkopts override our defaults
            onax.add_line( matplotlib.lines.Line2D( (tmark,tmark), altrange, **mopts ) )

    if do_cbar:
        cbar = onax.figure.colorbar( implain, ax=onax, orientation='vertical', **cbaropts ) # , pad=0.03, fraction=0.13, aspect=5 )
        if title is not None:
            cbar.ax.set_ylabel( title )


    return implain


def timemark(onax, tnow):
    # add a gray marker showing the current time - a pair of dotted lines.
    trange = onax.get_xlim()
    vrange = onax.get_ylim()
    rectwidth = 0.01 * (trange[1]-trange[0])
    onax.add_line( matplotlib.lines.Line2D( (tnow-0.5*rectwidth,tnow-0.5*rectwidth), vrange, color='#B0C0D050', linestyle=':', linewidth=2.0 ) )
    onax.add_line( matplotlib.lines.Line2D( (tnow+0.5*rectwidth,tnow+0.5*rectwidth), vrange, color='#B0C0D050', linestyle=':', linewidth=2.0 ) )


def nudge_ylabel( ax, newx, newy=0.5, label=None ):
    yla = ax.get_ylabel() if label is None else label
    ax.set_ylabel('')
    ax.figure.add_artist( matplotlib.text.Text( x=newx, y=newy, text=yla, rotation=90, verticalalignment='center', horizontalalignment='center', transform=ax.transAxes ) )
    

def nudge_xlabel( ax, newy, newx=0.5, label=None ):
    xla = ax.get_xlabel() if label is None else label
    ax.set_xlabel('')
    ax.figure.add_artist( matplotlib.text.Text( x=newx, y=newy, text=xla, rotation=0, verticalalignment='center', horizontalalignment='center', transform=ax.transAxes ) )
    

#
# Helper functions for nicer time-axes
def hmstick( hours, etc ):
    secs = int(hours*3600+0.5)
    h = int(secs/3600)
    m = int((secs-3600*h)/60)
    s = secs%60
    return "%02d:%02d:%02d" % (h, m, s)

HMSFormatter = matplotlib.ticker.FuncFormatter( hmstick )
NullFormatter = matplotlib.ticker.NullFormatter()

def xtimeticks( axis, timerange, timenow=None, showtime=True, wantticks=4 ):
    """Add clock-time-based ticks to 'axis's X-axis.
       If timenow provided, add a tick there with the current time on it.
       timerange[] and timenow are in floating-point hours since 0h UTC."""
    time0, time1 = sorted( list(timerange) )
    dt_h = time1-time0  # delta time in hours
    dt_sec = dt_h * 3600.0

    spacings_s = numpy.array( [ 1, 2, 5, 10, 15, 30, 60, 120, 300, 600, 900, 1800, 3600 ] )
    tickseach = dt_sec / spacings_s
    # Want to choose a spacing such that ...
    #  there's a tick at the center (= timenow)
    #  there are 1-3 ticks to its left and right
    #  no tick is closer than trimleft (0.15?) of the end
    #  tick spacing (in seconds) is one of the given spacings_s times
    # 
    tickmargin = 0.1
    halfseconds = (0.5 - tickmargin) * dt_sec
    ticksper = halfseconds / spacings_s
    wanted = 0.5*wantticks    # aim for about two ticks per half-axis, not counting the centermost one
    ibest = numpy.argmin( (ticksper/wanted) + (wanted/ticksper) )
    
    spacing_h = spacings_s[ ibest ] / 3600.0

    firsttick = time0 + numpy.remainder( 0.5*dt_h, spacing_h )
    ticktimes = numpy.arange( firsttick, time1 - tickmargin*dt_h, spacing_h )

    axis.set_xticks( ticktimes )

    if showtime:
        axis.xaxis.set_major_formatter( HMSFormatter )
        ## axis.tick_params( axis='x', labelrotation=30 )

    else:
        axis.xaxis.set_major_formatter( NullFormatter )


####

class Legender(object):
    def __init__(self):
        self.legends = []

    def append(self, pair):
        """Add an (artist,label) to the legend-to-be"""
        self.legends.append( pair )

    def extend(self, pairlist):
        self.legends.extend( pairlist )

    def legend(self, axis, **kwargs):
        """Apply accumulated legend entries.   Do this after all the plots etc have been added"""
        if len(self.legends) > 0:
            axis.legend( [artist for (artist,label) in self.legends], [label for (artist,label) in self.legends], **kwargs )
        
### ### ###

class Page(object):

    page = {}


    @staticmethod
    def allpages():
        allp = []
        for sc in Page.__subclasses__():
            Page.page[ sc.stemname ] = sc
            allp.append( sc.__name__ )
        print("\nRegistered: %s" % " ".join(sorted(allp)))
        return Page.page

    resolution = (768, 400)  # image resolution, to be overridden by per-Page subclasses which know how many pixels they need
    renderdpi = 200          # not necessarily overridden
    antialias = 2.0


    def __init__(self, datetag=None):
        self.connected = False
        self.datetag = datetag


    def setdate(self, datetag):
        if datetag != self.datetag:
            self.connected = False
            self.datetag = datetag

    def set_fig_size(self, fig):
        # convenience function.  Every prepfig() should do this.
        fig.set_size_inches( self.resolution[0]*self.antialias/self.renderdpi, self.resolution[1]*self.antialias/self.renderdpi )

    # subclasses define a connect_data(self) method and a prepfig(self, trange, tcenter) method.

class PageSSFR(Page):
    stemname = 'ssfr'
    resolution = (677, 266)
    nrows, ncols = (1, 1)

    def __init__(self, datetag):
        super().__init__(datetag)

        ## self.backcolor = (0,0,0,0) # was '#8B4C39'  # salmon4 from X11/rgb.txt

    def connect_data(self):
        if self.connected:
            return
        self.connected = True

        from db_ssfr import SSFR
        from db_spns import SPNS

        self.ssfr = SSFR( camp2exvis.ssfrdata[ self.datetag ], self.datetag )
        self.spns = SPNS( camp2exvis.spnsdata[ self.datetag ], self.datetag )

    plottitle = [ "Solar Spectral Flux\nRadiometer\n(SSFR/SPNS)" ]

    def prepfig(self, trange, tcenter):

        self.connect_data()

        fig, axSSFR = matplotlib.pyplot.subplots(nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False)

        self.set_fig_size(fig)

        self.ssfr.make_plot(0, axSSFR, trange, tcenter)
        self.spns.make_plot(0, axSSFR, trange, tcenter)

        camp2exvis.axis_set_title_styled( axSSFR, self.plottitle[0] )

        axSSFR.set_aspect('auto')

        return fig


class PageBBR(Page):
    stemname = 'bbr'
    resolution = (677, 266)
    nrows, ncols = (1, 1)

    def __init__(self, datetag):
        super().__init__(datetag)

        ## self.backcolor = (0,0,0,0) # was '#8B4C39'  # salmon4 from X11/rgb.txt

    def connect_data(self):
        if self.connected:
            return
        self.connected = True

        from db_spntransmittance import SPNTransmittance
        from db_bbr import BBR

        self.spntrans = SPNTransmittance( camp2exvis.spntransdata[ self.datetag ], self.datetag )
        self.bbr = BBR( camp2exvis.bbrdata[ self.datetag ], self.datetag )

    plottitle = [ "BBR Down/Up +\nSPN-S Transmittance" ]

    def prepfig(self, trange, tcenter):

        self.connect_data()

        fig, axSSTrans = matplotlib.pyplot.subplots(nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False)

        self.set_fig_size(fig)

        axBBR = axSSTrans.twinx()

        self.spntrans.make_plot(0, axSSTrans, trange, tcenter)
        camp2exvis.axis_set_title_styled( axBBR, self.plottitle[0], y=0.895 )
        axSSTrans.legend( loc='upper left' )
        axSSTrans.set_ylabel( 'transmittance' )
        nudge_ylabel( axSSTrans, -0.082 )

        ##legends = Legender()
        ##self.bbr.make_plot(0, axBBR, trange, tcenter, legends=legends)
        ##legends.legend( axBBR, loc='upper right' )
        self.bbr.make_plot( 0, axBBR, trange, tcenter )
        nudge_ylabel( axBBR, 1.11 )
        axBBR.legend( loc='upper right' )

        # axSSTrans/axBBR is time-dependent
        timemark( axSSTrans, tcenter )
        xtimeticks(axSSTrans, trange, tcenter, showtime=True)

        axSSTrans.set_aspect('auto')

        return fig

#####

class PageHSRL(Page):

    stemname = 'hsrl'    # name of page image

    resolution = ( 736, 800 )
    nrows, ncols = ( 3, 1 )

    def __init__(self, datetag=None):
        super().__init__( datetag )


    def connect_data(self):
        if self.connected:
            return

        self.connected = True

        self.ribbon_plot_cmap = camp2exvis.cmap_for( 'hsrl-plot' )
        self.ribbon_ratio_cmap = camp2exvis.cmap_for( 'hsrl-ratio' )

        from db_hsrl import HSRL

        self.hsrl = HSRL(camp2exvis.hsrldata[self.datetag])         # XXX default input for 20190915
        print("HSRL time range %g .. %g on %s" % ( *self.hsrl.timerange, self.hsrl.Day0))

        self.hsrl.use_freq( 3600/1 ) # subsample to about 1 sample every second

        self.hsrl_dust = self.hsrl.ribbon_for( '/DataProducts/Dust_Mixing_Ratio' )
        self.hsrl_532bsc = self.hsrl.ribbon_for( '/DataProducts/532_bsc' )
        self.hsrl_532aot = self.hsrl.ribbon_for( '/DataProducts/532_AOT_above_cloud' )  # just a scalar function of time, not a ribbon

        self.hsrl_lidar_ratio = self.hsrl.ribbon_for( '/DataProducts/532_Sa' )


        # also non-ribbon scalars
        self.hsrl_altitude = self.hsrl.ribbon_for( '/Nav_Data/gps_alt' )
        ## hsrl_cloudtop = hsrl.ribbon_for( '/DataProducts/cloud_top_height' )
        self.hsrl_532depol = self.hsrl.ribbon_for( '/DataProducts/532_dep' )


        
    def prepfig(self, trange, tcenter):

        self.connect_data()

        fig, (axhsrl1, axhsrldepol, axhsrlrat) = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False ) # sharey=False, right?
        self.set_fig_size(fig)

        for ax in axhsrl1, axhsrldepol, axhsrlrat: ## , axrsp
            ax.set_facecolor( '#30303080' )    # See also mp.style.use(...) above, to set dark-background theme

        timelabel = ( self.hsrl.Day0 + datetime.timedelta(hours=tcenter) ).strftime('%Y-%m-%d %H:%M:%S')
        cbaropts = dict( pad=0.02, fraction=0.06, shrink=0.80, aspect=20 )

        ribbon( onax=axhsrl1, timerange=trange, do_log10=True, vrange=(-4, -1), tnow=tcenter, \
                **self.hsrl_532bsc,  \
                title="log HSRL 532 bsc",  \
                maintitle="HSRL 532 bsc",  \
                cmap=self.ribbon_plot_cmap,
                cbaropts=cbaropts )

        ribbon( onax=axhsrldepol, timerange=trange, vrange=(0.0, 0.3), tnow=tcenter, \
                **self.hsrl_dust, \
                title="HSRL Dust Mixing Ratio(532)", cmap=self.ribbon_plot_cmap,
                cbaropts=cbaropts )

        ribbon( onax=axhsrlrat, timerange=trange, vrange=(0.0, 120.0), tnow=tcenter, \
                **self.hsrl_lidar_ratio, \
                title="extinc/bsc (532_Sa)",
                maintitle="HSRL extinction/backscatter (532_Sa)",
                cmap=self.ribbon_ratio_cmap,
                cbaropts=cbaropts )

        ##ribbon( onax=axhsrldust, timerange=trange, tnow=tcenter, **hsrl_dust, title="HSRL Dust Ratio", cmap=ribbon_plot_cmap )
        #Naw, this is wrong:# axhsrl1.plot( self.hsrl_532depol['times'], self.hsrl_532depol['values'], 'g', alpha=0.4 )
        axhsrl1.plot( self.hsrl_altitude['times'], self.hsrl_altitude['values'], 'r', alpha=0.4 )

        for ax in axhsrl1, axhsrldepol, axhsrlrat:
            xtimeticks(ax, trange, tcenter, showtime=(ax==axhsrlrat))

        return fig

class PageAMPR(Page):

    stemname = 'ampr'   # name of page image
    resolution = ( 736, 1080 )
    nrows, ncols = 5, 1


    def __init__(self, datetag=None):
        print("PageAMPR(%s)" % datetag)
        super().__init__(datetag)

    def connect_data(self):
        if self.connected:
            return
        self.connected = True

        from db_ampr import AMPR

        self.ampr = AMPR( camp2exvis.amprdata[self.datetag], self.datetag )  # datetag not passed - AMPR() pulls date from data
        self.ampr_ribbon_plot_cmap = camp2exvis.cmap_for( 'ampr' )
        self.ampr_ribbon_LWC_cmap = camp2exvis.cmap_for( 'ampr-lwc' )
        self.ampr_ribbon_2Dwind_cmap = camp2exvis.cmap_for( 'ampr-wind' )

        from db_mergedscalars import MergedScalars

        self.mergedscalars = MergedScalars( camp2exvis.mergedscalardata[self.datetag], self.datetag )

        from db_amprwind import AMPRwind

        self.amprwind = AMPRwind( camp2exvis.amprwinddata[self.datetag], self.datetag )
        self.show_amprwind_1D = False #  otherwise, would try using 2D cross-track AMPR wind
        if os.getenv("AMPR1D") is not None:
            self.show_amprwind_1D = True

    def prepfig(self, trange, tcenter):

        self.connect_data()

        ###fig, (axTb10, axTb37, axTb85, axampRGB, axLWC) = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False )
        fig, (axTb85, axLWC, axAMPRwind, axwind, axCO) = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False )
        self.set_fig_size(fig)

        cbaropts = dict( pad=0.02, fraction=0.06, shrink=0.80, aspect=20 )
        ## self.ampr.make_plot( 0, axTb37, trange, tcenter, cmap=self.ampr_ribbon_plot_cmap, cbaropts=cbaropts )
        self.ampr.make_plot( 1, axTb85, trange, tcenter, cmap=self.ampr_ribbon_plot_cmap, cbaropts=cbaropts )

        # AMPR's estimate of wind speed (without direction)
        if self.show_amprwind_1D:
            self.amprwind.make_plot( 0, axAMPRwind, trange, tcenter )
            axAMPRwind.set_ylabel( '$m/s$' )
            axAMPRwind.legend( loc='upper left' )
        else:
            self.ampr.make_plot( 4, axAMPRwind, trange, tcenter, cmap=self.ampr_ribbon_2Dwind_cmap, cbaropts=cbaropts )
            axAMPRwind.set_ylabel( '' )  # swath angle, like the others

        """make_plot_LARGE(): 0: nLAS  1: nAPS   2: Uwind  3: Vwind  4: Wwind"""

        axWwind = axwind.twinx()
        for i in 2,3:
            self.mergedscalars.make_plot_LARGE( i, axwind, trange, tcenter )

        self.mergedscalars.make_plot_LARGE( 4, axWwind, trange, tcenter )

        axCH4 = axCO.twinx()
        self.mergedscalars.make_plot_DISKIN( 0, axCO, trange, tcenter )
        self.mergedscalars.make_plot_DISKIN( 1, axCH4, trange, tcenter )
        axCO.legend( loc='upper left' )
        axCH4.legend( loc='upper right' )
        camp2exvis.axis_set_title_styled( axCO, '$CO~and~CH_4$' )
        axCO.set_yticks( [0.03, 0.1, 0.3, 1.0] )
        axCO.set_yticklabels( ["%g"%v for v in axCO.get_yticks()] )
        nudge_ylabel( axCO, -0.08 )

        ##axWwind.set_ylabel( 'W $m/s$' )
        # XXX Manually-placed y-scale label for W wind.  Unhappy with placement of automatic label.
        nudge_ylabel( axWwind, 1.09, label='W $m/s$' )

        nudge_ylabel( axwind, -0.08, label='U,V $m/s$' )

        axwind.legend( loc='upper left' )
        axWwind.legend( loc='upper right' )

        camp2exvis.axis_set_title_styled( axAMPRwind, "AMPR\nWind Speed" )
        camp2exvis.axis_set_title_styled( axwind, "TAMMS 3D Winds" )

        # omitting make_plot(3 ...) = 19GHz
        self.ampr.make_plot( 5, axLWC,  trange, tcenter, cmap=self.ampr_ribbon_LWC_cmap, cbaropts=cbaropts )

        # Some plots don't have colorbars.   Align them with the ones that do.
        if self.show_amprwind_1D:
            bareplots = [ axAMPRwind, axwind, axCO, axCH4 ]
        else:
            bareplots = [ axwind, axCO, axCH4 ]
        axmatch = axLWC
        for ax in bareplots:
            otherxywh = axmatch.get_position().bounds  # match the X-position of the other axis, keep our Y-position.
            myxywh = ax.get_position().bounds
            ax.set_position( (otherxywh[0], myxywh[1], otherxywh[2], myxywh[3]) )

        #for ax in axTb37, axTb85, axLWC, axAMPRwind, axwind:
        for ax in axTb85, axLWC, axAMPRwind, axwind, axCO:
            timemark( ax, tcenter )
            xtimeticks(ax, trange, tcenter, showtime=(ax==axCO))
            

        return fig

    ## plotlabels = [ "T_b 37 (K)",  "T_b 85 (K)",  "LWC (kg/m^2)" ]   # AMPR microwave temperatures (K) at 37, 85, 10 GHz, and liquid water content

class PageCPI(Page):

    stemname = 'cpi'
    resolution = ( 521, 673 )
    nrows, ncols = 1, 1


    cpibackcolor = (0.0, 0.0, 0.0, 1.0)

    cpistubcolor = '#787878FF' if os.getenv('CPIBGCOLOR') is None else os.getenv('CPIBGCOLOR')
    

    def __init__(self, datetag=None):
        super().__init__( datetag )

    def connect_data(self):
        if self.connected:
            return
        self.connected = True

        from db_images import CPIImages

        ######
        # Inhale CPI images

        self.cpiimages = CPIImages(camp2exvis.cpidata[self.datetag], self.datetag)


    def prepfig(self, trange, tcenter):

        self.connect_data()
        fig, axcpi = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False, gridspec_kw=dict(top=1, bottom=0, left=0, right=1) )
        self.set_fig_size(fig)

        # CPI images
        axcpi.set_facecolor( self.cpibackcolor )
        axcpi.patch.set_visible(False)                      # will this make the white frame vanish?

        cpimrgb = self.cpiimages.image_at_time( tcenter )
        if cpimrgb is not None:
            shp = cpimrgb.shape
            #axcpi.imshow( cpimrgb, extent=(0, shp[1], 0, shp[0]) )
            axcpi.imshow( cpimrgb.transpose(0,1,2)[:, :, :], extent=(0, shp[1], 0, shp[0]) )
        else:
            shp = (self.resolution[1], self.resolution[0])
            axcpi.add_patch( matplotlib.patches.Rectangle( (0,0), shp[1], shp[0], fill=True, color=self.cpistubcolor ) )
            camp2exvis.axis_set_title_styled( axcpi, "CPI\n(no recent data)", y=0.965 )

        axcpi.set_frame_on( False )
        axcpi.set_xlim(0, shp[1])
        axcpi.set_ylim(0, shp[0])

        axcpi.set_yticks([])
        axcpi.set_xticks([])

        return fig      # always return fig to indicate success!


class PageRAINY(Page):

    stemname = '_rainy'    # name of page image.   Combo of one plot apiece from HSRL backscatter + AMPR + APR, one plot apiece, as a test replacement for at least HSRL and AMPR pages.   Yulan Hong's suggestion.

    resolution = ( 768, 960 )
    nrows, ncols = ( 3, 1 )

    def __init__(self, datetag=None):
        super().__init__( datetag )


    def connect_data(self):
        if self.connected:
            return

        self.connected = True

        self.ribbon_plot_cmap = camp2exvis.cmap_for( 'hsrl-plot' )

        from db_hsrl import HSRL

        self.hsrl = HSRL(camp2exvis.hsrldata[self.datetag])         # XXX default input for 20190915

        self.hsrl.use_freq( 3600/1 ) # subsample to about 1 sample every second

        self.hsrl_532bsc = self.hsrl.ribbon_for( '/DataProducts/532_bsc' )
        # also non-ribbon scalars
        self.hsrl_altitude = self.hsrl.ribbon_for( '/Nav_Data/gps_alt' )

        # AMPR
        from db_ampr import AMPR

        self.ampr = AMPR(camp2exvis.amprdata[self.datetag])
        self.ampr_ribbon_plot_cmap = camp2exvis.cmap_for( 'ampr' )

        from db_apr3 import APR3

        self.apr3 = APR3( camp2exvis.apr3data[self.datetag], self.datetag )
        self.dbzcmap = camp2exvis.cmap_for( 'apr3' )


    def prepfig(self, trange, tcenter):
        """'rainy' combo of HSRL/AMPR/APR"""

        self.connect_data()

        fig, (axapr3, axampr, axLWC) = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=True, sharey=False, facecolor=self.backcolor )
        self.set_fig_size( fig )

        timelabel = ( self.hsrl.Day0 + datetime.timedelta(hours=tcenter) ).strftime('%Y-%m-%D %H:%M:%S')

        ##if False:
        ##    ribbon( onax=axhsrl, timerange=trange, do_log10=True, vrange=(-4, -1), tnow=tcenter, \
        ##        **self.hsrl_532bsc,  \
        ##        title="log HSRL 532 bsc",  \
        ##        maintitle="HSRL 532 bsc",  \
        ##        cmap=self.ribbon_plot_cmap )
        ## axampr.set_title( 'AMPR 35GHz', y=0.95, pad=-14 )

        ribbon( onax=axapr3, timerange=trange, do_log10=True, vrange=(-4, -1), tnow=tcenter, \
                **self.hsrl_532bsc,  \
                title="log HSRL 532 bsc",  \
                maintitle="",  \
                cmap=self.ribbon_plot_cmap,
                meshopts={'zorder':2.2, 'alpha':0.7} )

        imp = self.apr3.make_plot_nadir( 0, axapr3, trange, tcenter, do_cbar=False, vthresh=-20, meshopts={'zorder':1.5} ) # W-band nadir ribbon overlying HSRL.  Cut out faint stuff below 0 dBz.  Put below HSRL ribbon, so APR shows only where HSRL is absent

        if imp is None:
            imp = matplotlib.cm.ScalarMappable(cmap=self.apr3.dbzcmap)

        ax_for_aprcbar = axapr3.inset_axes( [1.025, 0.00,  0.025, 1.00], transform=axapr3.transAxes )
        ## aprcbar = axapr3.figure.colorbar( imp, cax=ax_for_aprcbar, ax=axapr3, orientation='vertical', cmap=self.apr3.dbzcmap )
        aprcbar = axapr3.figure.colorbar( imp, cax=ax_for_aprcbar, ax=[axapr3,axampr,axLWC], orientation='vertical', cmap=self.apr3.dbzcmap, pad=0.18, fraction=0.12, shrink=0.37, aspect=30 )
        aprcbar.ax.set_ylabel( "APR-3 W $(dBz)$" )
        aprcbar.ax.yaxis.set_label_position('left')  # HOWTO: put colorbar label+ticks above the bar, instead of below
        aprcbar.ax.yaxis.set_ticks_position('left')  # thanks to https://stackoverflow.com/questions/12895207/place-a-colorbar-label-above-horizontal-colorbar-instead-of-below
        

        camp2exvis.axis_set_title_styled( axapr3, 'HSRL 532 bsc + APR-3 W-band nadir' )

        # dummymappable = cm.ScalarMappable(cmap=self.dbzcmap)
        # dummymappable.set_clim( (-20,70) )
        # aprcbar = axapr3.figure.colorbar( dummymappable, ax=axapr3, orientation='vertical', cmap=self.dbzcmap ) #, pad=0.03, fraction=0.13, aspect=5 )

        self.ampr.make_plot_RGB( 0, axampr, trange, tcenter, chancodes=[85, 37, 10], axmatch=axapr3 )  # try "rgb" style AMPR

        self.ampr.make_plot( 4, axLWC,  trange, tcenter )

        for ax in (axampr, axapr3, axLWC):
            timemark( ax, tcenter )
            xtimeticks(ax, trange, tcenter, showtime=(ax==axLWC))

        return fig

class PageHSRLAPR(Page):

    stemname = '_hsrlapr'
    resolution = (4096, 256)
    nrows, ncols = (1,1)

    def __init__(self, datetag=None):
        super().__init__( datetag )


    def connect_data(self):
        if self.connected:
            return

        self.connected = True

        self.ribbon_plot_cmap = camp2exvis.cmap_for( 'hsrl-plot' )

        from db_hsrl import HSRL

        self.hsrl = HSRL(camp2exvis.hsrldata[self.datetag])         # XXX default input for 20190915

        self.hsrl.use_freq( 3600/1 ) # subsample to about 1 sample every second

        self.hsrl_532bsc = self.hsrl.ribbon_for( '/DataProducts/532_bsc' )

        from db_apr3 import APR3

        self.apr3 = APR3( camp2exvis.apr3data[self.datetag], self.datetag )
        self.dbzcmap = camp2exvis.cmap_for( 'apr3' )


    def prepfig(self, trange, tcenter):
        """bare-image combo of HSRL+APR"""

        self.connect_data()

        ## day00, tstart_h, tend_h = camp2exvis.timerange_from( self.datetag, 'h_0h' )
        ## trange = (tstart_h, tend_h)
        trange = self.hsrl.timerange # in hours from 0h UTC on flight date.  We'll write a texture that spans the HSRL acquisition lifetime, since that is the extent of the ribbon that hsrlidar.py writes.

        debug = False
        if debug:
            fig, ax = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=True, sharey=False, facecolor=(0,0,0,0), gridspec_kw=dict(left=0,right=1,bottom=0.1,top=0.9,wspace=0,hspace=0) )
        else:
            fig, ax = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=True, sharey=False, facecolor=(0,0,0,0), gridspec_kw=dict(left=0,right=1,bottom=0,top=1,wspace=0,hspace=0) )
        self.set_fig_size( fig )

        ribbon( onax=ax, timerange=trange, do_log10=True, vrange=(-4, -1), tnow=[], \
                **self.hsrl_532bsc,  \
                do_cbar=False,
                title="",  \
                maintitle="",  \
                cmap=self.ribbon_plot_cmap,
                meshopts={'zorder':2.2, 'alpha':0.7} )

        imp = self.apr3.make_plot_nadir( 0, ax, trange, [], do_cbar=False, vthresh=-20, meshopts={'zorder':1.5} ) # W-band nadir ribbon overlying HSRL.  Cut out faint stuff below 0 dBz.  Put below HSRL ribbon, so APR shows only where HSRL is absent

        if not debug:
            ax.set_title('')
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_xlabel('')
            ax.set_ylabel('')

            ax.margins(0, tight=True)  # like 'tight'
            ax.set_aspect('auto')

            ax.set_facecolor( (0,0,0,0) )

        ## ax.set_ylim( -0.500, 7.295 )   # magic numbers from range of HSRL altitude grid cells (-500m .. +7294.6m)

        return fig


class PageAPR3face(Page):

    stemname = '_apr3face'
    resolution = ( 512, 512 )
    nrows, ncols = 1, 1    

    def __init__(self, datetag):
        super().__init__(datetag)

    def connect_data(self):
        if self.connected:
            return

        self.connected = True

        from db_apr3 import APR3

        self.apr3 = APR3( camp2exvis.apr3data[ self.datetag ], self.datetag )

        self.dbzcmap = camp2exvis.cmap_for( 'apr3' )
        self.velcmap = camp2exvis.cmap_for( 'apr3' )        # XXX FIXME2 -- want discrete cmap

    def prepfig(self, trange, tcenter):

        self.connect_data()

        fig, ax  = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False )

        self.set_fig_size( fig )

        raise NotImplementedError("PageAPR3face not yet implemented")

        ## make this look like PageHSRLAPR

        for ax in axWnadir, axKanadir:
            timemark( ax, tcenter )
            xtimeticks( ax, trange, tcenter, showtime=(ax==axKanadir) )
            ax.set_xlim( trange )
            ax.set_ylim( (0,7) )


        self.apr3.make_plot_nadir( 0, axWnadir, trange, tcenter, do_cbar=True )
        self.apr3.make_plot_nadir( 1, axKanadir, trange, tcenter, do_cbar=True )

        camp2exvis.axis_set_title_styled( axWnadir, 'APR-3 W-band nadir' )
        camp2exvis.axis_set_title_styled( axKanadir, 'APR-3 Ka-band nadir' )

        impz = self.apr3.make_plot_scan( 0, axKascandbz, trange, tcenter )
        impvel = self.apr3.make_plot_scan( 1, axKascanvel, trange, tcenter )

        return fig

        

class PageRSP(Page):

    stemname = 'rsp'
    resolution = ( 677, 1408 )
    nrows, ncols = 5, 1


    def __init__(self, datetag=None):
        super().__init__( datetag )

    def connect_data(self):
        if self.connected:
            return
        self.connected = True

        from db_rsp import RSP
        from db_rspcld import RSPCld
        from db_hsrl import HSRL
        from db_mergedscalars import MergedScalars

        self.rsp = RSP( camp2exvis.rspdata[ self.datetag ], self.datetag )
        self.rspcld = RSPCld( camp2exvis.rspclddata[ self.datetag ], self.datetag )
        self.hsrl = HSRL(camp2exvis.hsrldata[self.datetag], self.datetag )         # XXX default input for 20190915
        self.mergedscalars = MergedScalars( camp2exvis.mergedscalardata[self.datetag], self.datetag )

        from db_fcdp import FCDP
        self.hvps = FCDP( camp2exvis.hvpsdata[self.datetag], self.datetag )  # HVPS is a lot like FCDP, with cbin<N> and bin-size-range info in .ict header

        from db_ictable import ICTable

        self.largeopti = ICTable( camp2exvis.largeopticaldata[self.datetag], self.datetag, [ 'Sc550_total', 'SSA_dry_550nm' ] )

        self.YELLOW = (1,1,0,1) # full YELLOW, brighter than 'y' yellow

    plottitleRSP = [ "RSP Droplet Size Dist",  "RSP $R_{eff} (\mu{m})$",      "RSP Cloud\nOptical Depth",  "RSP/HSRL\nCloud Top Height", "Aerosol Optical\nThickness $(532)$" ]
    plottitleOFF = [ "HVPS Size Distribution", "LARGE\nLAS / APS", 'LARGE Optical\n550$nm$\nDry Coefficients',     "HSRL cloud\ntop height $(km)$",  "Aerosol Optical\nThickness" ]


    def prepfig(self, trange, tcenter):

        self.connect_data()

        rsp_is_on = self.rsp.any_data_during( trange )

        # axes: (axReff, axOT, axCloudtop, axAOD)
        fig, axes = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False )
        self.set_fig_size(fig)

        ytitler = {}
        axuntimed = set()

        if rsp_is_on:
            (axDSD, axReff, axOT, axCloudtop, axAOD) = axes
            plottitles = self.plottitleRSP

            self.rspcld.make_plot(0, axDSD, trange, tcenter)
            axDSD.set_yticks( [2,3,4,5,6,7,8,9,20,30,40,50,60,70,80,90], minor=True )
            axDSD.set_yticks( [1, 3, 10, 30, 100] )
            axDSD.set_yticklabels( ['1', '3', '10', '30', '100'] )

            nudge_ylabel( axDSD, -0.072 )

            legends = Legender()
            self.rsp.make_plot(0, axReff, trange, tcenter, legends=legends)       # RSP droplet effective radius measured at 2260nm  (0-35 um)

            self.rsp.make_plot(1, axReff, trange, tcenter, legends=legends)       # RSP droplet - cloudbow-measured effective radius (ditto)
            # Move y-axis label in a bit
            nudge_ylabel( axReff, -0.072 )

            legends.legend( axReff, loc='upper left' )

            self.rsp.make_plot(2, axOT, trange, tcenter)         # RSP cloud optical-thickness (0 - 150)
            nudge_ylabel( axOT, -0.09 )

            axOT.legend( loc='upper right' )

            self.hsrl.make_plot(2, axCloudtop, trange, tcenter)  # HSRL aircraft altitude
            self.rsp.make_plot(3, axCloudtop, trange, tcenter)   # RSP cloudtop altitude (0-8 km)
            self.hsrl.make_plot(1, axCloudtop, trange, tcenter)  # HSRL cloudtop altitude

        else:
            (axHVPS, axLARGE, axLARGEOptical, axCloudtop, axAOD) = axes
            plottitles = self.plottitleOFF

            axuntimed.add( axHVPS )

            self.hvps.make_plot(0, axHVPS, trange, tcenter, color=self.YELLOW, vscale=1.0, label='HVPS @')
            axHVPS.set_xlim( 75, 50000 )
            axHVPS.set_xscale( 'log' )
            axHVPS.set_xticks( [100, 300, 1000, 3000, 10000, 20000, 50000], minor=False )
            axHVPS.set_xticklabels( [("%g$\\mu{m}$"%v if v <= 1000 else "%g$mm$"%(v/1000)) for v in axHVPS.get_xticks()] )
            axHVPS.set_xlabel('')
            axHVPS.set_yscale( 'log' )
            axHVPS.set_ylim( 0.001, 1.0 )
            nudge_ylabel( axHVPS, -0.10 )


            """make_plot_LARGE(): 0: nLAS  1: nAPS   2: Uwind  3: Vwind  4: Wwind"""
            self.mergedscalars.make_plot_LARGE(0, axLARGE, trange, tcenter)
            self.mergedscalars.make_plot_LARGE(1, axLARGE, trange, tcenter)

            axLARGE.set_ylim( 1, 10000 )
            axLARGE.set_yscale( 'log' )
            #axLARGE.set_yticklabels( [ "%g" % (v*0.001) for v in axLARGE.get_yticks() ] )
            axLARGE.set_yticklabels( [ "%g" % v for v in axLARGE.get_yticks() ] )
            ## axLARGE.set_ylabel( '$n/cm^3 \\times 1000$' )
            nudge_ylabel( axLARGE, -0.10, label='$n/cm^3$' )

            ytitler[axLARGEOptical] = 0.80  # special case for title positioning

            # in-place plot of LARGE-Optical green scattering and scattering/(scattering+absorption)
            axscatfrac = axLARGEOptical.twinx()
            axLARGEOptical.set_xlim( trange )
            axLARGEOptical.set_ylim( 1, 1500 ) # ???
            axLARGEOptical.set_yscale( 'log' )
            axLARGEOptical.set_ylabel( 'scattering ($1/Mm$)' )

            axscatfrac.set_xlim( trange )
            axscatfrac.set_ylim( 0.01, 1.18 )   # should be in range 0..1, but we have chart clutter near top of range
            nudge_ylabel( axscatfrac, 1.080, label='$SSA$' )

            loptimes, lops = self.largeopti.getslice( trange )

            if loptimes is not None:

                def masked_if_neg( vals ):
                    return numpy.ma.masked_array( vals, numpy.logical_not(numpy.isfinite(vals)) | (vals < 0) )

                lop_scattering = masked_if_neg( lops['Sc550_total'] ) # LARGE-Optical scattering (green light)
                lop_ssa = masked_if_neg( lops['SSA_dry_550nm'] )      # LARGE-single scattering albedo (scat / (scat+abs))
                axLARGEOptical.scatter( loptimes, lop_scattering, s=2, c='g', label='550$nm$ scattering' )
                axscatfrac.scatter( loptimes, lop_ssa, s=2, c='m', label='550$nm$ dry SSA' )
                axLARGEOptical.legend( loc='upper left' )
                axscatfrac.legend( loc='upper right' )


            self.hsrl.make_plot(2, axCloudtop, trange, tcenter)  # HSRL aircraft altitude
            self.hsrl.make_plot(1, axCloudtop, trange, tcenter)  # HSRL cloudtop altitude

        # AOD is always here, RSP or not.
        self.hsrl.make_plot(0, axAOD, trange, tcenter)       # HSRL aerosol optical depth
        self.mergedscalars.make_plot_aod( 0, axAOD, trange, tcenter ) # plot all three RSP-optical depth curves together with HSRL measure of the same


        for ax, plottitle in zip( axes, plottitles ):

            ### XXX Weird.  Title shows up beneath data (regardless of zorder) if we set_title() on original of a twinned axis.  We must put title on the twinx()'ed axis instead!
            if (not rsp_is_on) and (ax == axLARGEOptical):
                ax = axscatfrac

            # ytitler stuff: possibly override title Y position (for LARGE Optical)
            camp2exvis.axis_set_title_styled( ax, plottitle, **(dict(y=ytitler[ax]) if ax in ytitler else {}) )

            if ax not in axuntimed:
                timemark( ax, tcenter )
                xtimeticks(ax, trange, tcenter, showtime=(ax==axes[-1]))

        for ax in axuntimed:
            # shrink non-time-related axes vertically a bit to leave room for x-legend
            x,y,w,h = ax.get_position().bounds
            ax.set_position( (x, y+0.025, w, h-0.025) )

        return fig


class PageAPR3(Page):

    stemname = 'apr3'
    resolution = ( 576, 1050 )
    nrows, ncols = 4, 1     # give up on two-column layout, try positioning by brute force.  UGLY.  But it lets us force positions and sizes.

    def __init__(self, datetag):
        super().__init__(datetag)

    def connect_data(self):
        if self.connected:
            return

        self.connected = True

        from db_apr3 import APR3

        self.apr3 = APR3( camp2exvis.apr3data[ self.datetag ], self.datetag )

        self.dbzcmap = camp2exvis.cmap_for( 'apr3' )
        self.velcmap = camp2exvis.cmap_for( 'apr3' )        # XXX FIXME2 -- want discrete cmap

    def prepfig(self, trange, tcenter):

        self.connect_data()


        #fig = matplotlib.figure.Figure(tight_layout=True)
        #fig = matplotlib.figure.Figure()
        fig, (axWnadir, axKanadir, axKascandbz, axKascanvel)  = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False )

        # does it work if we do manual positioning at this point - do they stay positioned?

        #axWnadir = figpyplot.subplot2grid((self.nrows, self.ncols), (0,0), rowspan=13, colspan=2, fig=fig)
        #axKanadir = matplotlib.pyplot.subplot2grid((self.nrows, self.ncols), (14,0), rowspan=11, colspan=2, fig=fig)
        #axKascandbz =  matplotlib.pyplot.subplot2grid((self.nrows, self.ncols), (25,0), rowspan=18, colspan=1, fig=fig)
        #axKascanvel = matplotlib.pyplot.subplot2grid((self.nrows, self.ncols), (26,1), rowspan=18, colspan=1, fig=fig)

        self.set_fig_size( fig )

        # try moving the bottom two axes down a bit *before* we plot anything in them.
        #  (If we do this later, it moves the plot boxes but not the associated colorbars)

        for ax in axWnadir, axKanadir:
            timemark( ax, tcenter )
            xtimeticks( ax, trange, tcenter, showtime=(ax==axKanadir) )
            ax.set_xlim( trange )
            ax.set_ylim( (0,7) )


        imW, junkcb = self.apr3.make_plot_nadir( 0, axWnadir, trange, tcenter, do_cbar=False, do_substitute=True )
        self.apr3.make_plot_nadir( 1, axKanadir, trange, tcenter, do_cbar=False )
        axWncbar = fig.colorbar( imW, ax=axWnadir, orientation='horizontal', pad=0.06, shrink=0.75, fraction=0.08, aspect=60 )
        axWncbar.ax.set_xlabel( 'Reflectivity (Ka $or$ W) (dBz)' )

        camp2exvis.axis_set_title_styled( axWnadir, 'APR-3 W-band nadir' )
        camp2exvis.axis_set_title_styled( axKanadir, 'APR-3 Ka-band nadir' )

        impsz,   Kasdbzcbar = self.apr3.make_plot_scan( 0, axKascandbz, trange, tcenter )
        impsvel, Kasvelcbar = self.apr3.make_plot_scan( 1, axKascanvel, trange, tcenter )

        # right-hand scan plot has same vertical scale as its neighbors, so leave ticks but omit labels
        axKascanvel.set_yticks([])
        axKascanvel.set_ylabel('')

        if False:
            # XXX DEBUG
            namax = dict( (ax,nm) for (nm,ax) in zip(('W','Kan','Kad','Kav'), (axWnadir, axKanadir, axKascandbz, axKascanvel)) )
            names = []
            for ax in fig.axes:
                if ax not in namax:
                    namax[ax] = 'ax%d'%len(names)
                names.append( namax[ax] )
            print("APR3 fig has %d axes: %s" % (len(fig.axes), ", ".join(["%s y%.2f" % (namax[ax], ax.get_position().bounds[1]) for ax in fig.axes])))
        # end DEBUG



        # HACK.  Manually-position a plot, with associated horizontal colorbar.   Still ugly, but not as bad as picking fig.axes[N] was.
        def place_with_horiz_cbar( ax, xywh, cbar, dy, yfrac=0.75, xfrac=0.8 ):
            ## print("WAS pos %.2f %.2f  %.2f %.2f  subpos %.2f %.2f  %.2f %.2f" % (*ax.get_position().bounds, *subax.get_position().bounds))
            ax.set_position( xywh )

            subax = cbar.ax

            x,y,w,h = xywh
            subpos = [ x + w*(1-xfrac)*0.5, y - dy, w*xfrac, dy*yfrac ]
            subax.set_position( subpos )
            ## print("NOW pos %.2f %.2f  %.2f %.2f  subpos %.2f %.2f  %.2f %.2f" % (*xywh, *subpos))

        # manual placement
        place_with_horiz_cbar( axWnadir, [ 0.08, 0.71, 0.91, 0.23 ], axWncbar, dy=0.10, yfrac=0.75, xfrac=0.8 ) 

        axKanadir.set_position( [ 0.08, 0.38, 0.91, 0.23 ] )

        place_with_horiz_cbar( axKascandbz, [ 0.08, 0.10, 0.44, 0.23 ], Kasdbzcbar, dy=0.20, yfrac=0.75, xfrac=0.8 )
        place_with_horiz_cbar( axKascanvel, [ 0.55, 0.10, 0.44, 0.23 ], Kasvelcbar, dy=0.20, yfrac=0.75, xfrac=0.8 )

        # end manual placement.

        return fig


class PageMetNav(Page):

    stemname = 'metnav'
    resolution = ( 677, 390 )
    nrows, ncols = 2, 1


    def __init__(self, datetag):
        super().__init__(datetag)

    def connect_data(self):
        if self.connected:
            return
        self.connected = True

        from db_mergedscalars import MergedScalars

        self.metnav = MergedScalars( camp2exvis.mergedscalardata[ self.datetag ], self.datetag )

    def prepfig(self, trange, tcenter):

        self.connect_data()

        fig, (axPRoll, axAlt) = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False )

        axTDew = axAlt.twinx()

        self.set_fig_size( fig )

        self.metnav.make_plot_metnav( 0, axTDew, trange, tcenter, loc='upper right' )
        self.metnav.make_plot_metnav( 1, axTDew, trange, tcenter, loc='upper right' )
        self.metnav.make_plot_metnav( 2, axAlt, trange, tcenter, loc='upper left' )
        self.metnav.make_plot_metnav( 3, axPRoll, trange, tcenter )
        self.metnav.make_plot_metnav( 4, axPRoll, trange, tcenter )

        for ax in axTDew, axAlt, axPRoll:
            timemark(ax, tcenter)
            xtimeticks( ax, trange, tcenter, showtime=(ax==axAlt) )

        # nudge the title position set in make_plot_metnav()
        camp2exvis.axis_set_title_styled( axPRoll, "MetNav Pitch/Roll" )
        camp2exvis.axis_set_title_styled( axAlt, "MetNav\nTemp, Dewpoint, Altitude", y=0.85 )

        return fig


class PageTraceGas(Page):

    stemname = 'tracegas'
    resolution = (677, 544)
    nrows, ncols = 2, 1


    def __init__(self, datetag):
        super().__init__(datetag)

    def connect_data(self):
        if self.connected:
            return
        self.connected = True

        from db_mergedscalars import MergedScalars

        self.tracegas = MergedScalars( camp2exvis.mergedscalardata[ self.datetag ], self.datetag )

    tracetitle = [ "Relative Humidity + Ozone",  "AMS Aerosol Composition" ]

    def prepfig(self, trange, tcenter):

        self.connect_data()

        fig, (axisoK, axRH) = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False )

        axO3 = axRH.twinx()

        self.set_fig_size( fig )

        self.tracegas.make_plot_somegas( 0, axRH, trange, tcenter )    # Rel Hum
        self.tracegas.make_plot_somegas( 1, axO3, trange, tcenter )    # O3

        # Move RelHum and O3 legends down to make way for title
        axRH.legend( loc='upper left' )
        axO3.legend( loc='upper right' )

        # move y-axis labels in on both
        nudge_ylabel( axRH, -0.06, newy=0.425 )
        nudge_ylabel( axO3, 1.075 )

        self.tracegas.make_plot_tracegas( 0, axisoK, trange, tcenter )   # Org, SO4, NO3, NH4, Chl

        nudge_ylabel( axisoK, -0.07 )

        # for twinned axes, must set_title on the twinx ax, not the original.  So axO3 not axRH here.
        for ax, title in zip( [axO3, axisoK], self.tracetitle ):
            timemark(ax, tcenter)
            xtimeticks(ax, trange, timenow=tcenter, showtime=(ax == axO3))
            camp2exvis.axis_set_title_styled( ax, title )

        return fig

class PageAHIMap(Page):

    stemname = 'ahimap'

    resolution = (738, 1360)
    if os.getenv('AHIRES') is not None:
        resolution = [int(s) for s in os.getenv('AHIRES').split(',')]

    nrows, ncols = 2, 1

    def __init__(self, datetag):
        super().__init__(datetag)

    def connect_data(self):

        if self.connected:
            return
        self.connected = True

        from db_images import HIMAImages

        self.rfno = camp2exvis.RFno[ self.datetag ]

        self.hima = HIMAImages( camp2exvis.AHInaturaldata[ self.datetag ], self.datetag )
        self.himaaot = HIMAImages( camp2exvis.AHIAOTdata[ self.datetag ], self.datetag )
        self.himatb11 = HIMAImages( camp2exvis.AHIIRdata[ self.datetag ], self.datetag ) # Tb11 (11-micron IR) image
        self.aot_cmap = camp2exvis.cmap_for( 'AOT' ) # like copper

        self.tb11_cmap = camp2exvis.cmap_for( 'tb11' ) # compound colormap, colorful below -10C, shades of gray above -10C.

        from db_sattrack import SatTracks
        self.sattracks = SatTracks( camp2exvis.sattrackdata[ self.datetag ], self.datetag )

        from db_mergedscalars import MergedScalars

        self.metnav = MergedScalars( camp2exvis.mergedscalardata[ self.datetag ], self.datetag )

        from db_dropsonde import DropSonde

        self.dropsonde = DropSonde( camp2exvis.dropsondedata[ self.datetag ], self.datetag )


    def prepfig(self, trange, tcenter):

        self.connect_data()

        subplot_kw = dict( projection=cartopy.crs.PlateCarree() )

        fig, (axmap, axaot) = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False, \
                        # figsize = (9, 9),
                        subplot_kw = subplot_kw )

        self.set_fig_size( fig )

        #for ax in axmap, axaot:        # maybe not necessary, since we set 
        #    ax.set_aspect( 'auto' )

        # XXX plot span of nav positions plus map image

        legends = Legender()
        self.hima.make_plot( 0, axmap, trange, tcenter, legends=legends )

        special_bbox_opts = dict(boxstyle='round', facecolor='DarkSlateGray', alpha=0.92)

        pretitle = "Himawari 'RGB' $(1.6,0.86,0.67\mu{m})$\n%s  " % self.rfno

        titleopts=dict(pretitle=pretitle, x=0.025, y=0.88, fontsize=13, fontweight='bold', bbox=special_bbox_opts)
        self.metnav.make_plot_track( 0, axmap, trange, tcenter, legends=legends,
                titleopts=titleopts ) # also adds title, and (supposedly) tickmarks

        self.sattracks.make_plot( 0, axmap, trange, tcenter, legends=legends )

        legends.legend( axmap, loc='lower left' )

        drops = self.dropsonde.ticklist()
        for dlon, dlat, dtag in zip( drops['lon'], drops['lat'], drops['tag'] ):
            axmap.text( dlon, dlat, dtag )
            axaot.text( dlon, dlat, dtag )

        legends = Legender()

        imtb11 = self.himatb11.make_plot( 0, axaot, trange, tcenter, legends=legends, as_Tb=True, cmap=self.tb11_cmap, vrange=camp2exvis.tb11_vrange, zorder=0, do_substitute=True )

        imaot = self.himaaot.make_plot( 0, axaot, trange, tcenter, legends=legends, vscale=0.0002/2.5, vrange=(0,2.0), cmap=self.aot_cmap, invalidval=0, zorder=0.1, do_substitute=True )
            
        self.metnav.make_plot_track( 0, axaot, trange, tcenter, legends=legends, titleopts=dict(title='Himawari AOT + 11$\mu{m} T_b$', loc='left', x=0.02, y=0.98) )    # also adds title, and (supposedly) tickmarks


        cbartb11 = axaot.figure.colorbar( imtb11, ax=None, orientation='horizontal' )# , pad=0.03, fraction=0.13, aspect=20 )
        cbartb11.ax.set_xlabel( '11$\mu{m}$ Brightness Temperature' )

        cbaraot = axaot.figure.colorbar( imaot, ax=None, orientation='horizontal', aspect=10 )# , pad=0.03, fraction=0.13, aspect=20 )

        cbaraot.ax.set_xlabel( 'AHI AOT' )

        # print("ahimap axaot get_position(): ", axaot.get_position())
        # was: x0=0.2652608584862235, y0=0.10999999999999999, x1=0.7597391415137765, y1=0.46

        axmap.set_xticklabels([])   # omit longitude labels on upper axmap.
        
        # manual placement

        axmap.set_position( [0.10, 0.52,   0.83, 0.42] )
        axaot.set_position( [0.10, 0.09,   0.83, 0.42] )
        axmap.set_aspect('auto')
        axaot.set_aspect('auto')

        cbartb11.ax.set_position( [0.10, 0.015,  0.51, 0.05] )
        cbaraot.ax.set_position(  [0.65, 0.017,  0.28, 0.05] )

        legends.legend( axaot )

        return fig


class PageTimeBar(Page):

    stemname = 'timebar'
    resolution = ( 2725, 200 )
    nrows, ncols = 1, 1

    def __init__(self, datetag=None):
        super().__init__( datetag )

    def connect_data(self):
        if self.connected:
            return

        self.connected = True

        self.ribbon_plot_cmap = camp2exvis.cmap_for( 'hsrl-plot' )

        day00, tstart_h, tend_h = camp2exvis.timerange_from( self.datetag, 'h_0h' )
        self.full_trange = (tstart_h, tend_h)

        from db_hsrl import HSRL

        from db_dropsonde import DropSonde

        self.dropsonde = DropSonde( camp2exvis.dropsondedata[ self.datetag ], self.datetag )

        self.hsrl = HSRL(camp2exvis.hsrldata[self.datetag])         # XXX default input for 20190915
        print("HSRL time range %g .. %g on %s" % ( *self.hsrl.timerange, self.hsrl.Day0))

        self.hsrl.use_freq( 3600/1 ) # subsample to about 1 sample every second

        self.hsrl_532bsc = self.hsrl.ribbon_for( '/DataProducts/532_bsc' )

        # also non-ribbon scalars
        self.hsrl_altitude = self.hsrl.ribbon_for( '/Nav_Data/gps_alt' )

        self.sondedrops = self.dropsonde.ticklist()

        # APR 
        from db_apr3 import APR3

        self.apr3 = APR3( camp2exvis.apr3data[self.datetag], self.datetag )
        self.dbzcmap = camp2exvis.cmap_for( 'apr3' )

        rfnn = camp2exvis.RFno[self.datetag][2:4]
        yyyy = self.datetag[0:4]
        self.rftitle = "%s Research Flight %s" % (yyyy, rfnn)

        # SatTracks
        from db_sattrack import SatTracks
        self.sattracks = SatTracks( camp2exvis.sattrackdata[self.datetag], self.datetag )
        self.satticks = self.sattracks.ticklist()


    def prepfig(self, trange, tcenter):
        """TimeBar prepfig"""

        self.connect_data()

        # replace time range with full timespan
        trange = ( 0.5*numpy.floor( 2*self.full_trange[0] ), 0.5*numpy.ceil(2*self.full_trange[1] ) )

        fig, ax = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False,
                    gridspec_kw=dict(bottom=0.14, top=0.83) )

        self.set_fig_size( fig )

        axtop = ax.twiny()

        vrange = (-4, -1)

        marked_window = [ tcenter - camp2exvis.time_window_width*0.5, tcenter + camp2exvis.time_window_width*0.5 ]
        ribbon( onax=ax, timerange=trange, tnow=marked_window, do_log10=True, vrange=vrange, **self.hsrl_532bsc, title="", maintitle="", cmap=self.ribbon_plot_cmap, do_cbar=False, timemarkopts=dict(color='#FDC86080'), meshopts=dict(zorder=0.7, alpha=0.5) )

        apr_vthresh = 0.0  # don't plot APR radar stuff fainter than 0 dBz

        imp = self.apr3.make_plot_nadir( 1, ax, trange, tnow=marked_window, do_cbar=False, vthresh=apr_vthresh, timemarkopts=dict(color='#FDC86080'), meshopts=dict(alpha=1.0, zorder=0.5) ) # Ka-band(35GHz) nadir ribbon overlying HSRL.  Cut out faint stuff below -15 dBz.  Render APR3 beneath HSRL, so APR only visible where HSRL has gaps.

        # Mark the passage of time.  The future is gray.
        ax.add_patch( matplotlib.patches.Rectangle( (tcenter,0), trange[1]-tcenter, 8.0, fill=True, color='#80808080' ) )
        ax.plot( self.hsrl_altitude['times'], self.hsrl_altitude['values']*0.001, 'r', alpha=0.5 )

        xtimeticks(ax, trange, tcenter, showtime=True, wantticks=15)

        ax.set_xlim( trange )
        ax.set_aspect(aspect='auto', adjustable='datalim')

        axtop.set_xlim( trange )
        #ax.set_aspect(aspect='auto', adjustable='datalim')
 
        axtop.tick_params( axis='x', labelrotation=60 )
        axtop.set_xticks( self.sondedrops['time_h'] )
        axtop.set_xticklabels( [] ) # add tick labels within the plot box, below
        axtop.xaxis.set_tick_params( direction='in' )

        axsattop = ax.twiny()
        axsattop.tick_params( axis='x', labelrotation=30 )
        axsattop.xaxis.set_ticks_position('top')
        axsattop.set_xlim( trange )
        axsattop.set_xticks( self.satticks['time_h'] )
        #axsattop.set_xticklabels( self.satticks['tag'], verticalalignment='top', color='c', fontdict=dict(fontsize=9) )
        axsattop.set_xticklabels( [] ) # suppress normal labels, plant our own inside the plot box
        axsattop.xaxis.set_tick_params( direction='out' )

        # Put satellite labels above the box, each with its own color
        for sattime, sattag, satcolor in zip( self.satticks['time_h'], self.satticks['tag'], self.satticks['color'] ):
            axsattop.text( sattime, 7.3, sattag, rotation=30, horizontalalignment='center', color=satcolor )

        # Dropsonde labels just below top of box
        for sondetime, sondetag in zip( self.sondedrops['time_h'], self.sondedrops['tag'] ):
            axtop.text( sondetime, 5.7, sondetag, rotation=30, horizontalalignment='center' )

        # axtop.set_aspect(aspect=.125, adjustable='datalim')
        ax.set_aspect('auto', adjustable='box')
        axtop.set_aspect('auto', adjustable='box')  # Don't use adjustable='datalim', it'll change the upper axis' data range so as not to match the lower axis!

        camp2exvis.axis_set_title_styled( ax, "HSRL-2 532$nm$ backscatter + APR-3 Ka-band nadir" )

        camp2exvis.axis_set_title_styled( ax, self.rftitle, loc='left', position=(0.004,0.98), fontdict=dict(fontsize=12, fontweight='bold') )

        return fig

####

class PageFCDP(Page):

    stemname = 'fcdp'

    resolution = (737, 233)
    nrows, ncols = 1, 2

    def __init__(self, datetag):
        super().__init__(datetag)

    def connect_data(self):

        if self.connected:
            return
        self.connected = True

        from db_fcdp import FCDP
        from db_mergedscalars import MergedScalars

        self.fcdp = FCDP( camp2exvis.fcdpdata[ self.datetag ], self.datetag )
        self.twods10 = FCDP( camp2exvis.twods10data[ self.datetag ], self.datetag )
        self.mergedscalars = MergedScalars( camp2exvis.mergedscalardata[ self.datetag ], self.datetag )

        from db_fims import FIMS
        self.fims = FIMS( camp2exvis.fimsdata[ self.datetag ], self.datetag )

        self.YELLOW = (1,1,0,1)  # maximal YELLOW, brighter than 'y' yellow

    plottitlePSD = "FCDP/2DS10\npcle size dist"
    plottitleFIMSLAS = "FIMS/LAS\npcle size dist"

    def prepfig(self, trange, tcenter):

        self.connect_data()

        fig, (axLAS, axFCDP) = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False, gridspec_kw={'left':0.09, 'right':0.92, 'wspace':0.21, 'bottom':0.19} )
        self.set_fig_size( fig )

        self.fcdp.make_plot( 0, axFCDP, trange, tcenter, color='#44BBFFFF', vscale=0.001, label='FCDP' )  # scale from #/liter to #/cm^3
        self.twods10.make_plot( 0, axFCDP, trange, tcenter, color=self.YELLOW, vscale=0.001, label='2DS10' )  # scale from #/liter to #/cm^3

        legends = Legender()
        self.fims.make_plot( 0, axLAS, trange, tcenter, color=self.YELLOW, label='FIMS', legends=legends )
        self.mergedscalars.make_plot_LAS( 0, axLAS, trange, tcenter, color='#3090FFFF', label='LAS', legends=legends )
        legends.legend( axLAS, loc='upper right' )

        nudge_xlabel( axLAS, -0.17, label='$nm$' )
        nudge_ylabel( axLAS, -0.16 )

        # LAS: 100-3162 nm; FIMS: 10-600nm
        axLAS.set_xlim( 10, 3300 )
        xt = [10, 30, 100, 300, 1000, 3000]
        axLAS.set_xticks( xt )
        axLAS.set_xticklabels( ["%g"%v for v in xt] )

        axLAS.set_ylim( 0.1, 1.5e5 )  # Hack -- the title interferes with top part of graph, so raise upper limit (was 0.1..1e4)
        axLAS.set_yticks( [0.1, 1, 10, 100, 1000, 10000], minor=False )
        #axLAS.set_yticks( [0.3, 3, 30, 300, 3000, 30000], minor=True )

        ##axFCDP.tick_params(axis='y', right=True, left=False, labelright='on', labelleft='off') 

        camp2exvis.axis_set_title_styled( axFCDP, self.plottitlePSD, position=(0.28, 0.85) )
        camp2exvis.axis_set_title_styled( axLAS, self.plottitleFIMSLAS, position=(0.28, 0.85) )

        nudge_xlabel( axFCDP, -0.17, label='size $(\mu{m})$' )
        axFCDP.set_xlim( 0, 3200 )
        ## axFCDP.yaxis.tick_right()
        ## axFCDP.yaxis.set_label_position('right')

        for ax in axLAS, axFCDP:
            ax.set_aspect( 'auto' )

    
        nudge_ylabel( axFCDP, -0.13 )

        return fig


class PageRFLabel(Page):
    stemname = '_rflabel'
    resolution = (400, 120)
    nrows, ncols = 1, 1

    def __init__(self, datetag):
        super().__init__(datetag)

    def connect_data(self):

        if self.connected:
            return
        self.connected = True

        yyyy = self.datetag[0:4]
        rfnn = camp2exvis.RFno[ self.datetag ][2:4]
        self.rfno = '%s Research Flight %s' % (yyyy, rfnn)

    def prepfig(self, trange, tcenter):
        self.connect_data()

        fig, ax = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False, gridspec_kw=dict(top=0.8,bottom=0) )
        self.set_fig_size( fig )
        camp2exvis.axis_set_title_styled( ax, self.rfno, fontdict=dict(fontsize=18, fontweight='bold') )


        if True:
            ax.set_frame_on(False)
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_xlabel('')
            ax.set_ylabel('')

            ax.margins(0, tight=True)  # like 'tight'
            ax.set_aspect('auto')

            ax.set_facecolor( (0,0,0,0) )

        return fig
        

class PageDropsonde(Page):

    stemname = 'dropsonde'


    # relevant part of 1125x1125 source imagery:
    # resolution = (677, 677)
    resolution = (1125, 1125)
    nrows, ncols = 1, 1

    def __init__(self, datetag):
        super().__init__(datetag)

    def connect_data(self):

        if self.connected:
            return
        self.connected = True

        from db_dropsonde import DropSonde

        self.dropsonde = DropSonde( camp2exvis.dropsondedata[ self.datetag ], self.datetag )
        self.dropticks = self.dropsonde.ticklist()

        self.just_links = True

        self.blank_image = camp2exvis.dropsondeblankdata[ self.datetag ]


    def prepfig(self, trange, tcenter):

        self.connect_data()

        if self.just_links:
            imagename = self.dropsonde.file_at_time( tcenter )

            def makesondelink(outfname):
                src = self.blank_image if imagename is None else imagename
                destdir = os.path.dirname(outfname)
                reltarget = os.path.relpath( src, destdir )
                print("#dropsonde# ln -s '%s' '%s'" % (reltarget, outfname))
                return

            return makesondelink


        # otherwise do normal Figure processing

        
        fig, axsonde = matplotlib.pyplot.subplots( nrows=self.nrows, ncols=self.ncols, sharex=False, sharey=False, gridspec_kw=dict(top=1, bottom=0, left=0, right=1) )

        self.set_fig_size( fig )

        imrgb = self.dropsonde.image_at_time( tcenter )

        if imrgb is not None:

            ix = self.dropsonde.index_at_time( tcenter )
            shp = imrgb.shape[0:2] # likely 1125x1125
            axsonde.imshow( imrgb, extent=(0, shp[1], 0, shp[0]) )

            # Rose Miller's new images put a nice title directly on the image, so just use it.
            # Stag = self.dropticks['tag'][ix]
            # title = "AVAPS %s" % Stag
        else:
            #shp = (self.resolution[1], self.resolution[0])
            shp = (1125, 1125)
            axsonde.add_patch( matplotlib.patches.Rectangle( (0,0), shp[1], shp[0], fill=True, color='#000000FF' ) )
            # Tweaked to nearly match placement of label in Rose's image
            title = "AVAPS\n(no recent data)"
            camp2exvis.axis_set_title_styled( axsonde, title, y=0.77, fontdict=dict(fontsize=13) )


        axsonde.set_frame_on( False )
        axsonde.set_xlim(0, shp[1])
        axsonde.set_ylim(0, shp[0])

        axsonde.set_yticks([])
        axsonde.set_xticks([])

        axsonde.set_position( (0.00, 0.00, 1.00, 1.00) )
        axsonde.set_aspect( 'auto' )

        return fig      # always return fig to indicate success!

    

def make_simple_discrete_cmap( name, colorlist, N=256, margin=0.001, gamma=1.0 ):
    """Creates a matplotlib.colors.LinearSegmentedColormap() with discrete (constant) colors, given a list of tuples like
        RGBAcolor0, RGBAcolor1 ...  Each color is assigned 1/n_colors of the value range from 0..1."""
    llist = [ 0, vclist[0] ]
    for i in range(len(vclist)-1):
        llist.append( ( vclist[i+1][0]-margin, torgba( vclist[i][1:] ) ) )
        llist.append( ( vclist[i+1][0]+margin, torgba( vclist[i+1][1:] ) ) )
    if vclist[-1][0] < 1.0:
        llist.append( ( 1.0, torgba( vclist[-1][1] ) ) )

    return matplotlib.colors.LinearSegmentedColormap.from_list( name, llist, N=N, gamma=gamma )
