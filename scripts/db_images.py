#! /usr/bin/env python3

import sys, os
import datetime
import numpy
import re

from camp2exdb import DatedFiles

from osgeo import gdal, osr
import matplotlib.cm     as cm

import cartopy.crs
import cartopy.feature

class HIMAImages( DatedFiles ):

    """Inhale Himawari images for a day's run using DatedFiles class.
    Can ask for the current HIMA image filename at a specific time, like
    hima = HIMAImages('../himawari/downloads/Sept-15-2019', '20190915')
    imagename = hima.file_at_time( frameno / 3600 )   # or ...
    pixels = hima.image_at_time( frameno / 3600 )     # loads RGB/RGBA image pixel as numpy array.
    make_plot() would need an external mesh.   Or we could call gdalwarp to cut to specific geo box.
    """
        
    prefix = "hima8"
    suffix = ".tif"

    ###XXX### alternate coloring:  red=1.6um, green=0.86um, blue=0.64um,
    ###   as suggested by Larry from:  https://rammb.cira.colostate.edu/training/visit/quick_guides/QuickGuide_GOESR_daylandcloudRGB_final.pdf
    ###   Those are Himawari bands:      5(R)1.6um,  4(G)0.86um, 3(B)0.64um.
    ###   For comparison, default RGB is 3(R)0.64um, 2(G)0.51um, 1(B)0.47um

    #maxdwell = 10*60 / 3600     # 10 minutes between images
    maxdwell = 30*60 / 3600     # allow up to 30 minutes between images
    whatname = "HimawariRGB"

    ### Recipe for cutting a hima image to suit:
    ### First apply the right projection metadata...
    #   gdal_translate -a_srs '+proj=geos +a=6378169 +b=6356583.8 +lon_0=140.7 +h=35785831' -co COMPRESS=LZW -a_ullr -5500639.4747 5500658.2272 5497639.0713 -5497620.3188 hima_image.png   tmpfile.tif
    ### Then reproject to the resolution and coverage we want, e.g.
    #   gdalwarp -srcnodata -32767 -s_srs '+proj=geos +lon_0=140.7 +h=35785831 +x_0=0.0' -t_srs '+proj=latlong +datum=WGS84' -tr 0.03 0.03 -te 55.7 -80 225.7 80 -order 3 tmpfile.tif FINAL.tif

    def __init__(self, dirname, Day0yyyymmdd):
        super().__init__( dirname, Day0yyyymmdd )

        self.prev_fname = None

    datepat = re.compile('hima8(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)fd\.\w+$')

    JST_zone = 9.0  # Himawari file timestamps look like they're in JST time zone = UTC+9

    def whattime(self, fname):
        """parses filename, returns floating-point hours since visualization-time-0 on Day0"""
        # ../himawari/downloads/Sept-15-2019/hima820190916060000fd.png
        m = self.datepat.search(fname)
        if m is None:
            return None

        y,m,d, H,M,S = [int(v) for v in m.groups()]   # JST time.  Subtract JST_zone hours for UTC.

        # return floating-point hours since visualization-time0 on Day0.
        #
        return (datetime.datetime( y,m,d, H,M,S ) - self.Day0).total_seconds() / 3600.0 - self.JST_zone


    def make_plot( self, plotno, onax, trange, tcenter, legends=None, vscale=1, vrange=None, as_Tb=False, invalidval=None, zorder=0, cmap=None, do_substitute=False ):

        img = None

        fname = self.file_at_time( tcenter )

        if fname is None:
            print("HIMAImages(): no image at time %g" % tcenter)

        else:
            if fname == self.prev_fname:
                print("HIMAImages(): at time %g using cached %s" % (tcenter, fname))
            if fname != self.prev_fname and fname is not None:
                print("HIMAImages(): at time %g using new image %s" % (tcenter, fname))
                ds = gdal.Open(fname)
                data = ds.ReadAsArray()
                geotfm = ds.GetGeoTransform()
                proj = ds.GetProjection()

                inproj = osr.SpatialReference()
                inproj.ImportFromWkt(proj)

                projcs = inproj.GetAuthorityCode('PROJCS')
                if projcs is None:
                    projection = cartopy.crs.PlateCarree()
                else:
                    projection = cartopy.crs.epsg(projcs)

                if invalidval is not None:
                    data = numpy.ma.masked_array( data, data==invalidval )
                if data.dtype == numpy.uint16 and len(data.shape) == 3:
                    data = (data >> 8).astype(numpy.uint8)  # turn 16-bit RGB image into 8-bit RGB.  But leave 1-channel alone

                self.imdata = data
                self.geotfm = geotfm
                self.improj = projection
                self.prev_fname = fname

            #extent = (gt[0], gt[0] + ds.RasterXSize * gt[1],
            #          gt[3] + ds.RasterYSize * gt[5], gt[3])
            #extent = (114, 124,  4, 18)
            #img = ax.imshow((data[:, :].transpose((0, 1)).astype('f') - 32767)*.005, extent=extent,
            #                origin='upper', cmap='gray', vmin=-81,vmax=81, alpha=0.65)

            ysize, xsize = self.imdata.shape[-2:]

        
            extent = (self.geotfm[0],   self.geotfm[0] + xsize * self.geotfm[1],
                      self.geotfm[3] + ysize * self.geotfm[5], self.geotfm[3])

            if False:
                print("image xysize %d %d shape %s proj %s extent %s" % (xsize,ysize,self.imdata.shape,self.improj, extent))

            #extent = [ 114, 124, 4, 18 ]  # wrong but kinda pretty
            #extent = [ 80, 200, -60, 60 ]  # wrong but pretty

            if len(self.imdata.shape) == 3:
                img = onax.imshow( self.imdata[:3, :, :].transpose(1, 2, 0), extent=extent, aspect=0.75, origin='upper', transform=self.improj, zorder=zorder )

            elif as_Tb:
                # special case for brightness temperature images, after processing to uint16 with -scale 3,30000
                tbmin, tbmax = vrange   # must provide vrange with as_Tb
                tbC = (self.imdata[:, :].transpose((0, 1)).astype('f') - 30000)*0.003333
                img = onax.imshow(tbC, extent=extent,
                                origin='upper', cmap=cmap, vmin=tbmin,vmax=tbmax, zorder=zorder)
            else:
                # other grayscale image, e.g. AOT
                vmin, vmax = (None,None) if vrange is None else vrange
                img = onax.imshow(self.imdata[:, :].transpose((0, 1)).astype(numpy.float) * vscale, extent=extent, aspect=0.75,
                                origin='upper', cmap=cmap, vmin=vmin, vmax=vmax, zorder=zorder)

        # Whether there's a Himawari image or not, add the coastlines

        res = '10m'

        land_10m = cartopy.feature.NaturalEarthFeature('physical', 'land', res, edgecolor='face', facecolor=cartopy.feature.COLORS['land'], zorder=0)

        onax.add_feature(land_10m)
        onax.add_feature(cartopy.feature.COASTLINE.with_scale(res), linestyle='-')

        if img is None and do_substitute:
            img = cm.ScalarMappable(cmap=cmap)
            img.set_clim( vrange )

        return img

    

class CPIImages( DatedFiles ):

    prefix = 'CAMP2Ex_CPI_P3B'
    datepat = re.compile('%s_(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)' % prefix)

    maxdwell = 60.0 / 3600  # hang on to a CPI image for up to 2 minutes after it's taken

    whatname = "CPIimage"
        

    def __init__(self, dirname, Day0yyyymmdd):
        super().__init__( dirname, Day0yyyymmdd )


    def whattime(self, fname):
        """parses filename, returns floating-point hours since visualization-time-0 on Day0"""
        # CAMP2Ex_CPI_P3B_20190916_053329_433_RA.png
        m = self.datepat.search(fname)
        if m is None:
            return None

        y,m,d, H,M,S = [int(v) for v in m.groups()]

        # return floating-point hours since Day0 0h UTC
        return (datetime.datetime( y,m,d, H,M,S ) - self.Day0).total_seconds() / 3600.0


if __name__ == "__main__":
    # ( 106.0000000,  -8.5000000) ( 139.0000000,  38.0000000)

    import matplotlib.pyplot

    outfname = sys.argv[1]

    subplot_kw = dict( projection=cartopy.crs.PlateCarree() )

    fig, ax = matplotlib.pyplot.subplots(1, 1, subplot_kw=subplot_kw, gridspec_kw=dict(top=1, bottom=0, left=0, right=1))

    #extent = ( 112, 126,  4, 18 )  # lon0, lon1,   lat0, lat1
    extent = ( 106, 139,  -8.5, 38 )  # lon0, lon1,   lat0, lat1
    res_dpi = 3600/5 # one degree per fig inch, 4 arc seconds (about 120m) per pixel => 39600x55200
    if len(sys.argv)>2:
        res_dpi = float(sys.argv[2])

    fig.set_size_inches( (extent[1]-extent[0]), (extent[3]-extent[2]) )

    platecarree = cartopy.crs.PlateCarree()
    ax.set_extent( extent, crs=platecarree )

    res = '10m'

    ax.add_feature(cartopy.feature.COASTLINE.with_scale(res), linestyle='-', edgecolor='w', linewidth=0.375)

    ax.set_frame_on(False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlabel('')
    ax.set_ylabel('')

    ax.margins(0, tight=True)  # like 'tight'
    ax.set_aspect('auto')
    ax.set_adjustable('box')

    ax.set_facecolor( (0,0,0,1) )

    fig.savefig( outfname, dpi=res_dpi, facecolor=(0,0,0,1) )

    outstem = outfname.replace('.png','')

    print("gdal_translate -co COMPRESS=LZW -co PREDICTOR=2 -a_srs EPSG:4326 -a_ullr %g %g %g %g  %s %s.tif" % (extent[0],extent[3], extent[1],extent[2], outfname,  outstem))
    print("test -n \"$HFS\" || source houdini-setup.sh; iconvert -g off %s.tif %s.rat" % (outstem, outstem))
