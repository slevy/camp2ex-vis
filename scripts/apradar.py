#! /usr/bin/env python

from __future__ import print_function

import sys, os
import numpy
import h5py
import re

import datetime
import time

import camp2exvis   # time base, base point

sys.path.append('/fe3/demmapping/scripts')
import bgeo

R_earth = 6371.0

def ll2sphere(lons, lats, alts=0, R_sphere=R_earth):
    """given two N-element lists in degrees, and optional altitude for each, return Nx3 array of points on sphere of radius R_sphere"""
    flat = numpy.radians( numpy.ravel(lats) )
    flon = numpy.radians( numpy.ravel(lons) )
    coslat = numpy.cos( flat )
    sinlon = numpy.sin( flon )
    coslon = numpy.cos( flon )

    mesh = numpy.empty( shape=(flat.size, 3) )
    R_with = R_sphere + alts
    mesh[:,0] = R_with * coslon * coslat
    mesh[:,1] = R_with * sinlon * coslat
    mesh[:,2] = R_with * numpy.sin(flat)
    return mesh

class SpeckWriter(object):

    suffix = ".speck"

    def __init__(self, fname, pointattrnames=[], pointattrs=None, points=None, detailattrnames=[], detailattrs=None ):
        """ ... """

        self.fname = fname

        def asb(s):
            return s.encode() if hasattr(s,'encode') else s

        with open(fname, 'wb') as outf:
            outf.write(b'#! /usr/bin/env partiview\n\n')

            if isinstance(detailattrs, dict):
                for k in sorted( detailattrs.keys() ):
                    outf.write(b'##= %s = %s\n' % (asb(k), asb(str(detailattrs[k]))))

            else:
                for datt, dval in zip(detailattrnames, detailattrs):
                    attname = datt[0] if isinstance(datt, (list,tuple)) else datt
                    outf.write(b'##= %s = %s\n' % (asb(attname), asb('%s' % (dval,))))

            outf.write(b'\n')
                        
                
            if pointattrnames is None or len(pointattrnames)==0:
                for pt in points:
                    outf.write(b'%.8g %.8g %.8g\n' % tuple(pt))

            else:
                for i, attrname in enumerate(pointattrnames):
                    outf.write(b'datavar %d %s\n' % (i, asb(attrname)))
                outf.write(b'\n')

                attrfmt = b' %.8g' * len(pointattrnames)

                for pt, att in zip(points, pointattrs):
                    outf.write(b'%.8g %.8g %.8g ' % tuple(pt) + attrfmt % tuple(att) + b'\n')


class APRsss(object):
    """Opens an APR-3 radar data file, in lores scan mode in all three bands (KUsKAsWs)
       Rough resolution: 30m vertical/radial; 2-degrees lateral = 60m at 2km below aircraft; 240m along track"""

    # convert a UTC time to seconds-since-epoch.  Is this really the best way to do it??
    os.environ['TZ'] = 'UTC0'; time.tzset()
    # not anymore timebase = time.mktime(time.strptime('2019-01-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
    # 1546300800.0

    version = 11

    datepat = re.compile('CAMP2Ex-APR.-L2ZV_P3B_(\d\d\d\d\d\d\d\d)_')

    unix_epoch = datetime.datetime(1970,1,1, 0,0,0)

    def __init__(self, fname, timebase, basepoint=numpy.array([0,0,0])):
        """timebase is a datetime.datetime()"""
        self.fname = fname
        self.hf = h5py.File( fname, 'r' )
        self.hfk = self.hf['params_KUKA']
        self.hfl = self.hf['lores']
        self.basepoint = basepoint
        self.timebase_dt = timebase  # reference timepoint -- maybe start of vis time, or 0h on some convenient date
        self.timebase_0h_dt = datetime.datetime( timebase.year, timebase.month, timebase.day )  # at 0000 UTC on start-of-flight date
        # Two timebase numbers in Unix time system (which is what the APR3 files use internally):
        self.timebase_unixtime = (self.timebase_dt - self.unix_epoch).total_seconds()
        self.timebase_0h_unixtime = (self.timebase_0h_dt - self.unix_epoch).total_seconds()
        # timebase_unixtime is the Unix time of the specified timebase
        # timebase_0000_unixtime is the time of 0h UTC on the date of the specified timebase,
        # to make it easy to get human-readable times for the same moments.

        # Beginning timestamp datetime of this file's recording
        if 'date_beg' in self.hfk:
            YMDhms = self.hfk['date_beg'][:, 0].astype(numpy.int) # Y, M, D, h, m, s  in UTC
            self.date_beg = datetime.datetime( *YMDhms )
        else:
            # otherwise use first entry from /lores/scantime, which is in Unix time units (seconds since 1970.0)
            t0 = self.hfl['scantime'][0][0]
            self.date_beg = (self.unix_epoch + datetime.timedelta(seconds=t0) )

        self.basepoint = basepoint

        # Beginning date of this day's flight.   Use the caller-supplied timebase
        self.flight_yyyymmdd = int( self.timebase_0h_dt.strftime('%Y%m%d') ) # yyyymmdd

        ##self.flight_yyyymmdd_time0000 = time.mktime(time.strptime('%08d' % self.flight_yyyymmdd, '%Y%m%d')) # Unix time of 00:00UTC on date of flight

        # CAMP2Ex-APR3-L2ZV_P3B_20190915


        # "lla" = lon lat alt -- 2.5D version of location.
        # .midlla = lla at middle of this file's data
        self.nscans, self.nbeams, self.nranges = self.hfk['Nscan'][0], self.hfk['Nbeams'][0], self.hfk['Nbin_per_ray'][0]

        # midscan = self.nscans // 2
        # self.midlla = numpy.array( ( self.hfl['lon'][0,midscan], self.hfl['lat'][0,midscan], self.hfl['alt_nav'][0,midscan] ) )

    def datestr(self):
        "Returns yyyymmdd_hhmm (no seconds) of UTC start time of observation for this HDF5 file.  The yyyymmdd is always the date of the *start* of the daily run; hh can be >= 24 when the run extends past midnight"
        #return "%04d%02d%02d_%02d%02d%02d" % tuple(self.date_beg)
        dd, hh, mm = self.date_beg.day, self.date_beg.hour, self.date_beg.minute
        deltadays = (dd - self.timebase_0h_dt.day)
        hh += 24*deltadays
        return "%04d%02d%02d_%02d%02d" % (self.timebase_0h_dt.year, self.timebase_0h_dt.month, self.timebase_0h_dt.day, hh, mm)

    def writegeoms(self, outstem, flat_earth=False, writer=bgeo.BGeoPolyWriter):
        """Write <outstem>.radar.<suf>, <outstem>.track.<suf> files."""

        # radar observations in 3D
        lon3D = self.hfl['lon3D'][:]
        lat3D = self.hfl['lat3D'][:]
        alt3D = self.hfl['alt3D'][:]

        rawscantime = self.hfl['scantime'][:] #         nbeams, nscans  -- per-beam, not per-point, but we'll emit it per-point

        # airplane "p" track, as of midmost beam of each scan
        midbeam = self.hfk['Nbeams'][0,0] // 2

        plon = self.hfl['lon'][midbeam,:]
        plat = self.hfl['lat'][midbeam,:]
        palt = self.hfl['alt_nav'][midbeam,:]
        ppitch = self.hfl['pitch'][midbeam,:]
        proll = self.hfl['roll'][midbeam,:]
        pscantime = self.hfl['scantime'][midbeam,:] - self.timebase_unixtime
        psecs00 = self.hfl['scantime'][midbeam,:] - self.timebase_0h_unixtime # seconds since 0h UT on date of flight
        phh = numpy.floor( psecs00 / 3600 )
        pmmss = (psecs00 - 3600*phh)
        pmm = numpy.floor(pmmss/60)
        pss = numpy.remainder( pmmss, 60 )
        phhmmss = phh*10000 + pmm*100 + pss
        

        # self.llas = numpy.array( ( lon3D[:,12,:], lat3D[:,12,:], alt3D[:,12,:] ) )

        pointattrnames = ['time_since', 'refldB_14', 'refldB_35', 'depol_14', 'depol_35', 'vel_14', 'vel_35', 'at_surface'] 
        # Radar reflectivity in Ku band (13.4 GHz), Ka band (35.6 GHz)
        # 
        refldB_14  = self.hfl['zhh14'][:] # nranges, nbeams, nscans
        refldB_35  = self.hfl['zhh35'][:] # nranges, nbeams, nscans
        depol_14 = self.hfl['ldrhh14'][:] # nranges, nbeams, nscans
        depol_35 = self.hfl['ldrhh35'][:] # nranges, nbeams, nscans
        vel_14 = self.hfl['vel14'][:] # nranges, nbeams, nscans
        vel_35 = self.hfl['vel35'][:] # nranges, nbeams, nscans

        isurf = self.hfl['isurf'][:]

        # prepare for broadcast
        bscantime = (rawscantime - self.timebase_unixtime).reshape( [1] + list(rawscantime.shape) )
        # use broadcast_arrays() to make a view of the scantime that has the shape of the [nradial,nbeams,nscans] other arrays
        scantime, junk = numpy.broadcast_arrays( bscantime, refldB_14 )

        rangebucket = numpy.broadcast_to( numpy.arange(0, self.nranges).reshape(-1,1,1), refldB_14.shape )
        is_at_surface = (rangebucket == isurf[None,:])

        vr14 = numpy.isfinite(refldB_14)
        vr35 = numpy.isfinite(refldB_35)
        #dr14 = numpy.isfinite(depol_14)
        #dr35 = numpy.isfinite(depol_35)

        # Emit points where at least one of the reflectivity terms is valid.
        vvalid = (vr14 | vr35)

        def chosen( v ):
            return v[ vvalid ].ravel()

        def depolarization_remap( v ):
            vv = numpy.zeros( shape=v.shape, dtype=numpy.float32 )
            wanted = (numpy.nan_to_num(v, nan=-999) > -60)
            vv[ wanted ] = 10 ** (0.05 * numpy.clip( v[wanted], -1.0, -60.0 ))
            return vv

        cdepol_14 = numpy.clip( numpy.nan_to_num( depol_14[ vvalid ].ravel(), nan=-60.0 ), -1.0, -60.0 )
        cdepol_35 = numpy.clip( numpy.nan_to_num( depol_35[ vvalid ].ravel(), nan=-60.0 ), -1.0, -60.0 )
        # re-encode the delta-dB depolarization ratio as a fraction in range 0..1

        # nan_to_num(x, copy=True, nan=0.0, posinf=None, neginf=None)

        npoints = vvalid.sum()
        attrs = numpy.empty( (npoints, len(pointattrnames)), dtype='>f4' )
        attrs[:,0] = chosen( scantime )
        attrs[:,1] = numpy.nan_to_num( chosen( refldB_14 ), nan=0.0 )
        attrs[:,2] = numpy.nan_to_num( chosen( refldB_35 ), nan=0.0 )
        attrs[:,3] = depolarization_remap( chosen( depol_14 ) )
        attrs[:,4] = depolarization_remap( chosen( depol_35 ) )
        attrs[:,5] = numpy.nan_to_num( chosen( vel_14 ) )
        attrs[:,6] = numpy.nan_to_num( chosen( vel_35 ) )
        attrs[:,7] = chosen( is_at_surface )

        # Select the 3-D points for the above 
        if flat_earth:
            points = numpy.empty( (len(attrs), 3), dtype=numpy.float32 )
            points[:,0] = chosen( lon3D )
            points[:,1] = chosen( lat3D )
            points[:,2] = chosen( alt3D )

            trackpoints = numpy.empty( (len(plon), 3), dtype=numpy.float32 )
            trackpoints[:,0] = plon.ravel()
            trackpoints[:,1] = plat.ravel()
            trackpoints[:,2] = palt.ravel() * 0.001  # in km
        else:
            # altitudes are in meters, other units in km, so scale the altitudes
            points = ll2sphere( chosen( lon3D ), chosen( lat3D ), alts=.001*chosen( alt3D ), R_sphere=R_earth ) - self.basepoint.reshape(1,3)

            trackpoints = ll2sphere( plon.ravel(), plat.ravel(), alts=.001*palt.ravel(), R_sphere=R_earth ) - self.basepoint.reshape(1,3)


        detailattrnames = [ 'version', 'timebase_hi', 'timebase_lo', ('basepoint',3), 'flatearth', 'flight_20yymmdd', 'timebase_20yymmdd', 'timebase_hhmmss' ]

        timebase_yymmdd = 10000*(self.timebase_dt.year - 2000) + 100*self.timebase_dt.month + self.timebase_dt.day
        timebase_hhmmss = 10000*self.timebase_dt.hour + 100*self.timebase_dt.minute + self.timebase_dt.second

        detailattrs = [ self.version, *bgeo.floatpair( self.timebase_unixtime ), self.basepoint, flat_earth, self.flight_yyyymmdd - 20000000, timebase_yymmdd, timebase_hhmmss ]
 

        radarfname = "%s.radar%s" % (outstem, writer.suffix)
        writer( fname=radarfname,
                    points=points,
                    pointattrnames=pointattrnames,
                    pointattrs=attrs,
                    detailattrnames = detailattrnames,
                    detailattrs = detailattrs )

        trackattrnames = ['time_since', 'time_hhmmss', 'pitch', 'roll']
        trackattrs = numpy.empty( ( len(trackpoints), len(trackattrnames) ), dtype=numpy.float32 )
        trackattrs[:,0] = pscantime
        trackattrs[:,1] = phhmmss
        trackattrs[:,2] = ppitch
        trackattrs[:,3] = proll


        trackfname = "%s.track%s" % (outstem, writer.suffix)
        writer( fname=trackfname,
                    points = trackpoints,
                    pointattrnames = trackattrnames,
                    pointattrs = trackattrs, 
                    detailattrnames = detailattrnames,
                    detailattrs = detailattrs )




ii = 1
flat_earth = False
outdir = "."

def Usage():
    print("""Usage: %s [-flat] [-bgeo|-speck] [-visdate yyyymmdd | -ymddate yyyymmdd] [-o outdir] CAMP2Ex-APR3-..._KUsKAsWs.h5
Converts a CAMP2Ex APR3 (radar file) with KUsKAs (scan mode) to .bgeo form.
Must provide either
   -visdate yyyymmdd (set timebase to camp2exvis.py's start time listed for that date)
or -ymddate yyyymmdd (set timebase to 0h UTC on given date).
Writes a pair of files in given outdir, each with a collection of points:
    APR_KuKa_yyyymmdd_hhmmss.radar.bgeo_or_speck
    APR_KuKa_yyyymmdd_hhmmss.track.bgeo_or_speck
The .radar.bgeo_or_speck contains these fields for each point:
    time_since      time-of-observation as seconds since timebase (currently 2019-01-01 00:00:00 UTC)
    refldB_14       radar reflectivity at Ku-band (14GHz), in decibels.
                     40 dB is a lot, 55 dB is an awful lot.  65-90dB is probably solid earth.
    refldB_35       radar reflectivity at Ka-band (35GHz), in decibels.   Ditto.
    depol_14        depolarization ratio (range 0..1), Ku band
    depol_35        depolarization ratio (range 0..1), Ka band
    vel_14          radial velocity, measured in Ku-band (14GHz) (meters/sec) (roughly this is the vertical velocity)
    vel_35          radial velocity, measured in Ka-band (35GHz) (meters/sec) (roughly this is the vertical velocity)
    at_surface      1 if this point is approximately at ground level, 0 otherwise

and these as "detail" attributes, applying globally to the .bgeo file:
    timebase_hi    32-bit-float: high-order part of timebase time (seconds since UNIX epoch of 1970.0)
    timebase_lo    32-bit-float: rest of timebase time.   Add timebase_hi+timebase_lo+time_since for 64-bit UNIX time
    basepoint      3-component vector.   Add basepoint + 3-D points to get full 64-bit earth-centered position.
    flatearth      1 if all points are in (lon, lat, altitude_in_km) coordinates.  0 if 3-D Cartesian with Earth radius of 6371 (in km).
    flight_yyyymmdd  start date of flight as an 8-digit integer

The .track.bgeo_or_speck reports contains points along the airplane's track, plus these attributes for each point:
    time_since      time
    time_hhmmss     6-digit hours-minutes-seconds since 00:00 on starting date of flight
    pitch           orientation of airplane
    roll            orientation of airplane
and the same timebase, basepoint, flatearth, flight_yyyymmdd attributes as with .radar.bgeo.
    
    
    """ % sys.argv[0])
    sys.exit(1)

writer = bgeo.BGeoPolyWriter
wsuf = '.bgeo'

timebase = None

while ii < len(sys.argv) and sys.argv[ii][0] == '-':
    opt = sys.argv[ii]; ii += 1
    if opt == '-o':
        outdir = sys.argv[ii]; ii += 1
    elif opt.startswith('-flat'):
        flat_earth = True
    elif opt == '-speck':
        writer = SpeckWriter
        wsuf = '.speck'
    elif opt == '-bgeo':
        writer = bgeo.BGeoPolyWriter
        wsuf = '.bgeo'
    elif opt == '-visdate':
        yyyymmdd = sys.argv[ii]; ii += 1
        timebase = camp2exvis.timebase_from( yyyymmdd )
    elif opt == '-ymddate':
        yyyymmdd = sys.argv[ii]; ii += 1
        y, m, d = int(yyyymmdd[0:4]), int(yyyymmdd[4:6]), int(yyyymmdd[6:8])
        timebase = datetime.datetime( y, m, d, 0, 0 )
    else:
        print("Unknown option: %s" % opt)
        Usage()

if ii >= len(sys.argv):
    Usage()

if timebase is None:
    print("Must provide either -vistime yyyymmdd (to set base time from that date's camp2ex visualization as listed in camp2exvis.py) or -ymdtime yyyymmdd (to set base time from 0h UTC on that date)", file=sys.stderr)
    sys.exit(1)

basepoint = camp2exvis.basepoint
aprsss = APRsss( sys.argv[ii], timebase, basepoint )

datestr = aprsss.datestr()

outstem = os.path.join( outdir, "APR_KuKa_%s" % datestr )
if flat_earth:
    outstem += '.flat'

print("Writing to %s.{radar,track}%s" % (outstem, writer.suffix))

aprsss.writegeoms( outstem, flat_earth=flat_earth, writer=writer )
