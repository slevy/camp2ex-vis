#! /usr/bin/env python3

import sys, os

import h5py
import numpy

traps = 0

def table2h5( infile, outfile ):

    times = []
    vals = []

    with open(infile, 'r') as tabf:
        for line in tabf.readlines():
            if '#' in line:
                line = line.split('#')[0]
            ss = line.split()
            if len(ss) != 2:
                traps += 1
                continue
            time, val = float(ss[0]), float(ss[1])
            times.append( time )
            vals.append( val )

    dss = {}
    fieldnames = [ 'time', 'value' ]
    coldata = [ times, vals ]
    with h5py.File(outfile, 'w') as hff:
        for fieldname, data in zip( fieldnames, coldata ):
            dss[fieldname] = hff.create_dataset( fieldname, data=numpy.array( data ) )
            # set any attributes?
            #if numpy.any(data_now == -999999):
            #    dss[field].create('

if len(sys.argv) <= 2 or not sys.argv[2].endswith('.h5'):
    print("Usage: %s   ../CAMP2Ex/SPN/spn_transmittance_20190915.txt   ../CAMP2Ex/SPN/spn_transmittance_20190915.h5" % sys.argv[0])
    print("Read text file with (two) blank-separated columns of scalar quantities, write same data as HDF5 with field names 'time' and 'value'")
    sys.exit(1)

table2h5( sys.argv[1], sys.argv[2] )
print("Wrote to %s, caught %d traps" % (sys.argv[2], traps))
