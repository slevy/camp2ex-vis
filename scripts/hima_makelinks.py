#! /usr/bin/env python3

import sys, os
import numpy

import camp2exvis
from db_images import HIMAImages

import camp2exvis # for AHIopticaldata

# PageHIMAMap.AHIopticaldata = {'20190915': '../geo/AHI/optical/20190915/cut'}

ymd = "20190915"
stem = "rgb"
makelinks = False

ii = 1
while ii < len(sys.argv) and sys.argv[ii][0] == '-':
    opt = sys.argv[ii]; ii += 1
    if opt == '-ln':
        makelinks = True
    elif opt == '-stem':
        stem = sys.argv[ii]; ii += 1

if ii < len(sys.argv):
    ymd = sys.argv[ii]

hima = HIMAImages( camp2exvis.AHIopticaldata[ ymd ], ymd )

zero, timebase_h_0h, timeend_h_0h = camp2exvis.timerange_from( ymd, 'h_0h' )

def time2frame(when_h_0h):
    return int( numpy.round((when_h_0h - timebase_h_0h) * 3600) )

def frame2time(frame):
    return (frame/3600.0) + timebase_h_0h


imagelist = hima.files_in_timerange( (timebase_h_0h, timeend_h_0h) )
maxframe = time2frame(timeend_h_0h)
endframe = min( maxframe, time2frame( hima.whattime( imagelist[-1] ) + 11./60. ) )


ext = os.path.splitext(imagelist[0])[1]
linksdir = os.path.join( os.path.dirname( imagelist[0] ), "links" )

if makelinks:
    if not os.path.isdir(linksdir):
        os.makedirs(linksdir)
    for frameno in range(0, endframe+1):
        fname = hima.file_at_time( frame2time(frameno) )
        if fname is None:
            print("No image for frame %05d" % frameno)
        else:
            src = os.path.relpath( fname, linksdir )
            dst = os.path.join( linksdir, "%s.%05d%s" % (stem, frameno, ext) )
            print("ln -s %s %s" % (src,dst))
else:
    print("# Would make links %s.00000-%05d%s in %s" % (stem, endframe, ext, linksdir))

    for fname in imagelist:
        when_h_0h = hima.whattime( fname )
        frameno = time2frame( when_h_0h )
        print("%05d\t%s" % (frameno, fname))
