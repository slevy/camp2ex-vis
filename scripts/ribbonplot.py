#! /usr/bin/env python3

import sys, os
import h5py
import numpy
import glob
import re

import datetime

import camp2exvis

import PIL.Image

from intervaltree.intervaltree import IntervalTree
from intervaltree.interval     import Interval

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as mp
import matplotlib.colors as mc # for LinearSegmentedColormap
import matplotlib.cm     as cm
import matplotlib.patches
import matplotlib.lines        # for Line2D

def ribbon(onax, timerange, tnow, vrange, values, alts, times, cmap, do_log10=False, title=None, maintitle=None):
    j0, j1 = numpy.searchsorted( times, timerange, side='left' )

    t0 = times[min(j0, len(times)-1)]
    t1 = times[min(j1, len(times)-1)]

    if j1 <= j0:
        # print("timerange %g %g => ixrange %d %d => trange %g %g  ... skipping." % (*timerange, j0,j1, t0,t1))
        return

    vals = values[j0:j1, :]
    if do_log10:
        # rigamarole to prevent log10 warnings, since it doesn't respect the mask or fill_value of a masked array
        vvalid = numpy.nan_to_num(vals) > 0
        valsnow = numpy.ma.masked_array( numpy.zeros_like( vals ), numpy.logical_not(vvalid) )
        valsnow[vvalid] = numpy.log10( vals[vvalid] )
    else:
        valsnow = numpy.ma.masked_array( vals, numpy.isnan(vals) )
    numpy.clip( valsnow, vrange[0], vrange[1], out=valsnow )
    # print("timerange %g %g => ixrange %d %d => trange %g %g  vrange %g %g valuesrange %g %g" % (*timerange, j0,j1, t0,t1, *vrange, numpy.nanmin(valsnow), numpy.nanmax(valsnow)))
    
    # search for breaks in the time series -- split the image if times aren't uniformly spaced?   Well, not now.
    
    #extent = (timerange[0], timerange[1], alts[0], alts[-1])
    onax.set_xlim( timerange )
    extent = (t0, t1, alts[0], alts[-1])
    #implain = onax.imshow( values[j0:j1, :].transpose(), vmin=vrange[0], vmax=vrange[1], extent=extent, origin='lower', cmap=cmap )
    ## norma = matplotlib.colors.LogNorm( vmin=vrange[0], vmax=vrange[1] )
    timesnow = times[min(j0, len(times)-1) : min(j1, len(times)-2)+1]
    implain = onax.pcolormesh( timesnow, alts, valsnow.transpose(), vmin=vrange[0], vmax=vrange[1], cmap=cmap )

    if tnow is not None:
        # add a gray marker showing the current time - a tall narrow unfilled rectangle
        rectwidth = 0.01 * (t1-t0)
        # onax.add_patch( matplotlib.patches.Rectangle( (tnow-0.5*rectwidth,alts[0]), rectwidth, alts[-1]-alts[0], fill=False, edgecolor='#B0C0D050', linestyle='--', linewidth=2.0 ) )
        onax.add_line( matplotlib.lines.Line2D( (tnow-0.5*rectwidth,tnow-0.5*rectwidth), (alts[0],alts[-1]), color='#B0C0D050', linestyle=':', linewidth=2.0 ) )
        onax.add_line( matplotlib.lines.Line2D( (tnow+0.5*rectwidth,tnow+0.5*rectwidth), (alts[0],alts[-1]), color='#B0C0D050', linestyle=':', linewidth=2.0 ) )

    #was:#implain = onax.imshow( valsnow.transpose(), vmin=vrange[0], vmax=vrange[1], extent=extent, origin='lower', cmap=cmap )
    onax.set_aspect(0.62*(timerange[1]-timerange[0])/(extent[3]-extent[2]), adjustable='box')

    cbar = onax.figure.colorbar( implain, ax=onax, orientation='vertical' )
    if title is not None:
        cbar.ax.set_ylabel( title )

    onax.set_xlim( timerange )
    onax.set_ylim( [alts[0], alts[-1]] )

    if title is not None or maintitle is not None:
        onax.set_title( maintitle if maintitle is not None else title )

def curve(onax, timerange, vrange, values, alts, times, cmap):
    pass

def jday2date(jday):    # make a datetime object of 0h UTC on the given Jday day.   E.g. 737683 => datetime(2019, 9, 15).
    # Jday zero point is something like Jan 0 of 0 AD.  366+1 days before Jan 1 of 1 AD.
    return datetime.date( 1, 1, 1 ) + datetime.timedelta(days=(jday - 366 - 1))

def name_of_class( cls ):
    return str(cls).split("'")[1].replace('__main__.','')

#####

class DatedFiles(object):

    prefix = 'FILENAME_PREFIX'  # within each dir, for glob(prefix+'*'+suffix) file search
    suffix = '.png'             # file suffix within each dir

    datepat = re.compile('%s_(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)' % prefix) # y,m,d,H,M,S

    maxdwell = 15.0 / 3600.0    # time-limit at end of sequence.    Overridden in data-specific subclasses.

    whatname = "WHAT"  # For messages.  subclass should override, e.g. "CPImage"

    

    def ymd2date( self, ymd ):
        """Given a UTC date in yyyymmdd form (as 8-char string, or 8-digit integer, or datetime object), return a datetime object for 0h UTC on that date"""

        if isinstance(ymd, (int, float)) and ymd >= 20000000:
            ymd = "%08d" % ymd

        if isinstance(ymd, str) and len(ymd) >= 8:
            ymd = ymd.replace('-','')  # 2019-09-15 => 20190915
            return datetime.datetime( int(ymd[0:4]), int(ymd[4:6]), int(ymd[6:8]), 0, 0, 0 )

        if instance(ymd, datetime.datetime):
            return ymd
        raise ValueError("ymd2date(): expected yyyymmdd as string or integer or datetime; what's %s ?" % repr(ymd))

    def __init__(self, dirname, Day0yyyymmdd):
        self.dirname = dirname

        self.Day0 = self.ymd2date( Day0yyyymmdd )

        self.inti = IntervalTree()

        self.buildtree( dirname )

        # one-entry static image cache  [or should this be specific to subclass?]
        self.last_imname = None
        self.last_impixels = None

    def whattime(self, fname):
        raise ValueError("DatedFiles subclass %s must provide whattime() to parse time from filename %s" % ( self.__class__.__name__, fname ) )

    def buildtree(self, dirname):
        globpattern = os.path.join( dirname, self.prefix + '*' + self.suffix )
        names = sorted( glob.glob( globpattern ) )
        if len(names) == 0:
            print("%s.buildtree(): can't find any files named %s" % ( name_of_class(type(self)), globpattern ))
            return

        times = [ self.whattime(fname) for fname in names ]
        prevname, prevtime = names[0], times[0]
        for name, time in zip(names[1:], times[1:]):
            endtime = min( time, prevtime + self.maxdwell )
            if endtime > prevtime:
                self.inti.addi( prevtime, endtime, prevname )
            prevname, prevtime = name, time
        # add final image for one dwell time
        self.inti.addi( prevtime, prevtime + self.maxdwell, prevname )

    def file_at_time( self, time ):
        s = self.inti.at( time )
        if len(s) > 0:
            iminterval = s.pop()
            return iminterval.data
        else:
            return None

    def files_in_timerange( self, timerange ):
        ss = self.inti.overlap(timerange[0], timerange[1])
        return [s.data for s in sorted(list(ss))]  # ordered by time

    def image_at_time( self, time ):
        """Returns <height> x <width> x 3 numpy array of bytes, or None."""
        imname = self.file_at_time(time)
        if imname is None:
            print("# No %s at time %g = %02d%02d%02d" % (self.whatname, time, int(time), int((time-int(time))*60), int(time*3600)%60))
            return None

        if self.last_imname == imname:
            return self.last_impixels

        self.last_imname = imname
        self.last_impixels = None
        im = PIL.Image.open(imname)

        if im.mode != 'RGB':
            im = im.convert(mode='RGB')
        w, h = im.width, im.height
        rgb = numpy.frombuffer( im.tobytes(), dtype=numpy.uint8 ).reshape( h, w, 3 )

        self.last_impixels = rgb
        print("# Found %dx%d %s %s at time %g = %02d%02d%02d" % (w,h, self.whatname, imname, time, int(time), int((time-int(time))*60), int(time*3600)%60))
        return rgb
    

class HIMAImages( DatedFiles ):

    """Inhale Himawari images for a day's run using DatedFiles class.
    Can ask for the current HIMA image filename at a specific time, like
    hima = HIMAImages('../himawari/downloads/Sept-15-2019', '20190915')
    imagename = hima.file_at_time( frameno / 3600 )   # or ...
    pixels = hima.image_at_time( frameno / 3600 )     # loads RGB/RGBA image pixel as numpy array.
    make_plot() would need an external mesh.   Or we could call gdalwarp to cut to specific geo box.
    """
        
    prefix = "hima8"
    suffix = ".png"

    maxdwell = 10*60 / 3600     # 10 minutes between images
    whatname = "HimawariRGB"

    ### Recipe for cutting a hima image to suit:
    ### First apply the right projection metadata...
    #   gdal_translate -a_srs '+proj=geos +a=6378169 +b=6356583.8 +lon_0=140.7 +h=35785831' -co COMPRESS=LZW -a_ullr -5500639.4747 5500658.2272 5497639.0713 -5497620.3188 hima_image.png   tmpfile.tif
    ### Then reproject to the resolution and coverage we want, e.g.
    #   gdalwarp -srcnodata -32767 -s_srs '+proj=geos +lon_0=140.7 +h=35785831 +x_0=0.0' -t_srs '+proj=latlong +datum=WGS84' -tr 0.03 0.03 -te 55.7 -80 225.7 80 -order 3 tmpfile.tif FINAL.tif

    def __init__(self, dirname, Day0yyyymmdd):
        super(HIMAImages, self).__init__( dirname, Day0yyyymmdd )

    datepat = re.compile('hima8(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)fd\.png')

    JST_zone = 9.0  # Himawari file timestamps look like they're in JST time zone = UTC+9

    def whattime(self, fname):
        """parses filename, returns floating-point hours since visualization-time-0 on Day0"""
        # ../himawari/downloads/Sept-15-2019/hima820190916060000fd.png
        m = self.datepat.search(fname)
        if m is None:
            return None

        y,m,d, H,M,S = [int(v) for v in m.groups()]   # JST time.  Subtract JST_zone hours for UTC.

        # return floating-point hours since visualization-time0 on Day0.
        #
        return (datetime.datetime( y,m,d, H,M,S ) - self.Day0).total_seconds() / 3600.0 - self.JST_zone



class CPIImages( DatedFiles ):

    prefix = 'CAMP2Ex_CPI_P3B'
    datepat = re.compile('%s_(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)' % prefix)

    maxdwell = 60.0 / 3600  # hang on to a CPI image for up to 2 minutes after it's taken

    whatname = "CPIimage"
        

    def __init__(self, dirname, Day0yyyymmdd):
        super(CPIImages, self).__init__( dirname, Day0yyyymmdd )


    def whattime(self, fname):
        """parses filename, returns floating-point hours since visualization-time-0 on Day0"""
        # CAMP2Ex_CPI_P3B_20190916_053329_433_RA.png
        m = self.datepat.search(fname)
        if m is None:
            return None

        y,m,d, H,M,S = [int(v) for v in m.groups()]

        # return floating-point hours since Day0 0h UTC
        return (datetime.datetime( y,m,d, H,M,S ) - self.Day0).total_seconds() / 3600.0



class RSP( DatedFiles ):

    prefix = 'CAMP2EX-RSP1-CLD_P3B'
    suffix = '.h5'
    datepat = re.compile('%s_(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)' % prefix)

    maxdwell = 20*60 / 3600.0  # when assembling interval tree, assume that last file might have data for up to 20 minutes.

    whatname = "RSP"

    def __init__(self, dirname, Day0yyyymmdd):
        super(RSP, self).__init__( dirname, Day0yyyymmdd )

        # RSP's "/Data/Product_Time_Seconds" is in seconds since 0h UT on start of this flight's day.
        # Store the offset (seconds) between our adopted visualization 0-time for this day (self.Day0) after 0h UT on this day.
        self.time0_secs_within_day = (self.Day0 - datetime.datetime(self.Day0.year, self.Day0.month, self.Day0.day, 0, 0, 0)).total_seconds()
        self.timerange = None
        self.filetimeset = set()

        self.times_h = {}
        self.CloudBiSpec_Reff = {}
        self.CloudBow_Reff = {}
        self.CloudBow_OT = {}
        self.CloudTop = {}


    def whattime(self, fname):
        """parses filename, returns floating-point hours since visualization-time-0 on Day0.
        Note: these time numbers must be in the same time system as the self.times_h[] values read from inside the datafile in load_data()."""
        # CAMP2EX-RSP1-CLD_P3B_20190915223026_R2.h5
        m = self.datepat.search(fname)
        if m is None:
            return None

        y,m,d, H,M,S = [int(v) for v in m.groups()]

        # return floating-point hours since adopted visualization 0-time for start of this file's data
        # For these files the "hour" field might exceed 23, so add the H field separately -- datetime() doesn't want e.g. H=28.
        return (datetime.datetime( y,m,d, 0,M,S ) - self.Day0).total_seconds() / 3600.0 + H


    def gather(self, ddict):
        return numpy.concatenate( [ ddict[ftime] for ftime in sorted( ddict.keys() ) ] )

    def load_timerange(self, timerange):
        if self.timerange is None or (timerange[0] > self.timerange[1] or timerange[1] < self.timerange[0]):
            self.timerange = tuple(timerange)

            # find all the data files within the given timerange, in increasing time order
            for fname in self.files_in_timerange( timerange ):
                self.load_data( fname )
            

    def load_data( self, fname ):
        ftime = self.whattime(fname)
        if ftime in self.filetimeset:
            return

        self.filetimeset.add( ftime )
        hf = h5py.File( fname, 'r' )
        hfd = hf['Data']

        self.times_h[ftime] = tchunk = (hfd['Product_Time_Seconds'][:] - self.time0_secs_within_day) / 3600.0  # times in hours since today's visualization-time-0
        # update self.timerange to include this interval.  NOTE we need the times_h[] values to be in the same time system as the whattime() values parsed from filenames.  Beware!
        if self.timerange is None:
            self.timerange = (tchunk[0], tchunk[-1])
        else:
            self.timerange = ( min(self.timerange[0], tchunk[0]), max(self.timerange[1], tchunk[-1]) )

        self.CloudBiSpec_Reff[ftime] = hfd['Cloud_Bi_Spec_Particle_Effective_Radius'][:,1]  ## of the Bi_Spec bands, use [1] == 2260nm (second index)
        self.CloudBow_Reff[ftime] = hfd['Cloud_Bow_Droplet_Effective_Radius'][:]
        self.CloudBow_OT[ftime] = hfd['Cloud_Bow_Optical_Thickness'][:]
        self.CloudTop[ftime] = hfd['Cloud_Top_Altitude'][1,:]      ## of the two Bi_Spec bands, use [1] == 2260 nm (first index)

    plotlabel = [ 'Reff_2260', 'CBow_Reff', 'CBow_OT', 'CloudTop_2260' ]
    plotcolor = [ 'g',         'r',         'b',       'y'             ]
    plotvrange = [ (0,35),     (0,35),      (0,150),   (0,8000),       ]

    def make_plot( self, plotno, onaxis, timerange, tnow, cmap='plasma'):
        
        self.load_timerange( timerange )

        if self.times_h == {}:
            return


        if False:
            # data check
            for i, fldname in enumerate([ "CloudBiSpec_Reff", "CloudBow_Reff", "CloudBow_OT", "CloudTop" ]) :
                dd = getattr(self, fldname)
                print("RSP[%s] %s shape %s" % (fldname, self.plotlabel[i], list(dd.values())[0].shape))

        ddata = [ self.CloudBiSpec_Reff, self.CloudBow_Reff, self.CloudBow_OT, self.CloudTop ][ plotno ]
        values = self.gather(ddata)
        times_h = self.gather(self.times_h)

        # these are all scalars-as-functions-of-time.   
        vrange = self.plotvrange[plotno]
        extent = (timerange[0], vrange[0], timerange[1], vrange[1])
        bbox = matplotlib.transforms.Bbox.from_extents( *extent )
        onaxis.plot( times_h, values, color=self.plotcolor[plotno], label=self.plotlabel[plotno], clip_box=bbox, clip_on=True )

        print("RSP.make_plot(%d...) time[%d] %g .. %g, data.shape %s" % (plotno, len(times_h), times_h[0], times_h[-1], data.shape))

#####

class AMPR(object):

    nplots = 4

    plotlabels = [ "T_b 37 (K)",  "T_b 85 (K)",   "T_b 10 (K)",  "LWC (kg/m^2)" ]   # AMPR microwave temperatures (K) at 37, 85, 10 GHz, and liquid water content
    vranges =   [   (160,180),     (230,300),      (100,200),      (0, 1.5)     ]

    def __init__(self, fname):
        amprtb = pyampr.AmprTb( fname, 'CAMP2EX' )
        amprhf = h5py.File( fname, 'r' )

        # copy the needed values out - don't leave the AMPR file open after __init__()
        self.swath_angle = amprtb.swath_angle

        self.tb37 = 0.5 * (amprtb.TB37H + amprtb.TB37V)
        self.tb85 = 0.5 * (amprtb.TB85H + amprtb.TB85V)
        self.tb10 = 0.5 * (amprtb.TB10H + amprtb.TB10V)

        self.LWC = amprhf['atmosphere_cloud_liquid_water_content'][:]   # "LWC" = cloud liquid water content in mm, which is about equal to kg/m^2.

        ymdbase = "%04d%02d%02d" % (amprtb.Year, amprtb.Month, amprtb.Day)
        self.timebase = camp2exvis.timebase_from( ymdbase )

        # times[] is an array of floating-point hours since this day's timebase.
        time_since_epoch = float( self.timebase.strftime('%s') ) # %s => seconds since UNIX epoch of 1970.0
        self.times = (amprtb.netcdfTimes - time_since_epoch) / 3600.0

# 'TB10A', 'TB10B', 'TB10H', 'TB10V', 'TB19A', 'TB19B', 'TB19H', 'TB19V', 'TB37A', 'TB37B', 'TB37H', 'TB37V', 'TB85A', 'TB85B', 'TB85H', 'TB85V', 

    def make_plot(self, plotno, onaxis, timerange, tnow, cmap='plasma'):

        values = [ self.tb37, self.tb85, self.tb10, self.LWC ][plotno]

        ribbon( onaxis, timerange, tnow, vrange=self.vranges[plotno], values=values, alts=self.swath_angle, cmap=cmap, do_log10=False, title='AMPR %s' % self.plotlabels[plotno] )
        

class HSRL(object):
    def __init__(self, fname):
        self.fname = fname
        self.hf = h5py.File(fname, 'r')
        times = self.hf['Nav_Data/gps_time'][:].ravel()  # in hours from 0 UTC on current date
        self.jdays = self.hf['Nav_Data/Jday'][:].ravel() # julian-like date, with base of Jan 1 0AD or so (366 days before Jan 1, 1 AD)
        self.times = times + 24 * (self.jdays - self.jdays[0])   # Time in our adopted units: hours from 0 UTC on date when flight started.
        self.timerange = (self.times[0], self.times[-1])
        self.Day0 = jday2date( self.jdays[0] )

        self.ntimes = len(self.times)
        self.per_hour = (self.times[-1] - self.times[0]) / (self.ntimes - 1)
        self.subsetix = None

        self.alts = self.hf['/DataProducts/Altitude'][:].ravel()

    def use_freq(self, target_per_hour):
        if target_per_hour > 0.75*self.per_hour:
            self.subsetix = None
        else:
            nsamples = numpy.round( (self.times[-1] - self.times[0]) * target_per_hour ).astype(numpy.int)
            self.subsetix = numpy.linspace(0, len(self.times)-1, num=nsamples).astype(numpy.int)

    knownrange = {'/DataProducts/Dust_Mixing_Ratio': (0,7),
                  '/DataProducts/532_bsc': (0,0.5),
                  '/DataProducts/532_AOT_above_cloud': (0,4),
                  '/DataProducts/1064_bsc': (0,10),
                  '/DataProducts/cloud_top_height': (0,7300),
                  '/DataProducts/Aerosol_ID': (0,7),
                  '/Nav_Data/gps_alt': (0,8000),
                 }
                  
    def vrange_for(self, fieldname):
        if fieldname in self.knownrange:
            return self.knownrange[ fieldname ]
        if '/DataProducts/' + fieldname in self.knownrange:
            return self.knownrange[ '/DataProducts/' + fieldname ]
        return None
        
    def ribbon_for(self, fieldname):
        
        vrange = self.vrange_for( fieldname )
        values = self.hf[fieldname][:] if fieldname in self.hf else self.hf['/DataProducts/' + fieldname][:]
        times = self.times
        if self.subsetix is not None:
            times = times[self.subsetix]           
            if values.shape[0] == self.ntimes:
                values = values[self.subsetix]

        if vrange is None:
            vrange = (numpy.nanmin(values), numpy.nanmax(values))
        
        ##results = { 'vrange': vrange, 'values': values, 'times': times, 'alts': self.alts }
        results = { 'values': values, 'times': times, 'alts': self.alts }
        return results

    def ribbon_ratio( self, field1, field2 ):
        r2 = self.ribbon_for( field2 )

        r1values = self.hf[field1][:] if field1 in self.hf else self.hf['/DataProducts/' + field1][:]

        rrat = r2.copy()
        rrat['values'] = r1values / r2['values']
        return rrat

# Inhale standard(?) Panoply colormap

#outfmt = "../images/hsrlribbons3-4/rib.%05d.png"; backcolor = (0.20, 0.20, 0.20, 1.0)
#outfmt = "../images/hsrlribbons3-4/ribblack.%05d.png"; backcolor = (0.0, 0.0, 0.0, 1.0)
#outfmt = "../images/hsrlribbons3-4/ribbrain.%05d.png"; backcolor = (0.0, 0.0, 0.0, 1.0)
outfmt = "../images/hsrlribbons3-22/ribrain.%05d.png"; backcolor = (0.1, 0.1, 0.1, 1.0)

def make_simple_discrete_cmap( name, colorlist, N=256, margin=0.001, gamma=1.0 ):
    """Creates a matplotlib.colors.LinearSegmentedColormap() with discrete (constant) colors, given a list of tuples like
        RGBAcolor0, RGBAcolor1 ...  Each color is assigned 1/n_colors of the value range from 0..1."""
    llist = [ 0, vclist[0] ]
    for i in range(len(vclist)-1):
        llist.append( ( vclist[i+1][0]-margin, torgba( vclist[i][1:] ) ) )
        llist.append( ( vclist[i+1][0]+margin, torgba( vclist[i+1][1:] ) ) )
    if vclist[-1][0] < 1.0:
        llist.append( ( 1.0, torgba( vclist[-1][1] ) ) )

    return matplotlib.colors.LinearSegmentedColormap.from_list( name, llist, N=N, gamma=gamma )



# force dark background theme, with light text
mp.style.use('dark_background')

ribbon_plot_cmap = camp2exvis.cmap_for( 'hsrl-plot' )
#ribbon_plot_cmap = cm.viridis; ribbon_plot_cmap.set_bad( mc.to_rgba( backcolor[0:3] ) ) # viridis with "black" as bad (?) color

hsrl = HSRL('../CAMP2Ex/HSRL/CAMP2EX-HSRL2_P3B_20190915_R0.h5')         # XXX default input for 20190915
print("HSRL time range %g .. %g on %s" % ( *hsrl.timerange, hsrl.Day0))

hsrl.use_freq( 3600/15 ) # subsample to about 1 sample every 15 seconds

hsrl_dust = hsrl.ribbon_for( '/DataProducts/Dust_Mixing_Ratio' )
hsrl_532bsc = hsrl.ribbon_for( '/DataProducts/532_bsc' )
hsrl_532aot = hsrl.ribbon_for( '/DataProducts/532_AOT_above_cloud' )  # just a scalar function of time, not a ribbon

hsrl_rat_1064_532 = hsrl.ribbon_ratio( '/DataProducts/1064_bsc', '/DataProducts/532_bsc' )


# also non-ribbon scalars
hsrl_altitude = hsrl.ribbon_for( '/Nav_Data/gps_alt' )
## hsrl_cloudtop = hsrl.ribbon_for( '/DataProducts/cloud_top_height' )
hsrl_532depol = hsrl.ribbon_for( '/DataProducts/532_dep' )


rsp = RSP('../CAMP2Ex/RSP/20190915', '20190915')
ampr = AMPR('../CAMP2Ex/AMPR/CAMP2Ex-AMPR_P3B_20190915_R0.nc', '20190915')

######
# Inhale CPI images

cpiimages = CPIImages('../CAMP2Ex/RF09_subset_CPI_images', '20190915')

###
# Mapping of animation frames to time-window
##full_timerange = hsrl.timerange[0] - 0.5, hsrl.timerange[1] + 0.5
full_timerange = camp2exvis.full_timerange( '20190915' )   # tuple(starting_datetime, ending_datetime)
full_timerange_hours_since_0h = [ d.hour + d.minute/60. + d.second/3600. for d in full_timerange ] # e.g. 21.71666, 29.71666  from 21:43 UTC 9/15 to 05:43 UTC 9/16

time_window_width = 20 * (1.0/60) # 20 minutes' width of window.
time_incr = 1.0/3600      # 1 second per frame

startframe = 0
endframe = 28800   # or maybe full_timerange_hours_since_0h[1] - ...[0]) * 3600
framestep = 60

outimagesize = ( 880, 1840 )
renderdpi = 200
antialias = 2.0

ii = 1
if ii < len(sys.argv):
    ss = sys.argv[ii].replace('-',' ').replace('%',' ')
    vv = [int(s) for s in ss.split()]
    startframe = vv[0]
    endframe = vv[1] if len(vv)>1 else startframe
    framestep = vv[2] if len(vv)>2 else 1


if not os.path.isdir( os.path.dirname(outfmt) ):
    os.makedirs(os.path.dirname(outfmt))

maxframe = int( (full_timerange_hours_since_0h[1] - full_timerange_hours_since_0h[0]) / time_incr )

endframe = min(endframe, maxframe)




print("# frames %d-%d%%%d of 0..%d" % (startframe,endframe,framestep, maxframe))

for frameno in range(startframe, endframe+1, framestep):
    tcenter = full_timerange_hours_since_0h[0] + frameno * time_incr

    trange = tcenter - 0.5*time_window_width, tcenter + 0.5*time_window_width

    outfname = outfmt % frameno

    
    rsp.make_plot(1, None, trange, tcenter, cmap=ribbon_plot_cmap)

    fig, (axhsrl1, axhsrldepol, axhsrlrat, axcpi) = mp.subplots( nrows=4, ncols=1, sharex=False, sharey=False ) # sharey=False, right?
    fig.set_size_inches( outimagesize[0]*antialias/renderdpi, outimagesize[1]*antialias/renderdpi )    # trying to get 1840 pixels tall.   This with dpi=132 yields 880x1842, close.


    for ax in axhsrl1, axhsrldepol, axhsrlrat: ## , axrsp
        #ax.set_facecolor( (0.20, 0.20, 0.20, 1.0) )    # See also mp.style.use(...) above, to set dark-background theme
        ax.set_facecolor( backcolor )    # See also mp.style.use(...) above, to set dark-background theme
    axcpi.set_facecolor( (0.70, 0.70, 0.70, 1.0) )     # brighter default color for CPI particle-mugshot panel, which has a white background

    timelabel = ( hsrl.Day0 + datetime.timedelta(hours=tcenter) ).strftime('%Y-%m-%d %H:%M:%S')

    ribbon( onax=axhsrl1, timerange=trange, do_log10=True, vrange=(-4, -1), tnow=tcenter, **hsrl_532bsc, title="log HSRL 532 bsc", maintitle="%s\nHSRL 532 bsc + alti(red) + cloudtop(grn)" % timelabel, cmap=ribbon_plot_cmap )
    ribbon( onax=axhsrldepol, timerange=trange, vrange=(0.0, 0.3), tnow=tcenter, **hsrl_dust, title="HSRL Depol Ratio(532)", cmap=ribbon_plot_cmap )

    ribbon( onax=axhsrlrat, timerange=trange, vrange=(0.0, 1.0), tnow=tcenter, **hsrl_rat_1064_532, title="HSRL ratio 1064/532 bsc", cmap=ribbon_plot_cmap )
    ##ribbon( onax=axhsrldust, timerange=trange, tnow=tcenter, **hsrl_dust, title="HSRL Dust Ratio", cmap=ribbon_plot_cmap )
    axhsrl1.plot( hsrl_532depol['times'], hsrl_532depol['values'], 'g', alpha=0.4 )
    axhsrl1.plot( hsrl_altitude['times'], hsrl_altitude['values'], 'r', alpha=0.4 )

    # CPI images
    cpimrgb = cpiimages.image_at_time( tcenter )
    if cpimrgb is not None:
        shp = cpimrgb.shape
        #axcpi.imshow( cpimrgb, extent=(0, shp[1], 0, shp[0]) )
        axcpi.imshow( cpimrgb.transpose(1,0,2)[:, ::-1, :], extent=(0, shp[0], 0, shp[1]) )
    axcpi.set_yticks([])
    axcpi.set_xticks([])

    #axrsp.set_xticks([])
    #axrsp.set_yticks([])


    fig.set_tight_layout(True)

    print("Writing", outfname)
    fig.savefig( outfname, dpi=renderdpi )
    mp.close( 'all' )

    # Shrink image down 2x for antialiasing
    im = PIL.Image.open( outfname )
    nim = im.resize( size=outimagesize, resample=PIL.Image.LANCZOS )
    nim.save( outfname )
