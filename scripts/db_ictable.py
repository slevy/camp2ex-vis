#! /usr/bin/env python3

import sys, os
import numpy

import glob

import camp2exvis

from ict import ICTReader

class ICTable(object):

    maxdwell = 10.0 / 60.0  # 10-minute dwell time

    whatname = "LARGEOptical"

    def __init__(self, fname, Day0yyyymmdd, fieldnames=None):

        self.fname = fname

        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )
        self.fieldnames = fieldnames

        self.loaded = False
        self.times_h = None
        self.data = {}

    def load_data(self):
        if self.loaded:
            return
        self.loaded = True

        with open(self.fname, 'r') as ictf:
            ictr = ICTReader(ictf)
            if self.fieldnames is None:
                self.fieldnames = ictr.fieldnames

            timefield = 'Time_Start' if 'Time_Start' in self.fieldnames else ictr.fieldnames[0]
            times = []
            data = []
            for row in ictr:
                times.append( float(row[timefield]) )
                data.append( [ float(row[v]) for v in self.fieldnames ] )

            self.data = numpy.array( data )
            self.ixdata = dict( (fieldname,i) for (i,fieldname) in enumerate(self.fieldnames) )
            self.times_h = numpy.array( times ) / 3600.0

    def keys(self):
        return self.fieldnames


    def getslice( self, trange, fieldnames=None ):
        self.load_data()

        n_1 = len(self.times_h) - 1
        j0, j1 = sorted( [ min(j, n_1) for j in numpy.searchsorted( self.times_h, trange ) ] )
        if j0 >= j1:
            return None, None

        if fieldnames is None:
            fieldnames = self.fieldnames

        return self.times_h[j0:j1], dict( (fieldname, self.data[ j0:j1, self.ixdata[fieldname] ]) for fieldname in self.fieldnames )
