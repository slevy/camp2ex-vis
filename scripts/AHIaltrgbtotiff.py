#! /usr/bin/env python3

import sys, os
import h5py
import numpy
import datetime

import geoimage


#outdir = "../data/AHI/natural"
outdir = "../data/AHI/20190915/natrgb"
scale = 1.0
offset = 0.0
chans = [ 5,4,3 ]
gamma = 1.0
do_brf = False
brf_mincos = 0.1
cropbox = None

def Usage():
    print("""Usage: %s [-o outdir] [-brf] [-mincos VAL] [-gamma G] [-scale SCALE[,OFFSET]] [-chans 5,4,3] [-te lon0 lat0 lon1 lat1] ../CAMP2Ex/AHI/NC_H08_20190916_0150_R21_FLDK*.nc
Default -o %s
Converts netCDF Himawari images to GeoTIFFs, using the using the "RGB natural" encoding
(R=band 5 (1.6um), G=band 4(0.86um), B=band 3(0.64um)) unless -chans given.
Imitates the naming convention of the HIMA optical images, "hima8YYYYMMDDHHMMSSfd.tif"
with the YMDHMS in JST (UTC+9) time zone.
Use -scale to multiply all pixel values by that factor (-scale 5 looks nice for NaturalRGB 5,4,3).
-scale SCL,OFF adds an offset: outpixel_u16 = inpixel_s16 * SCL + OFF  (OFF is in 16bit-int units)
-chan tbb_11 -scale 3,30000 might be good for TB11 temperature.  Then out 0 = -100C, 65535 = +118.45 C, 0.03 C per pixel step.
Use -brf to multiply pixels by 1/cos(solar zenith angle) to estimate reflectance ("BRF").
  Use -mincos to specify min value of cos(zenithangle) (default -mincos 0.1) -- limit brightening near terminator
Use -te lon0 lat0 lon1 lat1 to crop to that lat-lon box, gdalwarp-style.""" % (sys.argv[0], outdir))
    sys.exit(1)


JST_zone = 9.0

ii = 1
while ii < len(sys.argv) and sys.argv[ii][0] == '-':
    opt = sys.argv[ii]; ii += 1
    if opt == '-o':
        outdir = sys.argv[ii]; ii += 1
    elif opt == '-scale':
        ss = sys.argv[ii].split(','); ii += 1
        scale = float(ss[0])
        offset = float(ss[1]) if len(ss)>1 else 1.0
    elif opt == '-chans':
        chans = sys.argv[ii].split(','); ii += 1
    elif opt == '-brf':
        do_brf = True
    elif opt == '-mincos':
        brf_mincos = float(sys.argv[ii]); ii += 1
    elif opt == '-gamma':
        gamma = float(sys.argv[ii]); ii += 1
    elif opt == '-te':
        cropbox = [ float(s) for s in sys.argv[ii:ii+4] ]; ii += 4
    else:
        Usage()

if len(sys.argv) <= ii:
    Usage()

if not os.path.isdir(outdir):
    os.makedirs(outdir)

for ncf in sys.argv[ii:]:

    hf = h5py.File(ncf, 'r')
    hfchans = []
    for c in chans:
        try:
            chan = "albedo_%02d" % int(c)   # if given a channel number (int or string)
        except:
            chan = c                    # if given a literal channel name, like "AOT" or "tbb_11"
        hfchans.append( chan )

    dim = hf[ hfchans[0] ].shape

    lons = hf[ 'longitude' ][:]
    lats = hf[ 'latitude' ][:]
    dlon = (lons[-1] - lons[0]) / (len(lons)-1)
    dlat = (lats[0] - lats[-1]) / (len(lats)-1)   # reversed since lats[] is in reverse order

    if cropbox is None:
        slicelat = slice(None,None)
        slicelon = slice(None,None)
    else:
        ilat0, ilat1 = numpy.searchsorted( -lats, (-cropbox[1], -cropbox[3]) )  # negated since lats[] is in reverse order, most-positive first
        ilat0, ilat1 = min(ilat0,ilat1), max(ilat0,ilat1)
        ilon0, ilon1 = numpy.searchsorted( lons, (cropbox[0], cropbox[2]) )
        ilat1 = min(ilat1, len(lats)-1)
        ilon1 = min(ilon1, len(lons)-1)

        print("Cropbox indices %d %d  %d %d => %g %g  %g %g" % (ilon0, ilat1, ilon1, ilat0, lons[ilon0], lats[ilat1], lons[ilon1], lats[ilat0]))
        if ilat0 >= ilat1 or ilon0 >= ilon1:
            raise ValueError("No pixels in cropbox %g %g  %g %g  from source's range %g %g  %g %g" % (*cropbox, lons[0],lats[-1], lons[-1],lats[0]))

        slicelon = slice(ilon0,ilon1)
        slicelat = slice(ilat0,ilat1)
        lons = lons[slicelon]
        lats = lats[slicelat]
        dim = (len(lats), len(lons))
    

    gim = geoimage.GeoImage()
    gim.set_proj_epsg( 4326 ) # lat/lon Plate Carree projection
    gim.set_ulcorner( lons[0], lats[0] )  # longitude 80 E, latitude 60 N at NW corner
    gim.set_spatialres( dlon, dlat )

    if do_brf:
        soz_scalefactor = 0.01 # degrees per unit int16.
        scaled_sec_za = scale / numpy.clip( numpy.cos( numpy.radians( hf[ 'SOZ' ][slicelat][:, slicelon]*soz_scalefactor ) ), brf_mincos, None )
        

    pixels = numpy.empty( dim + (len(chans),), dtype=numpy.uint16 )  # AHI netCDF written as signed 16-bit int; output is 16-bit unsigned.
    mx = [0] * len(chans)
    ninvalid = [0] * len(chans)
    if do_brf:
        scale_by = scaled_sec_za # spatially varying grid of scale / sin(zenithangle)
    else:
        scale_by = scale         # constant scale factor
    for i, chan in enumerate(chans):
        if gamma != 1:
            vv = (hf[ hfchans[i] ][slicelat][:, slicelon]*(scale_by + offset) / 65535.) ** (1.0/gamma)
            pixels[:,:,i] = numpy.clip( vv*65535, 0, 65535 ).astype(numpy.uint16) # write 16bit unsigned int GeoTIFF
        else:
            pixels[:,:,i] = numpy.clip( hf[ hfchans[i] ][slicelat][:, slicelon]*scale_by + offset, 0, 65535 ).astype(numpy.uint16) # write 16bit unsigned int GeoTIFF
        mx[i] = pixels[:,:,i].max()
        ninvalid[i] = (hf[ hfchans[i] ][:] == -32768).sum()

    # NC_H08_20190916_0150_R21_FLDK*.nc
    # hima820190916072000fd.png
    ss = os.path.basename( ncf ).split('_')  # NC H08 20190916 0150

    utctime = datetime.datetime.strptime(ss[2]+ss[3], "%Y%m%d%H%M")
    jsttime = utctime + datetime.timedelta(hours=JST_zone)
    outfname = jsttime.strftime("hima8%Y%m%d%H%M%Sfd.tif")
    outfname = os.path.join( outdir, outfname )

    def sval(vvv):
        return (",".join(("%d",)*len(vvv))) % tuple(vvv)
    print("Converting %s to %s  (max %s) (ninvalid %s)" % (ncf, outfname, sval(mx), sval(ninvalid)))

    # Need external processing?
    gim.saveimage( outfname, pixels, tilesize=None, bigtiff=False )
