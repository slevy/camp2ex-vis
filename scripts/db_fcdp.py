#! /usr/bin/env python3

import sys, os
import datetime
import re

import numpy

import glob

import camp2exvis
import camp2expages

from camp2exdb import DatedFiles

import ict

class FCDP(object):
    """For FCDP / 2DS10, which both measure droplet size distribution, on different spatial scales, but with similar file formats"""

    maxdwell = 10.0 / 60.0  # 10-minute dwell time

    whatname = "FCDPsizedist"

    def __init__(self, fcdpname, Day0yyyymmdd):

        ##super().__init__(fcdpname, Day0yyyymmdd)
        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

        with open(fcdpname, 'r') as ictf:
            ictr = ict.ICTReader( ictf )

            self.binspan = {}

            # cbin11, #/liter/um, CldMicro_NumSizeDist_InSitu_Optical_Drop_AMB, concentration of size ( 18.0 -21.0 um)    

            binpat = re.compile('(cbin\d+).*\(\s*(([.\d]+)\s*-\s*([.\dinf]+)|>\s*([.\d]+))')  # of size ( 18.0-21.0 ... or  150-inf or ( > 50.0 um )
            # discover bin ranges for cbinNN's
            for iline in ictr.headlines:
                m = binpat.match(iline)
                if m is not None:
                    key, junk, size0, size1, size0min = m.groups()
                    if size0min is None:
                        size0, size1 = float(size0), float(size1)
                    else:
                        size0 = float(size0min) # "> value"  == "value - infinity"
                        size1 = numpy.inf

                    self.binspan[key] = (size0, size1)

            self.bins = sorted( self.binspan.keys() )
            self.binranges = [ self.binspan[key] for key in self.bins ]

            cbinvals = []
            cbintimes = []

            for row in ictr:
                time_h = float(row['Time_Start']) / 3600.0
                
                cbintimes.append( time_h )
                cbinvals.append( [ float(row[bin]) for bin in self.bins ] )

            cbinvals = numpy.array( cbinvals )
            self.cbinvals = numpy.ma.masked_array( cbinvals, cbinvals==-999 )
            self.cbintimes = numpy.array( cbintimes )

            ## self.binrange = ( self.binspan[ self.bins[0] ][0], numpy.nanmax( self.binspan[ self.bins[-1] ] ) )
            self.binedges = [ binL for (binL,binR) in self.binranges ]
            self.binwidths = [ (binR-binL) if numpy.isfinite(binR) else 3 for (binL,binR) in self.binranges ]

    def make_plot(self, plotno, onax, trange, tcenter, color='c', vscale=1.0, label='FCDP @'):

        j = min( numpy.searchsorted( self.cbintimes, tcenter ), len(self.cbintimes)-1 )

        onax.bar( self.binedges, vscale*self.cbinvals[j], width=self.binwidths, align='edge', linewidth=2, color=(0,0,0,0), edgecolor=color, alpha=0.67, label=label.replace('@', 'size dist') )
        ##onax.plot( self.binedges, vscale*self.cbinvals[j], linewidth=2, color=color, label=label.replace('@', 'size dist') )

        onax.set_ylim( 0, 200 )
        onax.set_xlim( 0, 3000 )
        onax.set_xscale( 'symlog', linscalex=1.0 ) # basex=10, linscalex=1.0
        onax.set_yscale( 'symlog', linscaley=1.0, linrangey=3.0 ) # basey=10, linscaley=1.0
        #yticks = [0.5, 1, 2, 5, 10, 20, 50, 100]
        yticks = [0.5, 1, 3, 10, 30, 100]
        onax.set_yticks( yticks )
        onax.set_yticklabels( ["%g"%v for v in yticks] )
        yminorticks = [2, 4, 5, 6, 7, 8, 9, 20, 40, 50, 60, 70, 80, 90]
        onax.set_yticks( yminorticks, minor=True )

        onax.set_xlabel('$um$')
        onax.set_ylabel('$n/cm^3/\mu{m}$')
        onax.legend( loc='upper right' )


    # No need for make_plot() here.
    #   camp2expages.DropSondePage() calls our image_at_time() 

if __name__ == "__main__":
    fcdp = FCDP("../CAMP2Ex/SPEC Instruments/CAMP2Ex-FCDP_P3B_20190915_R1.ict")
