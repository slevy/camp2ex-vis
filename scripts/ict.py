#! /usr/bin/env python3

import sys, os
import csv

class ICTReader(csv.DictReader):
    """Use as with csv.DictReader.   E.g.
    with open('file.ict','r') as open_file_object:  # must be in text, not binary mode
        ictr = ICTReader( open_file_object )
        # ictr.fieldnames has list[] of fields, as with csv.DictReader
        # ictr.headlines is list[] of all the lines in the ICARTT header.
        for row in ictr:
            # each row is an OrderedDict of fieldname -> fieldvalue"""

    def __init__(self, infile):
        self.infile = infile

        headline = self.infile.readline()   # "28, 1001"

        ss = headline.split(',')
        if len(ss) >= 2:
            nheadlines = int(ss[0])-2

        headlines = [ headline ]
        for i in range(nheadlines):
            line = self.infile.readline()
            headlines.append( line )

        self.headlines = headlines
        # from here on, it's a .csv file, including the header line.

        super().__init__( infile )


if __name__ == "__main__":
    with open(sys.argv[1], 'r') as ictf:
        dr = ICTReader( ictf )
        print("fieldnames: ", dr.fieldnames)
        n = 0
        for i, row in enumerate(dr):
            print(row)
            if i >= 3:
                break
            
