#! /usr/bin/env python3

import sys, os
import datetime
import re

import numpy
import netCDF4

import matplotlib.transforms   # needed for RSP.make_plots() 
import matplotlib.cm     as cm

from camp2exdb import DatedFiles

from camp2expages import ribbon

import camp2exvis


class RSPCld( DatedFiles ):


    ## new style from https://data.giss.nasa.gov/pub/rsp/data/CAMP2Ex/L2CLD/
    ## RSP1-P3_20190915_T270950-271518_wcld_v1_1.nc
    prefix = 'RSP1-P3'
    suffix = '.nc'

    datepat = re.compile('%s_(\d\d\d\d)(\d\d)(\d\d)_T(\d\d)(\d\d)(\d\d)-(\d\d)(\d\d)(\d\d)_wcld' % prefix)

    maxdwell = 20*60 / 3600.0  # when assembling interval tree, assume that last file might have data for up to 20 minutes.

    whatname = "RSPCld"    # name of data source

    def __init__(self, dirname, Day0yyyymmdd):
        super().__init__( dirname, Day0yyyymmdd )

        # RSP's "/Data/Product_Time_Seconds" is in seconds since 0h UT on start of this flight's day.
        # Store the offset (seconds) between our adopted visualization 0-time for this day (self.Day0) after 0h UT on this day.
        self.time0_secs_within_day = (self.Day0 - datetime.datetime(self.Day0.year, self.Day0.month, self.Day0.day, 0, 0, 0)).total_seconds()
        self.timerange = None
        self.filetimeset = set()

        # data array fragments, indexed by whattime() 
        self.times_h = {}
        self.rft_radius = None
        self.rft_size_dist = {}  # rft_size_dist_863nm(rft_radius, time_utc)

        self.rspcld_cmap = camp2exvis.cmap_for( 'rspcld' )


    def whattime(self, fname):
        """parses filename, returns floating-point hours since visualization-time-0 on Day0.
        Note: these time numbers must be in the same time system as the self.times_h[] values read from inside the datafile in load_data()."""
        # CAMP2EX-RSP1-CLD_P3B_20190915223026_R2.h5
        m = self.datepat.search(fname)
        if m is None:
            return None

        # datepat = re.compile('%s_(\d\d\d\d)(\d\d)(\d\d)_T(\d\d)(\d\d)(\d\d)-(\d\d)(\d\d)(\d\d)_wcld' % prefix)
        y,m,d, H0,M0,S0, H1,M1,S1 = [int(v) for v in m.groups()]

        # return (starttime,endtime) tuple of floating-point hours since 0h UTC for start of the day's flight
        # For these files the "hour" field might exceed 23, so add the H field separately -- datetime() doesn't want e.g. H=28.
        tstart_h = (datetime.datetime( y,m,d, 0,M0,S0 ) - self.Day0).total_seconds() / 3600.0 + H0
        tend_h   = (datetime.datetime( y,m,d, 0,M1,S1 ) - self.Day0).total_seconds() / 3600.0 + H1
        return (tstart_h, tend_h)


    def gather_separated(self, ddict, trange, timeaxis=0):
        # Only gather timespans which overlap the needed time range.
        # AND, for this instrument at least they seem to be widely separated in time.
        # Don't concatenate them, but let the caller plot each chunk as a separate piece of ribbon.
        
        def overlap(trA, trB):
            return trA[1] > trB[0] and trA[0] < trB[1]

        ftimes = sorted( [ t0t1 for t0t1 in ddict.keys() if overlap(t0t1, trange) ] )

        if len(ftimes) == 0:
            return numpy.array([]) # or None, but this [] makes calling ribbon() easier.


        pieces = [ ddict[ftime] for ftime in ftimes ]

        ## are the components masked arrays?
        #if hasattr( ddict[ftimes[0]], 'mask' ):
        #    result = numpy.ma.concatenate( pieces, axis=timeaxis )
        #else:
        #    result = numpy.concatenate( pieces, axis=timeaxis )

        return pieces


    def load_timerange(self, timerange):
        # find all the data files within the given timerange, in increasing time order
        fit = self.files_in_timerange( timerange )
        for fname in self.files_in_timerange( timerange ):
            self.load_data( fname )


    def any_data_during(self, timerange):
        self.load_timerange( timerange )
        values = self.gather(self.rft_size_dist)

        if values is None:
            return False

        times = self.gather(self.times_h)
        j0, j1 = sorted( [min(j, len(times)-1) for j in numpy.searchsorted( times, timerange, side='left' )] )

        if j0 >= j1:
            return False

        windowvalues = values[j0:j1]
        gotany = not numpy.all( windowvalues.mask )

        if False:  # XXX DEBUG
            windowtimes = times[j0:j1]
            gotrpt = "True range %.3f %.3f" % (windowvalues.min(), windowvalues.max()) if gotany else "False"
            print("rsp any(%.2f, %.2f) gathered (%.2f, %.2f) any: %s" % (*timerange, windowtimes.min(), windowtimes.max(), gotrpt))

        return gotany

    def masked_where( self, invalidval, values ):
        if invalidval is None:
            return numpy.ma.masked_array( values, numpy.logical_not( numpy.isfinite(values) ) )
        else:
            return numpy.ma.masked_array( values, (values==invalidval) | numpy.logical_not( numpy.isfinite(values) ) )

    def load_data( self, fname ):
        ftime = self.whattime(fname)
        if ftime in self.filetimeset:
            return None

        self.filetimeset.add( ftime )
        ds = netCDF4.Dataset( fname, 'r' )

        if self.rft_radius is None:
            self.rft_radius = ds['rft_radius'][:]

        self.times_h[ftime] = tchunk = ds['time_utc'][:]  # time_utc is already in hours since 0hUTC on start-of-flight date, e.g. 27.3 - just what we wanted

        self.rft_size_dist[ftime] = ds['rft_size_dist_863nm'][:].transpose()  # was Nradii, Ntimes => Ntimes, Nradii

        if self.timerange is None:
            self.timerange = (tchunk[0], tchunk[-1])
        else:
            self.timerange = ( min(self.timerange[0], tchunk[0]), max(self.timerange[1], tchunk[-1]) )

    plotlabel = [ 'RFT radius' ]
    plotcolor = [ 'g'          ]
    plotvrange = [ (0,0.5)  ]
    plottitle = [ 'RSP particle size distrib' ]
    plotylabel = [ '$\mu{m}$'  ]
    plotvlabel = [ '$\mu{m}^{-1}$' ]
    plotyrange = [ (0,30)     ]


    def make_plot( self, plotno, onaxis, timerange, tnow):
        """RSPCld.make_plot().  0:Size distribution"""

        self.load_timerange( timerange )

        yrange = self.plotyrange[plotno] #  self.rft_radius[0], self.rft_radius[-1]
        vrange = self.plotvrange[plotno]
        extent = (timerange[0], yrange[0], timerange[1], yrange[-1])

        onaxis.set_label( self.plotlabel[plotno] )
        onaxis.set_xlim( *timerange )
        onaxis.set_ylim( *yrange )
        onaxis.set_yscale( 'symlog', linscaley=1.0, linthreshy=5.0 )  # nonlinear scale, emphasizing small values.  (where's the knee?)

        onaxis.set_ylabel( self.plotylabel[plotno] )

        onaxis.set_adjustable('box')

        if self.times_h == {}:
            return

        ddata = [ self.rft_size_dist ][ plotno ]
        agg_values = self.gather_separated(ddata, timerange)
        agg_times_h = self.gather_separated(self.times_h, timerange)

        imp = None
        for times_h, values in zip(agg_times_h, agg_values):
            imp = ribbon( onaxis, timerange, tnow, vrange=vrange, values=values, alts=self.rft_radius, times=times_h,
                cmap=self.rspcld_cmap,
                title=self.plotvlabel[plotno],
                maintitle=self.plotlabel[plotno],
                do_cbar=False)

        if imp is None:
            imp = cm.ScalarMappable(cmap=self.rspcld_cmap)
        imp.set_clim( vrange )
        cbar = onaxis.figure.colorbar( imp, ax=onaxis, orientation='horizontal',
                cmap=self.rspcld_cmap, pad=0.05, fraction=0.18, aspect=40 )
        ##cbar.ax.set_xlabel( self.plotvlabel[plotno] )
        # manual label placement - on the axis that's associated with the colorbar
        onaxis.figure.add_artist( matplotlib.text.Text( x=0.5, y=-1.6, text=self.plotvlabel[plotno], rotation=0, verticalalignment='center', horizontalalignment='center', transform=cbar.ax.transAxes ) )
        
        
