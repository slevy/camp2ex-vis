#! /usr/bin/env python3

import sys, os

import h5py
import numpy

import camp2exvis

import matplotlib.pyplot as mp

class SPNS(object):
    """
    SPNS total: spns_tot_flux (0..2.5) vs spns_wvl (300..1100)
    SPNS diffuse:  spns_dif_flux ... vs spns_wvl
    along with sza solar zenith angle
    tmhr = (hours since 0h UTC)

    """

    stemname = "spns"

    def __init__(self, fname, Day0yyyymmdd):

        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

        # time (hours) from 0h UTC on flight day, to visualization-frame 00000
        self.start_frame_hours = (self.timebase - self.Day0).total_seconds() / 3600.0  # tmhr of frame 0

        self.fname = fname
        self.datetag = Day0yyyymmdd
        self.hf = h5py.File(fname, 'r')
        self.times = self.hf['tmhr'][:]
        self.spns_tot_flux = self.hf['spns_tot_flux'][:]
        self.spns_dif_flux  = self.hf['spns_dif_flux'][:]
        self.spns_wvl = self.hf['spns_wvl'][:]
        self.sza     = self.hf['sza'][:]    # solar zenith angle

    def hour2index(self, timenow):
        """Returns closest index to current time (in hours from 0h UTC on flight day). (Maybe we'll want to change this to rounding down to the nearest index?)"""
        idx = (numpy.abs(self.times - timenow)).argmin()
        return idx

    def frame2index(self, curr_frame, start_frame=0):
        """Returns closest index to current frame. (Maybe we'll want to change this to rounding down to the nearest index?)"""

        # Convert frame to time (in hours)
        seconds_elapsed = curr_frame - start_frame
        hours_elapsed = seconds_elapsed / 60.0 / 60.0
        curr_hour = self.start_frame_hours + hours_elapsed

        # Convert time to index
        idx = self.hour2index(curr_hour)

        return idx

    y_range_plot = (0, 2.25)
    x_range_plot = (300, 2200)

    def make_plot(self, plotno, axis, trange, timenow, legends=None):

        # there is only one plot - ignore plotno
        # we only display the current instant - ignore trange

        # Convert timenow to closest tmhr index
        timeidx = self.hour2index(timenow)
        curr_tot_flux = self.spns_tot_flux[timeidx]
        curr_dif_flux = self.spns_dif_flux[timeidx]
        curr_sza      = self.sza[timeidx]

        # Set plot axes
        axis.set_xlim( self.x_range_plot )
        axis.set_ylim( self.y_range_plot )

        # Draw plot lines
        artistt = axis.scatter(self.spns_wvl, curr_tot_flux, s=1, c='y', label="SPNS Total")
        artistd = axis.scatter(self.spns_wvl, curr_dif_flux, s=1, c='g', label="SPNS Diffuse")

        # Text overlay
        if numpy.isfinite(curr_sza):
            szastr = "SZA %.1f" % curr_sza
        else:
            szastr = "SZA ----"
        axis.text(1760, 1.1, szastr, color="w", fontdict=dict(fontweight='bold')) # , fontdict={"fontsize":30,"fontweight":'bold',"ha":"left", "va":"baseline"})

        # Set labels
        ##axis.set_title("Solar Spectral Flux\nRadiometer (SSFR)", y=0.95, pad=-14 )  # or plt.rcParams['axes.titley'] = 1.0 and ['axes.titlepad'] = -14 -- put title just below top of plot box
        axis.set_xlabel("Wavelength ($nm$)")
        axis.set_ylabel("Flux ($Wm^{-2}nm^{-1}$)") # Is this right? Units copied from Larry's slide

        # Set legend

        if legends is not None:
            legends.append( (artistt, "SPNS Total") )
            legends.append( (artistd, "SPNS Diffuse") )
        else:
            legend = axis.legend(loc="upper right")
            ##legend.legendHandles[0]._sizes = [30]
            ##legend.legendHandles[1]._sizes = [30]


