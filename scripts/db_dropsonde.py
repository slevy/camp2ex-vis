#! /usr/bin/env python3

import sys, os
import datetime

import glob

import camp2exvis
import camp2expages

from camp2exdb import DatedFiles

import ict

from intervaltree.intervaltree import IntervalTree
from intervaltree.interval import Interval

class DropSonde(DatedFiles):

    maxdwell = 10.0 / 60.0  # 10-minute dwell time

    whatname = "dropsonde"

    def __init__(self, dirname, Day0yyyymmdd):

        super().__init__(dirname, Day0yyyymmdd)
        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

    def buildtree(self, dirname):
        # overriding superclass's buildtree()

        self.filetimes = {}
        self.inti = IntervalTree()

        self.dirname = dirname
        found_pngs = sorted( glob.glob( os.path.join(self.dirname, '*_L[0-9][0-9].png') ) )

        def ict_of_png(pngfname):
            # Original AVAPS dropsonde png's had "-skewt-image" in their filenames.
            # Thanks to Rose Miller for cooking up new and cooler ones.  They have _forAVL in their filenames.
            # Either way, edit it out to turn the .png filename into corresponding .ict source data.
            return pngfname.replace('-skewt-image','').replace('_forAVL','').replace('.png','.ict')

        pngs = [pngf for pngf in found_pngs if os.path.exists( ict_of_png(pngf) )]
        if len(pngs) < len(found_pngs):
            print("## Didn't find .ict's for these %d L<nn>.png files:" % (len(pngs)-len(found_pngs)))
            print("".join(["#\t%s\n" % pngf for pngf in found_pngs if pngf not in pngs]))

        for pngf in pngs:
            ictfname = ict_of_png( pngf )
            with open(ictfname, 'r') as ictfb:
                ictr = ict.ICTReader(ictfb)
                t0 = None
                lon = lat = None
                
                tagnn = pngf[-6:-4]   # turn "blah_L03.png" => "03"
                timestamp = "???"
                for hl in ictr.headlines:
                    # Launch Time: 2019-09-15 22:47:26
                    if hl.startswith('Launch Time'):
                        ss = hl.split()
                        timestamp = "P3B_%s_L%s_%s Z" % (ss[2], tagnn, ss[3])
                    
                for row in ictr:
                    # "Time_Start" field is in seconds since 0h UTC on flight day.   Convert to hours since 0h UTC.
                    t1 = float( row['Time_Start'] ) / 3600.0
                    t0 = t1 if t0 is None else t0
                    if lon is None and float(row['Longitude']) != -9999:
                        lon = float(row['Longitude']) 
                    if lat is None and float(row['Latitude']) != -9999:
                        lat = float(row['Latitude']) 
                

            self.filetimes[pngf] = (tagnn, t0,t1,lon,lat, timestamp)

            # Add to IntervalTree, including dwell-time margin
            self.inti.addi( t0, t1+self.maxdwell, pngf )

        self.files_by_nn = sorted( self.filetimes.keys(), key = lambda pngf: self.filetimes[pngf][0] )


    def index_at_time(self, time):
        pngf = self.file_at_time(time)
        if pngf not in self.files_by_nn:
            return None
        return self.files_by_nn.index( pngf )


    def ticklist(self):
        """Return dict of lists: 'time_h' droptimes (in hours since 0h UTC), 'tag' dropsonde labels (S01, S02...), 'lon' longitude of drop, 'lat' latitude of drop, 'timestamp' in form "P3B_yyyymmdd_Lnn_hh:mm:ss Z"
        """
        sondetags = []; times = []; lons = []; lats = []; tags = []; timestamps = []
        for pngf in self.files_by_nn:
            (tagnn, t0,t1,lon,lat, timestamp) = self.filetimes[pngf]
            sondetags.append('S' + tagnn) # Snn
            times.append(t0)
            lons.append(lon)
            lats.append(lat)
            timestamps.append(timestamp)

        return { 'time_h':times, 'tag':sondetags, 'lon':lons, 'lat':lats, 'timestamp':timestamps }
        
    def whattime(self, fname):
        if fname in self.filetimes:
            return self.filetimes[fname]
        return None


    # No need for make_plot() here.
    #   camp2expages.DropSondePage() calls our image_at_time() 
