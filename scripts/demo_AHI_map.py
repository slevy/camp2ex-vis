#!/usr/bin/env python

## Also used advice from here:
##  https://stackoverflow.com/questions/58247868/how-can-i-plot-a-png-image-on-a-cartopy-basemap-in-a-special-projection

import os
from IPython.core.display import HTML

#with open('creative_commons.txt', 'r') as f:
#    html = f.read()
html = 'License: Creative Commons<p>\n'
name = '2015-03-02-geotiff'

html = '''
<small>
<p> This post was written as an IPython notebook.
 It is available for <a href='https://ocefpaf.github.com/python4oceanographers/downloads/notebooks/%s.ipynb'>download</a>
 or as a static <a href='https://nbviewer.ipython.org/url/ocefpaf.github.com/python4oceanographers/downloads/notebooks/%s.ipynb'>html</a>.</p>
<p></p>
%s''' % (name, name, html)

get_ipython().run_line_magic('matplotlib', 'inline')
from matplotlib import style
style.use('ggplot')

from datetime import datetime

title = "Plotting geotiff with cartopy"
hour = datetime.utcnow().strftime('%H:%M')
comments="true"

date = '-'.join(name.split('-')[:3])
slug = '-'.join(name.split('-')[3:])

metadata = dict(title=title,
                date=date,
                hour=hour,
                comments=comments,
                slug=slug,
                name=name)

markdown = """Title: {title}
date:  {date} {hour}
comments: {comments}
slug: {slug}

{{% notebook {name}.ipynb cells[1:] %}}
""".format(**metadata)

#content = os.path.abspath(os.path.join(os.getcwd(), os.pardir, os.pardir, '{}.md'.format(name)))
content = "/tmp/" + "{}.md".format(name)
with open('{}'.format(content), 'w') as f:
    f.writelines(markdown)


# This post is a quick example on how to read geotiff images with GDAL and
# plot them with Cartopy.
# 
# The image, and most of the code, is from Kelsey Jordahl
# [original notebook](http://nbviewer.ipython.org/github/mqlaql/geospatial-data/blob/master/Geospatial-Data-with-Python.ipynb).
# The main difference is that I compressed the geotiff with:
# 
# `gdal_translate -co "TILED=YES" -co "COMPRESS=JPEG" manhattan.tif manhattan2.tif`
# 
# First: read the geotiff image with GDAL.

# In[2]:

####################################################

from osgeo import gdal, osr

gdal.UseExceptions()


fname = 'ahi-tb11.tif'

ds = gdal.Open(fname)
data = ds.ReadAsArray()
gt = ds.GetGeoTransform()
proj = ds.GetProjection()

inproj = osr.SpatialReference()
inproj.ImportFromWkt(proj)

print(inproj)


# Second: convert the [WKT](http://en.wikipedia.org/wiki/Well-known_text) projection information to a cartopy projection.

# In[5]:


import cartopy.crs as ccrs

projcs = inproj.GetAuthorityCode('PROJCS')
if projcs is None:
    projection = ccrs.PlateCarree()
else:
    projection = ccrs.epsg(projcs)
print(projection)


# Third (and last): the figure.

import matplotlib.pyplot as plt
import cartopy.feature
res = '10m'

land_10m = cartopy.feature.NaturalEarthFeature('physical', 'land', res, edgecolor='face', facecolor=cartopy.feature.COLORS['land'], zorder=0)
subplot_kw = dict(projection=projection)
fig, ax = plt.subplots(figsize=(9, 9), subplot_kw=subplot_kw)

extent = (gt[0], gt[0] + ds.RasterXSize * gt[1],
          gt[3] + ds.RasterYSize * gt[5], gt[3])
img = ax.imshow((data[:, :].transpose((0, 1)).astype('f') - 32767)*.005, extent=extent,
                origin='upper', cmap='gray', vmin=-81,vmax=81, alpha=0.65)

ax.add_feature(land_10m)
ax.add_feature(cartopy.feature.COASTLINE.with_scale(res), linestyle='-')
ax.set_xlim(114,124); ax.set_ylim(4,18)
cbar = ax.figure.colorbar(img, orientation='vertical')
#ax.set_xlim(110,155); ax.set_ylim(-38,-9)

