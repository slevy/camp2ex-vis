#! /usr/bin/env python3

import sys, os
import datetime
import re

import numpy

import glob

import matplotlib.lines

import camp2exvis
import camp2expages

from camp2exdb import DatedFiles

import ict

class FIMS(object):
    """For FIMS droplet size distribution"""

    maxdwell = 10.0 / 60.0  # 10-minute dwell time

    whatname = "FIMSsizedist"

    FIMSbinsize = [ 10.0000, 11.5164, 13.2627, 15.2738, 17.5899, 20.2571, 23.3289, 26.8664, 30.9403, 35.6320, 41.0351, 47.2575, 54.4235, 62.6761, 72.1802, 83.1253, 95.7302, 110.2464, 126.9639, 146.2163, 168.3880, 193.9219, 223.3276, 257.1923, 296.1921, 341.1057, 392.8299, 452.3974, 520.9976, 600.0000 ] # in nm

    def __init__(self, fcdpname, Day0yyyymmdd):

        ##super().__init__(fcdpname, Day0yyyymmdd)
        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

        with open(fcdpname, 'r') as ictf:
            ictr = ict.ICTReader( ictf )

            self.binspan = {}
            self.bins = [ s for s in ictr.fieldnames if s.startswith('n_Dp_') ]

            # cbin11, #/liter/um, CldMicro_NumSizeDist_InSitu_Optical_Drop_AMB, concentration of size ( 18.0 -21.0 um)    

            cbinvals = []
            cbintimes = []

            # an extra blank may appear in the final 'QC_Flag' column header, sigh.
            QC_Flagtag = 'QC_Flag ' if 'QC_Flag ' in ictr.fieldnames else 'QC_Flag'

            for row in ictr:
                time_h = float(row['Time_Start']) / 3600.0
                
                cbintimes.append( time_h )

                # Consider data valid only if QC_Flag is 0.
                if float(row[QC_Flagtag]) == 0:
                    cbinvals.append( [ float(row[bin]) for bin in self.bins ] )
                else:
                    cbinvals.append( [-999 for bin in self.bins] )

            cbinvals = numpy.array( cbinvals )
            self.cbinvals = numpy.ma.masked_array( cbinvals, cbinvals==-999 )
            self.cbintimes = numpy.array( cbintimes )

            ## self.binrange = ( self.binspan[ self.bins[0] ][0], numpy.nanmax( self.binspan[ self.bins[-1] ] ) )
            fbratio = numpy.sqrt( self.FIMSbinsize[2] / self.FIMSbinsize[1] )
            FIMSbinrange = [ (v/fbratio, v*fbratio) for v in self.FIMSbinsize ]
            self.binedges = [ binL for (binL,binR) in FIMSbinrange ]
            self.binwidths = [ (binR-binL) for (binL,binR) in FIMSbinrange ]

    def make_plot(self, plotno, onax, trange, tcenter, color='g', label='FIMS @', legends=None ):
        """0: FIMS particle size (bar chart)"""

        j = min( numpy.searchsorted( self.cbintimes, tcenter ), len(self.cbintimes)-1 )

        onax.bar( self.binedges, self.cbinvals[j], width=self.binwidths, align='edge', linewidth=2, color=(0,0,0,0), edgecolor=color, alpha=0.67, label=label.replace('@', 'size dist') )
        ## onax.plot( self.FIMSbinsize, self.cbinvals[j], color=color, label=label.replace('@', 'size dist') )

        if legends is not None:
            artist = matplotlib.lines.Line2D([], [], color=color, label=label)
            legends.append( (artist,label) )

        onax.set_ylim( 1, 20000 )
        onax.set_xlim( 8, 600 )
        onax.set_xscale( 'log' )
        onax.set_yscale( 'log' )
        #yticks = [0.5, 1, 2, 5, 10, 20, 50, 100]
        #yticks = [0.5, 1, 3, 10, 30, 100]
        #onax.set_yticks( yticks )
        #onax.set_yticklabels( ["%g"%v for v in yticks] )
        #yminorticks = [2, 4, 5, 6, 7, 8, 9, 20, 40, 50, 60, 70, 80, 90]
        #onax.set_yticks( yminorticks, minor=True )

        onax.set_xlabel('$nm$')
        onax.set_ylabel('$n/cm^3$')
        onax.legend( loc='upper right' )
