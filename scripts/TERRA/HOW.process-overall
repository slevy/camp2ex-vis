#! /bin/bash

# All the steps for processing a new satellite swath for Houdini rendering

tag="$1"
eval `./lookup-tag.sh $tag` || exit 1
# sets $hf, $clock

set -e # abort on error

if [ -z "$hf" ]; then
    echo "$0 tag -- process satellite swath named tag for Houdini, starting from scratch." >&2
    exit 1
fi

# 1) generate surface meshes and texture images (time-independent).  Parallelized.
HOW.process-grids $tag | pardo -j10

# 2) compute (smoothed) satellite and sun positions (globalClock-dependent).  Serial but quick.
HOW.satpos $tag

# 3) generate beams from satellite, and corresponding reveal/hide numbers for each frame.  Parallelized.
HOW.process-beams $tag | pardo -j10

# 4) patch up gaps in reveal/hide numbers (propagate them to frames where swath wasn't in view).  Serial but quick.
HOW.rescan-beams $tag

# 5) (optional) generate beam-motion-smoothness curves.   Serial but quick.
for instr in MODIS MISR_?? ASTER ; do ../../scripts/beamrate.py ../extracts/$tag/$instr/scan/rescan*geo > logs/beamrate.$tag.$instr.dat; done
