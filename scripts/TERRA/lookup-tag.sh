#! /bin/bash

tag="$1"

F3571=../data/9-28fusion/TERRA_BF_L1B_O3571_20000819133750_F000_V001.h5
F2569=../data/9-28fusion/TERRA_BF_L1B_O2569_20000611181603_F000_V001.h5
F69626=../data/9-28fusion/TERRA_BF_L1B_O69626_20130119123228_F000_V000.h5
F20190915=../../data/TERRA/TERRA_BF_L1B_O105021_20190916015551_F000_V000.h5

CLOCK1HZ=globalClock-1hz.chan
CLOCK1=globalClock1x.chan
CLOCK2=globalClock2x.chan
CLOCK4=globalClock4x.chan

# above generated with e.g.
#  awk 'BEGIN { for(i=0; i <= 4000; i++) { print 3600*i/4000; } }' > globalClock4x.chan

clock="$CLOCK1HZ"
case "$tag" in
    20190915) clock=$CLOCK1HZ ;;
    alberto|20190915) clock=$CLOCK1 ;;
    3571x2) clock=$CLOCK4; tag=3571 ;;  # better would be to allow <tag>x2 for any <tag> in the available tracks.
esac

hf=../../data/TERRA/tracks/$tag.h5

if [ ! -f "$hf" ]; then
    them=`cd ../../data/TERRA/tracks; ls {????,?????,??????,????????}.h5 2>/dev/null | sed -e 's:.h5::' | tr '\n' ' '`
    echo "Tag '$tag' must be one of: $them" >&2
    echo "test" ;# cause eval `lookup-tag.sh ...` to exit with failure status
else
    echo "hf='$hf' clock='$clock' EXTRACTS=../../data/TERRA/extracts"
fi
