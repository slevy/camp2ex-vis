#! /usr/bin/env python3

import sys, os

import h5py
import numpy

import camp2exvis

import ict

import matplotlib.pyplot as mp

class BBR(object):
    """
    BBR broadband radiometer -- upwelling/downwelling IR (longwave) and solar (shortwave) radiation

    """

    stemname = "bbr"

    def __init__(self, fname, Day0yyyymmdd):

        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

        # time (hours) from 0h UTC on flight day, to visualization-frame 00000
        self.start_frame_hours = (self.timebase - self.Day0).total_seconds() / 3600.0  # tmhr of frame 0

        self.fname = fname
        self.datetag = Day0yyyymmdd
        hf = h5py.File(fname, 'r')
        self.times_h = hf['Time_Start'][:] / 3600.0  # Time_Start is in seconds since 0h UTC on flight day; we want hours since 0h

        def masked_val(fieldname):
            val = hf[fieldname][:]
            return numpy.ma.masked_array( val, val<=-9999 )

        self.down_solar = masked_val('DN_CM22_SOLAR_Irrad')
        self.up_solar = masked_val('UP_CM22_SOLAR_Irrad')
        self.down_IR = masked_val('DN_IR_Irrad')
        self.up_IR = masked_val('UP_IR_Irrad')
        # /DN_SPN1_Diffuse_SOLAR_Irrad Dataset {26225}
        # /DN_SPN1_Total_SOLAR_Irrad Dataset {26225}

    vrange = (1.0, 2000.0)
    plotylabel = "$W/m^2$"
    plottitle = "BBR Down/Upwelling Radiation"
    plotlabel = [ "Downwell SW", "Downwell LW", "Upwell SW", "Upwell LW" ]
    plotcolor = [ "#FFFF00",      "#FF3333",     "#888822",   "#881111"  ]



    def make_plot(self, plotno, onaxis, trange, timenow, legends=None):

        vals = [ self.down_solar, self.down_IR, self.up_solar, self.up_IR ]

        j0, j1 = sorted( [ min(j, len(self.times_h)-1) for j in numpy.searchsorted( self.times_h, trange ) ] )
        if j0 < j1:
            for val, lbl, clr in zip(vals, self.plotlabel, self.plotcolor):
                artist = onaxis.scatter( self.times_h[j0:j1], val[j0:j1], s=2, c=clr, label=lbl )

                if legends is not None:
                    legends.append( (artist, lbl) )

        if legends is None:
            onaxis.legend( loc="upper right" )

        onaxis.set_xlim( trange )
        onaxis.set_ylim( self.vrange )
        onaxis.set_ylabel(self.plotylabel)
        #onaxis.set_title( self.plottitle, y=0.95, pad=-14 )
