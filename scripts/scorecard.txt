What plot clumps are available, and which already exist?

[ Data archive reference:  https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex ]

Referring to CAMP2Ex_Vis_20210215.pptx from DiGirolamo and company

   
[x.y]

0.0:  time slider

        Avail: no.
        TODO: add new wide short timeslider page.   Maybe colorcode

1.1:  AHI 11 micron TB with P-3 track overlap

        Avail: no.
        TODO:  Add plotno 1 to db_images.py HIMAImages, expand "ahimap" page.

1.2:  P-3 Flight Track overlaid on optical Himawari

        Avail: yes. "ahimap" page.

1.3:  P-3 Flight track colored by altitude

        Avail: no.
        TODO:   Add extra plotno to db_mergedscalars make_plot_track, expand "ahimap" page

====

2.1:    forward dome video

        Avail: yes, from Kalina's extraction

2.2:   RSP "normalized size dist" for cloud droplet radius vs time.  Ribbon plot.  Cbar 0 .. 0.4, radius 0 .. 30 um

        Avail: no
        TODO:  Add to db_rsp and some page

2.3:   RSP  Re (um) vs time.   Scatterplot.  Overlaying "N-K"(blue) vs "Pol"(orange) results.   Re in 0 .. 30 um.

        Avail: yes?  on RSP page
        TODO: check N-K vs Pol 

2.4:   RSP optical depth vs time.  Scatterplot.   Overlaying "N-K"(blue) vs "Pol"(orange) results.  OD in 0..150.

        Avail: yes?  on RSP page
        TODO: check N-K vs Pol.  ==> N-K =? "/Cloud_Bi_Spec_Optical_Thickness", from  "Nakajima and King (1990)".  [0]=865+1590nm or [1]=865+2250nm
                                 ==> Pol =? "/Cloud_Bow_Optical_Thickness" (from nadir 865nm reflectance using polarimetric drop effective radius retrieval)

===
2.5:   APR3 W-band nadir reflectivity.  Ribbon plot, altitude vs time.  300m .. 5500m, -20 .. +60 dbZ

        Avail: no
        TODO: create db_apr, make_plot_nadir

2.6:   APR3 Ka-band nadir reflectivity.  Ribbon plot, altitude vs time.  300m .. 5500m, -20 .. +60 dbZ

        Avail: no
        TODO: create db_apr, make_plot_nadir

2.7a:   APR3 cross-track Ka-band reflectivity.   Slice plot, altitude 0..7000m vs cross-distance -4000 .. 4000, -20 .. +60 dbZ

        Avail: no
        TODO: db_apr, make_plot_crosstrack(0)

2.7b:   APR3 cross-track Ka-band Doppler velocity.   Slice plot, altitude 0..7000, cross-distance -4000..4000, -10 .. +10 m/s Doppler vel

        Avail: no
        TODO: db_apr, make_plot_crosstrack(1)

====

3.0:   nadir video

        Avail: yes, Kalina's extraction

3.1:    HSRL 532nm backsacatter.  Ribbon plot, time vs altitude.  0..6000?m, log Bsc532 from -4 to -1.

        Avail: yes, hsrl page

3.2:    HSRL depolarization ratio @532nm.  Ribbon plot, time vs altitude.   0..6000?m, ratio 0 - 0.3

        Avail: yes, hsrl page

3.3:    HSRL color ratio 1064nm/532nm

        Avail: yes, hsrl page

===

3.4     AMPR TB37 brightness temp. at 37 GHz.  Ribbon plot, time vs crosstrack angle -40..+40.

        Avail: yes, ampr page

3.5     AMPR TB85

        Avail: yes, ampr page

3.6:    AMPR TB10

        Avail: yes, ampr page

3.7:    AMPR LWC (liquid water content).  Ribbon plot, time vs crosstrack angle -40..+40.  0.0-1.0 kg/m^2

        Avail: yes, ampr page

========

4.1:    HSRL AOD @532nm.   Scatterplot of 4 quantities: HSRL's /DataProducts/532_AOT_hi, and RSP's 555_fine, 555_coarse, 555_total; all on AOD scale 0.0 - 2.0

        Avail: yes, RSP page
        TODO: HSRL /DataProducts/532_AOT_hi and mergedscalars 'tau_c_0555_VAN_DIEDENHOVEN', 'tau_f_0555_VAN_DIEDENHOVEN', 'tau_total_0555_VAN_DIEDENHOVEN'  (mostly masked, occasionally valid)

4.2:    HSRL "CTH" (/DataProducts/cloud_top_height) (km)    Scatterplot, HSRL 2Hz and RSP "/Cloud_Top_Altitude" vs time.   0 .. 6km.

        Avail: yes, RSP page
        TODO: where do values come from?   RSP: "/Cloud_Top_Altitude"  [0]=865nm, [1]=1880nm

4.3:    In-situ curves: RH and O3.   Curves vs time.  ** 10-second time range with hh:mm:ss tick labels **  RH 20% - 55%?  O3 20-34 ppbv ['way less than 10000!]

        Avail: yes, on tracegas page.

4.4:    In-situ trace gases: SO4, NO3, NH4, Chl, Org.   ** 10-second time range with hh:mm:ss tick labels **  0.0 - 0.40 ug/m^3 common scale

        Avail: yes, on tracegas page

4.5:    SSFR + SPNS.   Scatterplot, wavelength 0..2500nm vs W/m^2/nm 0..2.5.   Annotated by SZA (solar zenith angle)

        Four quantities on plot: SSFR zenith (blue), SSFR nadir (red), SPNS total (yellow), SPNS diffuse (green)

        Avail: kinda, Kalina's SSFR plot
        TODO: integrate SPNS (from where, Radiation?), add SZA (decimal) to legend.

4.6:    FIMS_total_conc and LAS_total_conc.   Scatterplot, time vs concentration (#/cm^3) (0..8000).

        Avail: no
        TODO: where do FIMS and LAS come from?

4.7:    FCDP and 2ds10 total conc.   Curve plot.   time vs conc(#/cm^3), 0..2000

=======

5.0:    upward dome video

        Avail: yes, Kalina's extraction

===

5.1:    MetNav P-3 altitude, P-3 Static_Temp, Dew_point_temp.
        Adaptive GPS_altitude scale (e.g. 6000m to 8000m) on left,
        Temperature (C) (-20 .. 0) on right

        Avail: partly, on metnav
        TODO: combine GPS_altitude with temperatures on one plot.  Adaptive altitude scale
        TODO: get both Static_Temp and Dew_point_temp on plot

5.2:    MetNav P-3 Yaw, Pitch, Roll       

        Avail: mostly, on metnav page
        TODO: where to find Yaw?

5.3:    Downwelling SW, Transmittance, Upwelling SW, Upwelling LW, Downwelling LW vs time
        Vertical scale: what units?   0.0 - 1.2

5.4a:   [What instrument?]  Curveplot? scatterplot?   particle size distribution (PSD) vs conc (#/cm^3).   PSD 0-60, conc 0-80

        Avail: no.
        TODO: what instrument?

5.4b:   Aerosol particle size (nm) vs dN/dlog(Dp (#/cm^3)), log-log plot.  curveplot.    size 10^1 .. 5*10^3; dN/dlog10(Dp) 10^-1 .. 5*10^3.
        Two sets of quantities: from FIMS (blue) and from LAS (magenta).

        Avail: no.
        TODO: where are LAS and FIMS data?

5.5:    CPI cloud droplet mugshots

        Avail: yes, on dedicated cpi page.

====

6.0:    Dropsonde  "SF09 S09 launched at yyyy-mm-dd hh:mm:ss Z"
        PI Susan C van den Heever
        Complex plot with:

            superimposed subplot: U by V winds (curves colored by altitude)

            main plot: temperature (-10 .. +40 C) vs pressure (1000 .. 400 hPa, but maybe really altitude)
            superimposed multiple sets of dotted lines -- adiabats(?) and (?) -- one in blue, one in red

            wind-vector-at-altitude flag/arrows along right edge

            superimposed box with conditions:
                PWAT = 54.59 mm
                LCL = 535.9 m
                LFC = 813.0 m
                CIN = -16.45 J/kg
                0-1km SHR = 10.83 kt
                0-3km SHR = 17.32 kt
                0-5km SHR = 13.83 kt

            Avail: no
            TODO: where do these come from?   Are there pre-rendered plots for each dropsonde?   If not how on earth do we make these?
