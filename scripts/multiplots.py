#! /usr/bin/env python3

import sys, os
import h5py
import numpy
import glob
import re
import PIL.Image
import datetime

import time
import posix

import camp2exvis

# import camp2exdb
import camp2exdb    # DatedFiles, jday2date

import camp2expages  # Page, and its per-page subclasses


import matplotlib.pyplot as mp


##############
        
be_dark = True
be_transparent = True
bgcolor = None

datetag = '20190915'
outtag = 'multiplots6-7'
outfmt = "../images/%(datetag)s/%(outtag)s/%(stemname)s/%(stemname)s.%(frameno)05d.png"
repair = False
boxcheck = False
timeflop = 1
scaleby = None

all_pagenames = set( camp2expages.Page.allpages().keys() )   # which pages to render?
pagenames = set( [p for p in all_pagenames if not p.startswith('_')] )

def Usage():
    print("""Usage: %s [options...] from[-to]%%incr ...
Options:
   -date yyyymmdd   -- use that day's data
   -o outtag        -- write to ../images/yyyymmdd/outtag/pagename/pagename.NNNNN.png
   -page pagename,pagename... -- render only those pages (default is all).  '-page help' shows a list.
   -page -pagename,pagename...   -- render all pages *except* those listed
   -r               -- repair.   Only render images that don't already exist.
   -timeflop        -- reverse graphical flow of time: from left-to-right (default right-to-left) on plots
   -scale S         -- enlarge all images by factor S
   -black           -- render image on opaque black background.   (Movies of raw page images look ugly if they're transparent.)
   
   -boxcheck        -- for each page, report what its resolution would have to be to create plot boxes 525 pixels wide.

   -light|-dark -- choose theme.   Default -dark.
   -bg          -- opaque background.  Default is transparent.
   -white|-black -- like -bg -light  or -bg -dark""" % (sys.argv[0]))
    sys.exit(1)

if len(sys.argv) <= 1:
    Usage()

ii = 1
while ii < len(sys.argv) and sys.argv[ii][0] == '-':
    opt = sys.argv[ii]; ii += 1
    if opt.startswith('-page'):
        arg = sys.argv[ii]; ii += 1
        not_these = (arg[0] == '-')
        ss = arg.replace('-','').replace(',',' ').split()
        missing = [s for s in ss if s not in all_pagenames]
        if missing != []:
            print("%s -page: don't recognize these page names: %s" % (sys.argv[0], ",".join(missing)))
            print("Known page names:")
            for s in sorted(list(all_pagenames)):
                print("\t%s" % s)
            sys.exit(1)
        if not_these:
            pagenames = pagenames - set(ss)
        else:
            pagenames = set(ss)

    elif opt == '-o':
        outtag = sys.argv[ii]; ii += 1

    elif opt == '-timeflop':
        timeflop = -1

    elif opt == '-scale':
        scaleby = float( sys.argv[ii] ); ii += 1

    elif opt == '-light':
        be_dark = False

    elif opt == '-dark':
        be_dark = True

    elif opt == '-bg':
        be_transparent = False

    elif opt == '-white':
        be_transparent = False
        be_dark = False

    elif opt == '-black':
        be_transparent = False
        be_dark = True

    elif opt == '-r':
        repair = True
        
    elif opt == '-date':
        datetag = sys.argv[ii]; ii += 1

    elif opt == '-boxcheck':
        boxcheck = True

    elif opt in ['-h', '--help']:
        Usage()

    else:
        print("Unknown option: %s" % opt)
        Usage()


if be_dark:
    # force dark background theme, with light text
    mp.style.use('dark_background')

if be_transparent:
    camp2expages.Page.backcolor = (0,0,0,0)
else:
    if be_dark:
        camp2expages.Page.backcolor = (0,0,0,1)
    else:
        camp2expages.Page.backcolor = (1,1,1,1)

# Mapping of animation frames to time-window
zero, timebase_h_0h, timeend_h_0h = camp2exvis.timerange_from( datetag, 'h_0h' )



startframe = 0
endframe = 28800        # overridden later
framestep = 60
        
allframes = []
allframes_set = set()

for frange in sys.argv[ii:]:   
    ss = frange.replace('-',' ').replace('%',' ')
    ii += 1
    vv = [int(s) for s in ss.split()]
    startframe = vv[0]
    endframe = vv[1] if len(vv)>1 else startframe
    framestep = vv[2] if len(vv)>2 else 1

    for frame in range(startframe, endframe+1, framestep):
        if frame not in allframes_set:
            allframes.append(frame)
            allframes_set.add(frame)

maxframe = int( (timeend_h_0h - timebase_h_0h) / camp2exvis.time_incr )

endframe = min(endframe, maxframe)

pages = [ camp2expages.Page.page[pn](datetag) for pn in pagenames ]

if False:
    # Paste scaling advice from Jeff in here.
    advice = """
    Dashboard layout scaling

    x=horizontal  y=vertical

    ahimap.16000.png

    scale: x 96%       y 96%
    fcdp.27800.png
    scale: x 100%     y 90%

    rsp.03700.png
    scale: x 100%     y 110%

    apr3.20300.png
    scale: x 85%       y 115%

    ampr.16000.png
    scale: x 100%     y 120%

    hsrl.17600.png
    scale: x 90%       y 95%

    tracegas.16000.png
    scale: x 80%       y 80%

    ssfr.16000.png
    scale: x 90%       y 70%

    metnav.16000.png
    scale: x 90%       y 50%

    dropsonde.04000.png
    scale: x 70%       y 70%

    cpi.01000.png
    scale: x 92%       y 75%

    timebar.16000.png
    scale: x 100%     y 100%
    """

    for line in advice.split('\n'):
        if 'png' in line:
            pname = line.split('.')[0]
        elif line.startswith('scale:'):
            ss = line.replace('%','').split()
            if ss[3] != 'y':
                raise ValueError("Couldn't parse 'scale:' line: " + line)
            sx, sy = float(ss[2]), float(ss[4])
            found = False
            for page in pages:
                if page.stemname == pname:
                    print("%s.resolution = (%d, %d)" % (pname, int(0.5+page.resolution[0]*sx/100), int(0.5+page.resolution[1]*sy/100)))
                    found = True
            if not found:
                print("# Couldn't find page named '%s' out of %s" % (pname, " ".join([p.stemname for p in pages])))

for page, pagename in zip(pages, pagenames):
    outname = outfmt % {'datetag':datetag, 'outtag':outtag, 'stemname':page.stemname, 'frameno':0}
    outdir = os.path.dirname( outname )
    print("# frames %d-%d%%%d of 0..%d in dir %s for page %s" % (startframe,endframe,framestep, maxframe, outdir, pagename))
    if not os.path.isdir( outdir ):
        try:
            os.makedirs( outdir )
        except:
            if not os.path.isdir(outdir):
                raise


for frameno in allframes:
    tcenter = timebase_h_0h + frameno * camp2exvis.time_incr   # current time, in hours since 0h UTC

    trange = tcenter - 0.5*timeflop*camp2exvis.time_window_width, tcenter + 0.5*timeflop*camp2exvis.time_window_width

    t0 = time.time()
    pcount = 0

    for page in pages:

        outfname = outfmt % {'datetag':datetag, 'outtag':outtag, 'stemname':page.stemname, 'frameno':frameno}

        if repair and os.path.exists(outfname) and os.path.getsize(outfname) > 0:
            continue

        # Load data, render figure
        fig = page.prepfig( trange, tcenter )

        if fig is None:
            continue
        elif hasattr(fig, '__call__'):
            fig(outfname)       # callable?  special HACK for dropsonde, which just tells us where to link the input image
            continue

        # Normally, the callee returns a Figure object with plots on it.   Save image for it.

        if boxcheck:
            boxwidths = [fig.transFigure.transform_bbox(aax.get_position()).width for aax in fig.axes]
            boxheights = [fig.transFigure.transform_bbox(aax.get_position()).height for aax in fig.axes]
            boxwidth = max(boxwidths)
            target_boxwidth = 525
            scaleby = target_boxwidth / float(boxwidth)
            print("New resolution (%d, %d) for page %s (boxwidth %d -> %d)  allboxes: W %s ||| H %s" % (page.resolution[0]*scaleby, page.resolution[1], page.stemname, boxwidth, target_boxwidth, " ".join(["%d" % w for w in boxwidths]), " ".join(["%d" % h for h in boxheights])))

        # fig.set_tight_layout(True)
        pcount += 1

        resolution, renderdpi = page.resolution, page.renderdpi
        if scaleby is not None:
            renderdpi *= scaleby
            resolution = numpy.rint( numpy.array(page.resolution) * scaleby ).astype(numpy.int)
        
        fig.savefig( outfname, dpi=renderdpi, facecolor=page.backcolor )

        mp.close( 'all' )

        # Shrink image down 2x for antialiasing
        im = PIL.Image.open( outfname )
        nim = im.resize( size=resolution, resample=PIL.Image.BICUBIC )
        nim.save( outfname )

        sys.stdout.flush()

    print("Frame %05d (time %.5f = %s) rendered %d pages in %.2f sec on %s" % (frameno, tcenter, camp2expages.hmstick(tcenter,0), pcount, time.time() - t0, posix.uname().nodename))
