#! /usr/bin/env python3

import sys, os

import h5py
import numpy

import camp2exvis

import matplotlib.pyplot as mp

class SPNTransmittance(object):
    """
    time = (hours since 0h UTC)
    value = ...

    """

    stemname = "spntransmittance"

    def __init__(self, fname, Day0yyyymmdd):

        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

        # time (hours) from 0h UTC on flight day, to visualization-frame 00000
        self.start_frame_hours = (self.timebase - self.Day0).total_seconds() / 3600.0  # tmhr of frame 0

        self.fname = fname
        self.datetag = Day0yyyymmdd
        self.hf = h5py.File(fname, 'r')
        self.times = self.hf['time'][:]
        self.transmittance = self.hf['value'][:]

    def hour2index(self, timenow):
        """Returns closest index to current time (in hours from 0h UTC on flight day). (Maybe we'll want to change this to rounding down to the nearest index?)"""
        ## idx = (numpy.abs(self.times - timenow)).argmin()
        idx = numpy.searchsorted( self.times, timenow, side='left' )
        return min( idx, len(self.times)-1 )

    def frame2index(self, curr_frame, start_frame=0):
        """Returns closest index to current frame. (Maybe we'll want to change this to rounding down to the nearest index?)"""
        # Convert frame to time (in hours)
        seconds_elapsed = curr_frame - start_frame
        hours_elapsed = seconds_elapsed / 60.0 / 60.0
        curr_hour = self.start_frame_hours + hours_elapsed

        # Convert time to index
        idx = self.hour2index(curr_hour)

        return idx

    plotvrange = [ (0, 1.2) ]

    def make_plot(self, plotno, axis, trange, timenow, legends=None):

        # there is only one plot - ignore plotno
        # we only display the current instant - ignore trange

        # Convert timenow to closest tmhr index
        j0 = self.hour2index(trange[0])
        j1 = self.hour2index(trange[1])


        trans = self.transmittance

        axis.set_xlim( *trange )
        axis.set_ylim( *self.plotvrange[plotno] )

        # Draw plot lines if any
        if j1>j0:
            artist = axis.scatter(self.times[j0:j1], self.transmittance[j0:j1], s=4, c='c', label="SPNS\nTransmittance")

        # (don't) Set title, camp2expages does that instead.
        ## camp2exvis.axis_set_title_styled( axis, "SPN-S Transmittance" )

