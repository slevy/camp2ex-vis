#! /usr/bin/env python3

import sys, os
import datetime
import re

import numpy
import netCDF4

import matplotlib.cm     as cm

from camp2expages import ribbon

import camp2exvis


class AMPRwind( object ):

    ## new style from https://data.giss.nasa.gov/pub/rsp/data/CAMP2Ex/L2CLD/
    ## RSP1-P3_20190915_T270950-271518_wcld_v1_1.nc

    whatname = "AMPRwind"    # name of data source

    def __init__(self, filename, Day0yyyymmdd):

        self.datetag = Day0yyyymmdd

        self.Day0, self.timebase, endtime = camp2exvis.timerange_from( Day0yyyymmdd, 'datetime' )

        # RSP's "/Data/Product_Time_Seconds" is in seconds since 0h UT on start of this flight's day.
        # Store the offset (seconds) between our adopted visualization 0-time for this day (self.Day0) after 0h UT on this day.

        self.filename = filename

        self.loaded = False

    def load_data( self ):

        if self.loaded:
            return

        self.loaded = True


        ncf = netCDF4.Dataset( self.filename, 'r' )
        # select time for this day's flight
        vtime = ncf['time']
        vwind = ncf['wind_speed']

        reftimestr = vtime.getncattr('units').replace('seconds since ','') # "seconds since 2019-09-04 00:34:37.500000"
        ss = reftimestr.split('.')
        reftime = datetime.datetime.strptime( ss[0], '%Y-%m-%d %H:%M:%S' )
        if len(ss)>1:
            reftime += datetime.timedelta( microseconds = 1000000 * float('0.'+ss[1]) )

        # Given flight timing, we want time samples that start at least 12h UT on the given Day0 and end by 12h UT on the following day.
        flightdate0h = datetime.datetime( self.Day0.year, self.Day0.month, self.Day0.day, 0, 0, 0 )
        timegate0 = ( flightdate0h + datetime.timedelta(seconds=12*3600) - reftime ).total_seconds()
        timegate1 = ( flightdate0h + datetime.timedelta(seconds=36*3600) - reftime ).total_seconds()

        times_raw = vtime[:]
        j0, j1 = sorted( numpy.searchsorted( times_raw, [timegate0, timegate1] ) )
        timeoffset = (flightdate0h - reftime).total_seconds()
        
        self.times_h = (times_raw[j0:j1] - timeoffset) / 3600.0 # time relative to 0h on start-of-flight date
        self.wind_speed = vwind[j0:j1]


    plotlabel = [ 'AMPR wind speed' ]
    plotcolor = [ 'g'          ]
    plotyrange = [ (0,40)  ]
    plottitle = [ 'AMPR wind speed' ]
    plotylabel = [ '$m/s$'  ]


    def make_plot( self, plotno, onaxis, timerange, tnow):
        """AMPRwind.make_plot().  0:speed"""

        self.load_data( )

        yrange = self.plotyrange[plotno] #  self.rft_radius[0], self.rft_radius[-1]
        label = self.plotlabel[plotno]
        extent = (timerange[0], yrange[0], timerange[1], yrange[-1])

        onaxis.set_xlim( *timerange )
        onaxis.set_ylim( *yrange )

        onaxis.set_ylabel( self.plotylabel[plotno] )

        onaxis.set_adjustable('box')

        if len(self.times_h) == 0:
            return

        onaxis.scatter( self.times_h, self.wind_speed, s=5, color=self.plotcolor[plotno], label=label )
