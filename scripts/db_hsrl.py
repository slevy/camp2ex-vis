#! /usr/bin/env python3

import sys, os
import numpy
import h5py

import matplotlib.transforms

from camp2exdb import jday2date

class HSRL(object):
    def __init__(self, fname, ymd_ignored=None):
        self.fname = fname
        self.hf = h5py.File(fname, 'r')
        times = self.hf['Nav_Data/gps_time'][:].ravel()  # in hours from 0 UTC on current date
        self.jdays = self.hf['Nav_Data/Jday'][:].ravel() # julian-like date, with base of Jan 1 0AD or so (366 days before Jan 1, 1 AD)
        self.times = times + 24 * (self.jdays - self.jdays[0])   # Time in our adopted units: hours from 0 UTC on date when flight started.
        self.timerange = (self.times[0], self.times[-1])
        self.Day0 = jday2date( self.jdays[0] )

        self.ntimes = len(self.times)
        self.per_hour = (self.times[-1] - self.times[0]) / (self.ntimes - 1)
        self.subsetix = None

        self.alts = self.hf['/DataProducts/Altitude'][:].ravel()

    def use_freq(self, target_per_hour):
        if target_per_hour > 0.75*self.per_hour:
            self.subsetix = None
        else:
            nsamples = numpy.round( (self.times[-1] - self.times[0]) * target_per_hour ).astype(numpy.int)
            self.subsetix = numpy.linspace(0, len(self.times)-1, num=nsamples).astype(numpy.int)

    knownrange = {'/DataProducts/Dust_Mixing_Ratio': (0,7),
                  '/DataProducts/532_bsc': (0,0.5),
                  '/DataProducts/532_AOT_above_cloud': (0,4),
                  '/DataProducts/532_Sa': (0,120),
                  '/DataProducts/cloud_top_height': (0,7.300),
                  '/DataProducts/Aerosol_ID': (0,7),
                  '/DataProducts/532_AOT_hi': (0,2),
                  '/Nav_Data/gps_alt': (0,8.0),
                 }

    def vrange_for(self, fieldname):
        if fieldname in self.knownrange:
            return self.knownrange[ fieldname ]
        if '/DataProducts/' + fieldname in self.knownrange:
            return self.knownrange[ '/DataProducts/' + fieldname ]
        return None
        
    def ribbon_for(self, fieldname):
        
        vrange = self.vrange_for( fieldname )
        values = self.hf[fieldname][:] if fieldname in self.hf else self.hf['/DataProducts/' + fieldname][:]
        times = self.times
        if self.subsetix is not None:
            times = times[self.subsetix]           
            if values.shape[0] == self.ntimes:
                values = values[self.subsetix]

        if vrange is None:
            vrange = (numpy.nanmin(values), numpy.nanmax(values))
        
        ##results = { 'vrange': vrange, 'values': values, 'times': times, 'alts': self.alts }
        results = { 'values': values, 'times': times, 'alts': self.alts*0.001 }
        return results

    def ribbon_ratio( self, field1, field2 ):
        r2 = self.ribbon_for( field2 )

        r1values = self.hf[field1][:] if field1 in self.hf else self.hf['/DataProducts/' + field1][:]

        rrat = r2.copy()
        rrat['values'] = r1values / r2['values']
        return rrat


    plotfield =  [ '/DataProducts/532_AOT_hi',  '/DataProducts/cloud_top_height', '/Nav_Data/gps_alt'  ]
    plotsubset=  [    None,                     None,                                None              ]
    plotvrange=  [   (0,2),                     (0,8),                               (0,8)             ]
    plotvscale=  [   1.0,                       0.001,                              0.001              ]
    plotlabel =  [   "HSRL AOD$_{532}$",      "HSRL cloudtop",                    "GPS Altitude"       ]
    plotcolor =  [     None,                      "c",                               "r"               ]
    plotylabel=  [   "AOD ($\\tau$)",            "$km$",                             ""                ]

    def make_plot( self, plotno, onaxis, trange, tcenter, legends=None ):
        """HSRL.make_plot()  0:AOD(aerosol optical depth)  1:CTH(cloud top height)  1:GPS Altitude"""

        values = self.hf[ self.plotfield[plotno] ][:]
        if self.plotsubset[plotno] is not None:
            values = values[ self.plotsubset[plotno] ]  # if we needed to take a slice of a 2D array, plotsubset[] could contain e.g. (None,0) or (1,None) to get the equivalent of [:,0] etc.
        values *= self.plotvscale[plotno]
        vrange = self.plotvrange[ plotno ]
        extent = ( trange[0], vrange[0], trange[1], vrange[1] )

        bbox = matplotlib.transforms.Bbox.from_extents( *extent )

        onaxis.set_label( self.plotlabel[plotno] )
        onaxis.set_xlim( *trange )
        onaxis.set_ylim( *vrange )
        
        onaxis.set_adjustable('box')

        label = self.plotlabel[plotno]

        if plotno == 2:
            # altitude curve
            artist = onaxis.plot( self.times, values, label=label, color=self.plotcolor[plotno], clip_box=extent, clip_on=True )
        else:
            # HSRL measurements get isolated dots
            artist = onaxis.scatter( self.times, values, s=5, label=label, color=self.plotcolor[plotno], clip_box=extent, clip_on=True )

        if legends is not None:
            legends.append( (artist,label) )
        else:
            onaxis.legend( loc='upper right' )

        onaxis.set_ylabel( self.plotylabel[plotno] )
