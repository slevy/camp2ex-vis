# camp2ex-vis
Code from the [NCSA AVL](https://avl.ncsa.illinois.edu/) project, ["Data Fusion Visualization for NASA CAMP2Ex Field Campaign"](http://hdl.handle.net/2142/110095) visualizing multi-instrument data from the NASA CAMP2Ex research flight.

Initially at least, this repo contains a subset.  Much of the code for 2D processing is here in the form of python scripts and config files, but the 3D animations require other software and more-complicated manual processes to create.

The repo also doesn't include the moderately-bulky data for even a single day's flight.

There's a separate 7GB package of [sample data](https://virdir.ncsa.illinois.edu/NCSAvis/camp2ex/camp2ex-vis/extrafiles-sample-data.tar.gz) (username camp2exvis, password same as for the [LARC web site](https://www-air.larc.nasa.gov/cgi-bin/ArcView/camp2ex) ).

This includes full data for most instruments, but only a few time intervals for APR3 (frames 2100-4350, 16890-17190, 20530-21070).

The data package will unpack into subdirectories "CAMP2Ex" (for most things) and "data" (for processed AHI imagery for the maps).In scripts/camp2exvis.py is a bunch of settings including TableFile() and PatternFIle() instances, that specify where it looks for data, as well as English-y descriptions of where to obtain that data.   The "%s" entries are the yyyymmdd start-of-flight date for each research flight, so 20190915 for RF09.

You can generate sample images with something like:

    cd scripts
    python3 multiplots.py -black -date 20190915 -o blah  2500-4300%100

This puts a pile of images into ../images/20190915/`blah`/`pagename`/`pagename`.NNNNN.png
It renders every 100th frame from 02500 through 04300.

Each `pagename` is an image group, corresponding to a class in `camp2expages.py`.

Try `python3 multiplots.py -page help` for a list of them.   If you only want a subset of those, try

    python3 multiplots.py -black -date 20190915 -o blah -page ahimap,apr3,hsrl  2500-4300%100
